<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route home
Route::get('/', 'ChapterDetailController@index')->name('');
Route::match(['get','post'],'/chapterDetail', 'ChapterDetailController@index');


//auth
Auth::routes();

Route::get('/logout','LoginController@getLogout');

Route::post('/postLogin','LoginController@postLogin')->name('postLogin');

Route::get('/login','LoginController@getLogin')->name('login');

//category
Route::match(['post','get'],'/category','CategoryController@index');

Route::match(['post','get'],'/getCatByid','CategoryController@getCatByid');

Route::match(['post','get'],'/updateCateById','CategoryController@updateCateById');

Route::match(['post','get'],'/deleteCatByid','CategoryController@deleteCatByid');

Route::match(['post','get'],'/insertCat','CategoryController@insertCat');

Route::match(['post','get'],'/findCatbyName','CategoryController@findCatbyName');

//book
Route::match(['post','get'],'/book','BookController@index');

Route::match(['post','get'],'/getBookByid','BookController@getBookByid');

Route::match(['post','get'],'/editBookByid','BookController@editBookByid');

Route::match(['post','get'],'/deleteBookByid','BookController@deleteBookByid');

Route::match(['post','get'],'/insertBook','BookController@insertBook');

Route::match(['post','get'],'/findBookbyName','BookController@findBookbyName');

Route::match(['post','get'],'/previewBook','BookController@previewBook');
//Author
Route::match(['post','get'],'/author','AuthorController@index');

Route::match(['post','get'],'/findAuthbyName','AuthorController@findAuthbyName');

Route::match(['post','get'],'/getAuthorByid','AuthorController@getAuthorByid');

Route::match(['post','get'],'/updateAuthor','AuthorController@updateAuthor');

Route::match(['post','get'],'/deleteAuthorByid','AuthorController@deleteAuthorByid');

Route::match(['post','get'],'/insertAuthor','AuthorController@insertAuthor');


//Link Chapter
Route::match(['post','get'],'/linkChapter','ChapterLinkController@index');

Route::match(['post','get'],'/getChapterLinkByid','ChapterLinkController@getChapterLinkByid');

Route::match(['post','get'],'/updateChapterLink','ChapterLinkController@updateChapterLink');

Route::match(['post','get'],'/deleteChapLinkByid','ChapterLinkController@deleteChapLinkByid');

//Xpath
Route::match(['post','get'],'/xpath','XpathController@index');

Route::match(['post','get'],'/getXpathByid','XpathController@getXpathByid');

Route::match(['post','get'],'/updateXpath','XpathController@updateXpathByid');

Route::match(['post','get'],'/deleteXpathByid','XpathController@deleteXpathByid');

Route::match(['post','get'],'/insertXpath','XpathController@insertXpath');

Route::match(['post','get'],'/findXpathbyName','XpathController@findXpathbyName');


//Cron setting
Route::match(['post','get'],'/cronSetting','cronSettingController@index');

Route::match(['post','get'],'/getCronById','cronSettingController@getCronById');

Route::match(['post','get'],'/editCronByid','cronSettingController@editCronByid');

Route::match(['post','get'],'/deleteCronByid','cronSettingController@deleteCronByid');

Route::match(['post','get'],'/insertCronSetting','cronSettingController@insertCronSetting');

//Chapter Detail
Route::match(['post','get'],'/getChapterDetailByid','ChapterDetailController@getChapterDetailByid');

Route::match(['post','get'],'/updateChapDetail','ChapterDetailController@updateChapDetail');

Route::match(['post','get'],'/deleteChapdetail','ChapterDetailController@deleteChapdetail');

Route::match(['post','get'],'/insertChapDetail','ChapterDetailController@insertChapDetail');


// getLinkChapController
// get book by cat id
Route::match(['post','get'],'/getBookByCatid','getLinkChapController@getBookByCatid');

Route::match(['post','get'],'/getChapterLinkByBook','getLinkChapController@getChapterLinkByBook');

Route::match(['post','get'],'/getChapterLinkByBookId','getLinkChapController@getChapterLinkByBookId');

Route::match(['post','get'],'/getLinkChapFromIndex','getLinkChapController@getLinkChapFromIndex');


// BookLinkController
Route::match(['post','get'],'/bookLink','BookLinkController@index');

Route::match(['post','get'],'/getBookLinkByid','BookLinkController@getBookLinkByid');

Route::match(['post','get'],'/updateBookLinkByid','BookLinkController@updateBookLinkByid');

Route::match(['post','get'],'/deleteBookLinkByid','BookLinkController@deleteBookLinkByid');

Route::match(['post','get'],'/insertBookLink','BookLinkController@insertBookLink');


//Route::match(['post','get'],'/findBookLinkbySource','BookLinkController@findBookLinkbySource');


// getChapDetailController
Route::match(['post','get'],'getChapdetailByidChapLink','getChapDetailController@getChapdetailByidChapLink');

Route::match(['post','get'],'getChapDetailByidBook','getChapDetailController@getChapDetailByidBook');

Route::match(['post','get'],'/publishByChapDetail','publishController@publishByChapDetail');