<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

ini_set('max_execution_time', 0);
define('MAX_FILE_SIZE', 6000000);

class ChapterLinkController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }
    //
    public function index(Request $request){
        $bookDataQuery = DB::table('chapterlinks as cl')
            ->select('cl.id','b.name as book','cl.source_url','cl.hash_url',DB::raw("DATE_FORMAT(cl.getTime, '%d-%m-%Y %H:%i:%s') as getTime"),'cl.chapName',DB::raw("DATE_FORMAT(cl.created_at, '%d-%m-%Y %H:%i:%s') as created_at"))
            ->leftJoin('books as b','b.id','=','cl.id_books')
            ->whereNull('cl.deleted_at');

//        $bookData['data'] = $bookDataQuery->orderBy('cl.id','desc')->get()->toArray();
        $bookData['data'] = $bookDataQuery->orderBy('cl.id','desc')->paginate(50);

        if ($request->ajax()){
//            return response()->json($bookData,200);
            return response()->json(['bookData' => $bookData],200);
        }
        return view('admins.chapterLink.index',['title'=>'Books Manage','bookData' => $bookData['data']]);
    }

    public function getChapterLinkByid(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data['id'];
            if(!empty($id)){
                $dataChapterLink = DB::table('chapterlinks  as cl')
                    ->select('cl.id','b.name as book','cl.source_url','cl.hash_url','cl.getTime','cl.id_books','cl.chapName',DB::raw("DATE_FORMAT(cl.getTime, '%d-%m-%Y %H:%i:%s') as getTime"))
                    ->leftJoin('books as b','b.id','=','cl.id_books')
                    ->where('cl.id',$id)->first();

                if(!empty($dataChapterLink)){
                    return response()->json($dataChapterLink,200);
                }
            }
        }
    }

    public function insertChapterLink(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $now = Carbon::now();
            $arrInsert = array(
                'id_books'=>$data['id_books'],
                'source_url'=>$data['source_url'],
                'hash_url'=>md5($data['source_url']),
                'created_at'=>$now
            );
            if($arrInsert){
                $insertData = DB::table('chapterlinks')->insert($arrInsert);
                if($insertData){
                    return response()->json(1,200);
                }else{
                    return response()->json(0,200);
                }
            }
        }
    }

    public function updateChapterLink(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data['id'];
            $now = Carbon::now();
            $arrData = array(
                'id_books'=>$data['select_book'],
                'source_url'=>$data['inputSourceUrl'],
                'hash_url'=>md5($data['inputSourceUrl']),
                'updated_at'=>$now
            );

            if(!empty($id)){
                $updateData = DB::table('chapterlinks')->where('id',$id)->update($arrData);
                if($updateData){
                    return response()->json(1,200);
                }else{
                    return response()->json(0,200);
                }
            }
        }
    }

    public function deleteChapLinkByid(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data['id'];
            if(!empty($id)){
                $now = Carbon::now();
                $deleteData = DB::table('chapterlinks')->where('id',$id)->update(['deleted_at'=>$now]);
                if($deleteData){
                    return response()->json(1,200);
                }else{
                    return response()->json(0,200);
                }
            }
        }
    }
}
