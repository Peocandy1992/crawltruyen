<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Sunra\PhpSimple\HtmlDomParser;
use Illuminate\Support\Facades\Redis;
use GuzzleHttp\Client;

ini_set('max_execution_time', 0);


class getLinkChapController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    //get Chap link manual
    function getBookByCatid(Request $request)
    {
        $data = $request->all();
        if (!empty($data)) {
            $redis = Redis::connection();
            $redis->del('sAllChap');
            $category_id = $data['id'];

            if (!empty($category_id)) {

                /** Get Categories Info */
                $category = DB::table('categories')->select('id', 'slug', 'name')->where('id', $category_id)->first();
                $slug = $category->slug;

                /** Get Xpath Configs */
                $xpath = DB::table('xpath')->select('xpath_options')->where('id', 14)->first();
                $xpath_page = DB::table('xpath')->select('xpath_options')->where('id', 2)->first();
                $xpath_paginate = $xpath_page->xpath_options;


                /** Check chapterLink Exits */
                $listChap = DB::table('chapterlinks')->select('source_url')->get();

                if (!empty($listChap)) {
                    foreach ($listChap as $k => $chapItem) {
                        $redis->sadd('sAllChap', $chapItem->source_url);
                    }
                }


                if (!empty($xpath) && !empty($slug)) {
                    $mainUrl = 'http://webtruyen.com/';
                    $fullUrl = $mainUrl . $slug;
                    if ($this->checkUrl($fullUrl) == true) {
                        try {
                            // get html by guzzle
                            $client = new \GuzzleHttp\Client();
                            $res = $client->request('GET', $fullUrl);
                            $body = $res->getBody();
                            $remainingBytes = $body->getContents();

                            // using htmldomParser get content html
                            $html = HtmlDomParser::str_get_html($remainingBytes);
                        } catch (exception $e) {
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
                            curl_setopt($ch, CURLOPT_URL, $fullUrl);
                            curl_setopt($ch, CURLOPT_HEADER, 0);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            $html = curl_exec($ch);
                            curl_close($ch);

                            // using htmldomParser get content html
                            $html = HtmlDomParser::str_get_html($html);
                        }

                        if (!empty($html)) {
                            $html = HtmlDomParser::file_get_html($fullUrl);
                        }

                        if ($html) {

                            /** Update Category Infomations */
                            if (!empty($xpath_paginate)) {
                                $paginate = '1';
                                foreach ($html->find($xpath_paginate) as $paginate_items) {
                                    $paginate = $paginate_items->innertext();
                                    $paginate = str_replace('Trang 1 / ', '', $paginate);
                                    break;
                                }

                                $updatePaginate_Category = DB::table('categories')->where('id', $category_id)->update(['paginate' => $paginate, 'paginate_left' => $paginate, 'status' => 1]);

                            }

                            $xpath_link = '.list-content .row-info a';
                            foreach ($html->find($xpath_link) as $item) {
                                $checkExitsBook = $redis->SISMEMBER('sAllChap', $item->href);
                                $now = Carbon::now();
                                dump($checkExitsBook, $item->href);
                                if ($checkExitsBook == 0) {

                                    $insertBookLink = DB::table('booklink')->insert(['source_url' => $item->href, 'created_at' => $now, 'hash_url' => md5($item->href), 'status' => 1, 'cat_id' => $category_id]);
                                    $redis->sadd('sAllChap', md5($item->href));
                                    $redis->sadd('sChapIns', $item->href);
                                }
                            }
                            $redis->del('sAllChap');
                            return response()->json(1, 200);

                        }
                    }
                }

            }
        }
    }

    static function getBookLinkConf($idCat)
    {

        if (!empty($idCat)) {
            $redis = Redis::connection();
            $redis->del('sAllChap');

            $category = DB::table('categories')->select('id', 'slug', 'name')->where('id', '=', $idCat)->get();

            foreach ($category as $k => $cat) {
                if ($idCat == 15 || $idCat == 16) {
                    $xpath = DB::table('xpath')->select('xpath_options')->where('id', 14)->first();
                    $xpath_page = DB::table('xpath')->select('xpath_options')->where('id', 2)->first();

                    $listChap = DB::table('chapterlinks')->select('hash_url')->get();

                    if (!empty($listChap)) {
                        foreach ($listChap as $k => $chapItem) {
                            $redis->sadd('sAllChap', $chapItem->hash_url);
                        }
                    }
                    $slug = $cat->slug;
                    $arrRet = array('xpath' => $xpath, 'xpath_page' => $xpath_page, 'slug' => $slug, 'idCat' => $idCat);
                    dump("15 16:" . json_encode($arrRet));
                    return $arrRet;
                } else {

                    $bookLink = DB::table('booklink')->select('hash_url')->get();
                    if (!empty($bookLink)) {
                        foreach ($bookLink as $kbLink => $vbLink) {
                            $redis->sadd('sBookLink', $vbLink->hash_url);
                        }
                    }

                    $ListBook = DB::table('books')->select('slug')->get();

                    if (!empty($ListBook)) {
                        foreach ($ListBook as $k => $books) {
                            $redis->sadd('stemplink', $books->slug);
                        }
                    }

                    $xpath = DB::table('xpath')->select('xpath_options')->where('id', 1)->first();
                    $xpath_page = DB::table('xpath')->select('xpath_options')->where('id', 2)->first();
                    $slug = $cat->slug;
                    $arrRet = array('xpath' => $xpath, 'xpath_page' => $xpath_page, 'slug' => $slug, 'idCat' => $idCat);
                    dump("not 15 16:" . json_encode($arrRet));
                    return $arrRet;
                }

            }
        }
    }

    /** getBookLink by cronjob **/
    function getBookLink($configs)
    {
        if (!empty($configs)) {
            $xpath = $configs['xpath'];
            $xpath_page = $configs['xpath_page'];
            $slug = $configs['slug'];
            $now = Carbon::now();
            $redis = Redis::connection();

            $category_id = $configs['idCat'];
            if (!empty($category_id)) {
                if ($category_id == 15 || $category_id == 16) {
                    if (!empty($xpath) && !empty($slug)) {
                        $mainUrl = 'http://webtruyen.com/';
                        $fullUrl = $mainUrl . $slug;
                        if (self::checkUrl($fullUrl) == true) {
                            try {
                                // get html by guzzle
                                $client = new \GuzzleHttp\Client();
                                $res = $client->request('GET', $fullUrl);
                                $body = $res->getBody();
                                $remainingBytes = $body->getContents();

                                // using htmldomParser get content html
                                $html = HtmlDomParser::str_get_html($remainingBytes);
                            } catch (exception $e) {
                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
                                curl_setopt($ch, CURLOPT_URL, $fullUrl);
                                curl_setopt($ch, CURLOPT_HEADER, 0);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                $html = curl_exec($ch);
                                curl_close($ch);

                                // using htmldomParser get content html
                                $html = HtmlDomParser::str_get_html($html);
                            }

                            if (!empty($html)) {
                                $html = HtmlDomParser::file_get_html($fullUrl);
                            }

                            if ($html) {

                                foreach ($html->find($xpath->xpath_options) as $item) {
                                    $checkExitsBook = $redis->SISMEMBER('sAllChap', md5($item->href));
                                    if ($checkExitsBook == 0) {
                                        $redis->sadd('sAllChap', md5($item->href));
                                        $redis->sadd('sChapIns', $item->href);
                                    }
                                }

                                foreach ($html->find($xpath_page->xpath_options) as $val) {

                                    $page = trim(substr($val->innertext(), 9));
                                    for ($i = 2; $i <= $page; $i++) {
                                        $subPageUrl = $fullUrl . '/' . $i . '/';
                                        try {
                                            // get html by guzzle
                                            $client = new \GuzzleHttp\Client();
                                            $res = $client->request('GET', $subPageUrl);
                                            $body = $res->getBody();
                                            $remainingBytes = $body->getContents();

                                            // using htmldomParser get content html
                                            $html = HtmlDomParser::str_get_html($remainingBytes);
                                        } catch (exception $e) {
                                            $ch = curl_init();
                                            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
                                            curl_setopt($ch, CURLOPT_URL, $subPageUrl);
                                            curl_setopt($ch, CURLOPT_HEADER, 0);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                            $html = curl_exec($ch);
                                            curl_close($ch);

                                            // using htmldomParser get content html
                                            $html = HtmlDomParser::str_get_html($html);
                                        }

                                        if (!empty($html)) {
                                            $html = HtmlDomParser::file_get_html($subPageUrl);
                                        }

                                        if ($html) {
                                            foreach ($html->find($xpath->xpath_options) as $item) {

                                                $checkExitsBookInPage = $redis->SISMEMBER('sAllChap', md5($item->href));
                                                if ($checkExitsBookInPage == 0) {
                                                    $redis->sadd('sAllChap', md5($item->href));

                                                    /* $idBook = 1 default is Tong hop */
                                                    $idBook = 1;

                                                    $arrIns = array(
                                                        'source_url' => trim($item->href),
                                                        'hash_url' => md5(trim($item->href)),
                                                        'id_books' => $idBook,
                                                        'created_at' => $now,
                                                        'chapName' => trim($item->innertext)
                                                    );

                                                    $dataInsert = DB::table('chapterlinks')->insert($arrIns);
                                                    unset($arrIns);
                                                }
                                            }

                                            $redis->del('stemplink');
                                            $redis->del('sChapIns');
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (!empty($xpath) && !empty($slug)) {
                        $mainUrl = 'http://webtruyen.com/';
                        $fullUrl = $mainUrl . $slug;
                        if ($this->checkUrl($fullUrl) == true) {
                            try {
                                // get html by guzzle
                                $client = new \GuzzleHttp\Client();
                                $res = $client->request('GET', $fullUrl);
                                $body = $res->getBody();
                                $remainingBytes = $body->getContents();

                                // using htmldomParser get content html
                                $html = HtmlDomParser::str_get_html($remainingBytes);
                            } catch (exception $e) {
                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
                                curl_setopt($ch, CURLOPT_URL, $fullUrl);
                                curl_setopt($ch, CURLOPT_HEADER, 0);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                $html = curl_exec($ch);
                                curl_close($ch);

                                // using htmldomParser get content html
                                $html = HtmlDomParser::str_get_html($html);
                            }

                            if (!empty($html)) {
                                $html = HtmlDomParser::file_get_html($fullUrl);
                            }

                            if ($html) {
                                foreach ($html->find($xpath->xpath_options) as $item) {

                                    $checkExitsBook = $redis->SISMEMBER('stemplink', $item->href);
                                    $checkExitsLinkBook = $redis->SISMEMBER('sBoookLink', md5($item->href));

                                    if ($checkExitsBook == 0) {
                                        $redis->sadd('sBookLink', md5($item->href));
                                    }

                                    if ($checkExitsLinkBook == 0) {
                                        $redis->sadd('stemplink', $item->href);
                                    }
                                }

                                foreach ($html->find($xpath_page->xpath_options) as $val) {
                                    $page = trim(substr($val->innertext(), 9));
                                    for ($i = 2; $i <= $page; $i++) {
                                        $subPageUrl = $fullUrl . '/' . $i . '/';
                                        try {
                                            // get html by guzzle
                                            $client = new \GuzzleHttp\Client();
                                            $res = $client->request('GET', $subPageUrl);
                                            $body = $res->getBody();
                                            $remainingBytes = $body->getContents();

                                            // using htmldomParser get content html
                                            $html = HtmlDomParser::str_get_html($remainingBytes);
                                        } catch (exception $e) {
                                            $ch = curl_init();
                                            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
                                            curl_setopt($ch, CURLOPT_URL, $subPageUrl);
                                            curl_setopt($ch, CURLOPT_HEADER, 0);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                            $html = curl_exec($ch);
                                            curl_close($ch);

                                            // using htmldomParser get content html
                                            $html = HtmlDomParser::str_get_html($html);
                                        }

                                        if (!empty($html)) {
                                            $html = HtmlDomParser::file_get_html($subPageUrl);
                                        }
                                        foreach ($html->find($xpath->xpath_options) as $item) {
                                            $checkExitsBookInPage = $redis->SISMEMBER('stemplink', $item->href);
                                            $checkExitsLinkBook = $redis->SISMEMBER('sBoookLink', md5($item->href));

                                            if ($checkExitsBookInPage == 0) {
                                                if ($checkExitsLinkBook == 0) {
                                                    $redis->sadd('sBookLink', md5($item->href));
                                                    $redis->sadd('stemplink', $item->href);
                                                }
                                            }
                                        }

                                    }
                                }

                                //remove duplicate url
                                $arrLinkBook = $redis->smembers('stemplink');
                                if (!empty($arrLinkBook)) {
                                    $arrLinkBookInsert = self::addArrayLink($arrLinkBook, $category_id);
                                    // chia nhỏ mảng thành 1k phần tử r insert
                                    foreach (array_chunk($arrLinkBookInsert, 1000) as $t) {
                                        $dataInsert = DB::table('booklink')->insert($t);
                                    }
                                }
                                $redis->del('stemplink');
                            }
                        }
                    }
                }
            }
        }
    }

    //get chapter link by book manual
    public function getChapterLinkByBook(Request $request)
    {
        $data = $request->all();
        if (!empty($data)) {
            $id = $data['id'];
            if (!empty($id)) {
                $redis = Redis::connection();
                // get book link from id booklinks
                $bookData = DB::table('booklink')->select('id', 'cat_id', 'source_url', 'hash_url')->where('id', $id)->first();
                if (!empty($bookData)) {
                    $source_url = $bookData->source_url;
                    $catId = $bookData->cat_id;

                    if ($this->checkUrl($source_url) == true) {
                        try {
                            // get html by guzzle
                            $client = new \GuzzleHttp\Client();
                            $res = $client->request('GET', $source_url);
                            $body = $res->getBody();
                            $remainingBytes = $body->getContents();

                            // using htmldomParser get content html
                            $html = HtmlDomParser::str_get_html($remainingBytes);
                        } catch (exception $e) {
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
                            curl_setopt($ch, CURLOPT_URL, $source_url);
                            curl_setopt($ch, CURLOPT_HEADER, 0);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            $html = curl_exec($ch);
                            curl_close($ch);

                            // using htmldomParser get content html
                            $html = HtmlDomParser::str_get_html($html);
                        }

                        if (empty($html)) {
                            $html = HtmlDomParser::file_get_html($source_url);
                        }

                        if ($html) {
                            //GET CONFIG XPATH
                            $authorXpath = DB::table('xpath')->select('id', 'xpath_name', 'xpath_options')->where('id', 4)->first();
                            $descriptionsXpath = DB::table('xpath')->select('id', 'xpath_name', 'xpath_options')->where('id', 3)->first();

                            $paginateXpath = DB::table('xpath')->select('id', 'xpath_name', 'xpath_options')->where('id', 7)->first();
//                            $chapterLinkNew = DB::table('xpath')->select('id','xpath_name','xpath_options')->where('id',6)->first();
                            $chapterLinkOld = DB::table('xpath')->select('id', 'xpath_name', 'xpath_options')->where('id', 5)->first();

                            $bookNameXpath = DB::table('xpath')->select('id', 'xpath_name', 'xpath_options')->where('id', 11)->first();
                            $BookThumbnailXpath = DB::table('xpath')->select('id', 'xpath_name', 'xpath_options')->where('id', 12)->first();

                            $releaseStatusXpath = DB::table('xpath')->select('id', 'xpath_name', 'xpath_options')->where('id', 17)->first();
                            // END GET CONFIG PATH

                            // GET LIST DATA EXITS
                            $listAuthor = DB::table('author')->select('author_name')->get();

                            foreach ($listAuthor as $k => $v) {
                                $auNameEncode = md5($v->author_name);
                                $redis->sadd('sAuthorName', $auNameEncode);
                            }

                            $listChap = DB::table('chapterlinks')->select('hash_url')->get();

                            foreach ($listChap as $key => $Hashlc) {
                                $redis->sadd('sListChapter', $Hashlc->hash_url);
                            }

                            $listBook = DB::table('books')->select('slug')->get();

                            foreach ($listBook as $kBook => $vBook) {
                                $hashSlug = md5($vBook->slug);
                                $redis->sadd('sListBook', $hashSlug);
                            }


                            // END LIST DATA EXIT

                            // CREATE VALUE
                            $bookInfo = (object)[
                                'slug' => $source_url,
                                'id_cat' => $catId
                            ];

                            // GET AUTHOR
                            $authorXpath = $authorXpath->xpath_options;

                            foreach ($html->find($authorXpath) as $author) {

                                $authorName = strip_tags($author->innertext);
                                $authorSlug = $author->href;
                                $checkExits = $redis->SISMEMBER('sAuthorName', md5($authorName));

                                if ($checkExits == 0) {
                                    $now = Carbon::now();
                                    $idAuthor = DB::table('author')->insertGetId(['author_name' => $authorName, 'slug' => $authorSlug, 'created_at' => $now]);
                                    $bookInfo->author = $idAuthor;

                                    $authorNameEnc = md5($authorName);
                                    $redis->sadd('sAuthorName', $authorNameEnc);
                                } else {
                                    $dataAuthor = DB::table('author')->select('author_name', 'id')->where('author_name', 'like', '%' . $authorName . '%')->orderBy('id', 'asc')->first();
                                    $bookInfo->author = $dataAuthor->id;

                                    $authorNameEnc = md5($authorName);
                                    $redis->sadd('sAuthorName', $authorNameEnc);
                                }
                            }
                            // END GET AUTHOR

                            // GET DESCRIPTIONS
                            $descriptionsXpath = $descriptionsXpath->xpath_options;

                            foreach ($html->find($descriptionsXpath) as $descript) {
                                $bookInfo->descriptions = strip_tags($descript->innertext());
                            }
                            // END GET DESCRIPTIONS

                            // GET BOOK THUMBNAIL
                            $BookThumbnailXpath = $BookThumbnailXpath->xpath_options;

                            foreach ($html->find($BookThumbnailXpath) as $thumbnail) {
                                $bookInfo->bookThumbnail = $thumbnail->src;
                            }
                            // END GET BOOK THUMBNAIL


                            // GET RELEASE STATUS
                            $releaseStatusXpath = $releaseStatusXpath->xpath_options;


                            foreach ($html->find($releaseStatusXpath) as $releaseStatus) {
                                $bookInfo->releaseStatus = trim(strip_tags($releaseStatus->children(3)->innertext()));
                            }

                            // END RELEASE STATUS

                            // GET BOOK NAME
                            $bookNameXpath = $bookNameXpath->xpath_options;

                            $checkBookExist = 0;
                            $bookUrl = '';
                            foreach ($html->find($bookNameXpath) as $bookName) {
                                $bookUrl = $bookName->href;
                                $hashBookSlug = md5($bookName->href);
                                $checkBookExist = DB::table('books')->select('id', 'name')->where('name', '=', $bookName->innertext())->first();
                                if (!empty($checkBookExist->id)) {
                                    $bookInfo->bookName = $checkBookExist->name;
                                    $checkBookExist = 1;
                                } else {
                                    $bookInfo->bookName = trim($bookName->innertext());
                                }
                            }
                            // END GET BOOK NAME
                            // END CREATE VALUES

                            //HANDLE BOOK INFO
                            if ($checkBookExist == 0) {
                                if ($bookInfo) {
                                    $arrIns = array(
                                        'slug' => $bookInfo->slug,
                                        'id_cat' => $bookInfo->id_cat,
                                        'id_author' => $bookInfo->author,
                                        'description' => $bookInfo->descriptions,
                                        'name' => $bookInfo->bookName,
                                        'images' => $bookInfo->bookThumbnail,
                                        'releaseStatus' => $bookInfo->releaseStatus
                                    );


                                    $insertIdBook = DB::table('books')->insertGetId($arrIns);

                                    if (!empty($insertIdBook)) {
                                        $now = Carbon::now();

                                        // HANDLE LINK FROM PAGE 1
                                        $chapterLinkOld = $chapterLinkOld->xpath_options;
                                        if (!empty($chapterLinkOld)) {
                                            foreach ($html->find($chapterLinkOld) as $chapOld) {
                                                $chapLinkPage1Hash = md5($chapOld->href);
                                                $chapterOldName = $chapOld->innertext;

                                                $checkChap1Exists = $redis->SISMEMBER('sListChapter', $chapLinkPage1Hash);
                                                if ($checkChap1Exists == 0) {
                                                    $chapLinkPage1 = array(
                                                        'id_books' => $insertIdBook,
                                                        'source_url' => $chapOld->href,
                                                        'hash_url' => $chapLinkPage1Hash,
                                                        'chapName' => $chapterOldName,
                                                        'created_at' => $now
                                                    );

                                                    $insChapLinkPage1 = DB::table('chapterlinks')->insert($chapLinkPage1);
                                                    if ($insChapLinkPage1) {
                                                        $redis->sadd('sListChapter', $chapLinkPage1Hash);
                                                    }
                                                }
                                            }
                                        }

                                        // HANDLE PAGINATE FROM PAGE 2
                                        $paginateXpath = $paginateXpath->xpath_options;
                                        if (!empty($paginateXpath)) {
                                            foreach ($html->find($paginateXpath) as $page) {

                                                $page = trim(substr($page->innertext(), 9));
                                                for ($i = 2; $i <= $page; $i++) {
                                                    $subPageUrl = $source_url . $i . '/';
                                                    try {
                                                        // get html by guzzle
                                                        $client = new \GuzzleHttp\Client();
                                                        $res = $client->request('GET', $subPageUrl);
                                                        $body = $res->getBody();
                                                        $remainingBytes = $body->getContents();

                                                        // using htmldomParser get content html
                                                        $html = HtmlDomParser::str_get_html($remainingBytes);
                                                    } catch (exception $e) {
                                                        $ch = curl_init();
                                                        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
                                                        curl_setopt($ch, CURLOPT_URL, $subPageUrl);
                                                        curl_setopt($ch, CURLOPT_HEADER, 0);
                                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                                        $html = curl_exec($ch);
                                                        curl_close($ch);

                                                        // using htmldomParser get content html
                                                        $html = HtmlDomParser::str_get_html($html);
                                                    }

                                                    if (!empty($html)) {
                                                        $html = HtmlDomParser::file_get_html($subPageUrl);
                                                    }


                                                    if ($html) {
                                                        foreach ($html->find($chapterLinkOld) as $item) {
                                                            $chapterLinkHashPage = md5($item->href);
                                                            $chapterLinkName = $item->innertext;

                                                            $chapLinkDataPage = array(
                                                                'id_books' => $insertIdBook,
                                                                'source_url' => $item->href,
                                                                'hash_url' => $chapterLinkHashPage,
                                                                'chapName' => $chapterLinkName,
                                                                'created_at' => $now
                                                            );
                                                            if (!empty($chapLinkDataPage)) {
                                                                $insChapLinkPage = DB::table('chapterlinks')->insert($chapLinkDataPage);
                                                                if ($insChapLinkPage) {
                                                                    $redis->sadd('sListChapter', $chapterLinkHashPage);
                                                                }
                                                            }
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                        // END HANDE PAGINATE
                                        return response()->json(1, 200);
                                    } else {
                                        return response(0, 200);
                                    }
                                }
                            } else {
                                $idBook = DB::table('books')->select('id')->where('slug', $bookUrl)->first();
                                $now = Carbon::now();

                                // HANDLE PAGINATE FROM PAGE 2
                                $paginateXpath = $paginateXpath->xpath_options;
                                if (!empty($paginateXpath)) {

                                    foreach ($html->find($paginateXpath) as $page) {

                                        $page = trim(substr($page->innertext(), 9));

                                        for ($i = 2; $i <= $page; $i++) {
                                            $subPageUrl = $source_url . $i . '/';
                                            try {
                                                // get html by guzzle
                                                $client = new \GuzzleHttp\Client();
                                                $res = $client->request('GET', $subPageUrl);
                                                $body = $res->getBody();
                                                $remainingBytes = $body->getContents();

                                                // using htmldomParser get content html
                                                $html = HtmlDomParser::str_get_html($remainingBytes);
                                            } catch (exception $e) {
                                                $ch = curl_init();
                                                curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
                                                curl_setopt($ch, CURLOPT_URL, $subPageUrl);
                                                curl_setopt($ch, CURLOPT_HEADER, 0);
                                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                                $html = curl_exec($ch);
                                                curl_close($ch);

                                                // using htmldomParser get content html
                                                $html = HtmlDomParser::str_get_html($html);
                                            }

                                            if (!empty($html)) {
                                                $html = HtmlDomParser::file_get_html($subPageUrl);
                                            }
                                            if ($html) {


                                                foreach ($html->find($chapterLinkOld->xpath_options) as $itemOld) {
                                                    $checkChapExits = $redis->sismember('sListChapter', md5($itemOld->href));

                                                    if ($checkChapExits == 0) {
                                                        $chapterLinkHashPage = md5($itemOld->href);
                                                        $itemName = $itemOld->innertext;

                                                        $chapLinkDataPage = array(
                                                            'id_books' => $idBook->id,
                                                            'source_url' => $itemOld->href,
                                                            'hash_url' => $chapterLinkHashPage,
                                                            'chapName' => $itemName,
                                                            'created_at' => $now
                                                        );
                                                        if (!empty($chapLinkDataPage)) {
                                                            $insChapLinkPage = DB::table('chapterlinks')->insert($chapLinkDataPage);
                                                            if ($insChapLinkPage) {
                                                                $redis->sadd('sListChapter', $chapterLinkHashPage);
                                                                $redis->smembers('sListChapter');
                                                            }
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                        return response()->json(1, 200);
                                    }

                                }
                                // END HANDE PAGINATE
                            }
                            // END HANDLE BOOK INFO

                        }
                    }
                }
            }
        }
    }

    //get chapter link by book id
    public function getChapterLinkByBookId(Request $request)
    {
        $data = $request->all();
        if (!empty($data)) {
            $id = $data['id'];
            if (!empty($id)) {
                $redis = Redis::connection();

                $bookData = DB::table('books')->select('id', 'id_cat', 'slug')->where('id', $id)->first();

                if (!empty($bookData)) {
                    $source_url = $bookData->slug;
                    $catId = $bookData->id_cat;
                    $idBook = $bookData->id;

                    if ($this->checkUrl($source_url) == true) {

                        try {
                            // get html by guzzle
                            $client = new \GuzzleHttp\Client();
                            $res = $client->request('GET', $source_url);
                            $body = $res->getBody();
                            $remainingBytes = $body->getContents();

                            // using htmldomParser get content html
                            $html = HtmlDomParser::str_get_html($remainingBytes);
                        } catch (exception $e) {
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
                            curl_setopt($ch, CURLOPT_URL, $source_url);
                            curl_setopt($ch, CURLOPT_HEADER, 0);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            $html = curl_exec($ch);
                            curl_close($ch);

                            // using htmldomParser get content html
                            $html = HtmlDomParser::str_get_html($html);
                        }

                        if (empty($html)) {
                            $html = HtmlDomParser::file_get_html($source_url);
                        }

                        if ($html) {
                            //GET CONFIG XPATH
                            $paginateXpath = DB::table('xpath')->select('id', 'xpath_name', 'xpath_options')->where('id', 7)->first();
//                            $chapterLinkNew = DB::table('xpath')->select('id','xpath_name','xpath_options')->where('id',6)->first();
                            $chapterLinkOld = DB::table('xpath')->select('id', 'xpath_name', 'xpath_options')->where('id', 5)->first();

                            // END GET CONFIG PATH

                            // GET LIST DATA EXITS
                            $listChap = DB::table('chapterlinks')->select('hash_url')->get();

                            foreach ($listChap as $key => $Hashlc) {
                                $redis->sadd('sListChapter', $Hashlc->hash_url);
                            }
                            // END LIST DATA EXIT

                            //HANDLE BOOK INFO
                            if (!empty($idBook)) {
                                $now = Carbon::now();

                                // HANDLE LINK FROM PAGE 1
                                $chapterLinkOld = $chapterLinkOld->xpath_options;
                                if (!empty($chapterLinkOld)) {
                                    foreach ($html->find($chapterLinkOld) as $chapOld) {
                                        $chapLinkPage1Hash = md5($chapOld->href);
                                        $checkChap1Exists = $redis->SISMEMBER('sListChapter', $chapLinkPage1Hash);
                                        if ($checkChap1Exists == 0) {
                                            $chapLinkPage1 = array(
                                                'id_books' => $idBook,
                                                'source_url' => $chapOld->href,
                                                'hash_url' => $chapLinkPage1Hash,
                                                'created_at' => $now
                                            );

                                            $insChapLinkPage1 = DB::table('chapterlinks')->insert($chapLinkPage1);
                                            if ($insChapLinkPage1) {
                                                $redis->sadd('sListChapter', $chapLinkPage1Hash);
                                            }
                                        }
                                    }
                                }

                                // HANDLE PAGINATE FROM PAGE 2
                                $paginateXpath = $paginateXpath->xpath_options;
                                if (!empty($paginateXpath)) {
                                    foreach ($html->find($paginateXpath) as $page) {
                                        $page = trim(substr($page->innertext(), 9));
                                        for ($i = 2; $i <= $page; $i++) {
                                            $subPageUrl = $source_url . $i . '/';
                                            try {
                                                // get html by guzzle
                                                $client = new \GuzzleHttp\Client();
                                                $res = $client->request('GET', $subPageUrl);
                                                $body = $res->getBody();
                                                $remainingBytes = $body->getContents();

                                                // using htmldomParser get content html
                                                $html = HtmlDomParser::str_get_html($remainingBytes);
                                            } catch (exception $e) {
                                                $ch = curl_init();
                                                curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
                                                curl_setopt($ch, CURLOPT_URL, $subPageUrl);
                                                curl_setopt($ch, CURLOPT_HEADER, 0);
                                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                                $html = curl_exec($ch);
                                                curl_close($ch);

                                                // using htmldomParser get content html
                                                $html = HtmlDomParser::str_get_html($html);
                                            }

                                            if (!empty($html)) {
                                                $html = HtmlDomParser::file_get_html($subPageUrl);
                                            }


                                            if ($html) {
                                                foreach ($html->find($chapterLinkOld) as $item) {
                                                    $chapterLinkHashPage = md5($item->href);

                                                    $chapLinkDataPage = array(
                                                        'id_books' => $idBook,
                                                        'source_url' => $item->href,
                                                        'hash_url' => $chapterLinkHashPage,
                                                        'created_at' => $now
                                                    );
                                                    if (!empty($chapLinkDataPage)) {
                                                        $insChapLinkPage = DB::table('chapterlinks')->insert($chapLinkDataPage);
                                                        if ($insChapLinkPage) {
                                                            $redis->sadd('sListChapter', $chapterLinkHashPage);
                                                        }
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                                // END HANDE PAGINATE
                                return response()->json(1, 200);
                            } else {
                                return response(0, 200);
                            }


                            // END HANDLE BOOK INFO

                        }

                    }
                }

            }
        }
    }

    // get link new chap index
    public function getLinkChapFromIndex(Request $request)
    {
        $redis = Redis::connection();
        $redis->del('sListBookUpdate');

        $xpathNewLinkQ = DB::table('xpath')->select('xpath_options')->where('id', 16)->first();

        if (!empty($xpathNewLinkQ)) {
            $xpathNewLink = $xpathNewLinkQ->xpath_options;
            if (!empty($xpathNewLink)) {
                $source_url = 'http://webtruyen.com/';
                $now = Carbon::now();

                if ($this->checkUrl($source_url) == true) {

                    try {
                        // get html by guzzle
                        $client = new \GuzzleHttp\Client();
                        $res = $client->request('GET', $source_url);
                        $body = $res->getBody();
                        $remainingBytes = $body->getContents();

                        // using htmldomParser get content html
                        $html = HtmlDomParser::str_get_html($remainingBytes);
                    } catch (exception $e) {
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
                        curl_setopt($ch, CURLOPT_URL, $source_url);
                        curl_setopt($ch, CURLOPT_HEADER, 0);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        $html = curl_exec($ch);
                        curl_close($ch);

                        // using htmldomParser get content html
                        $html = HtmlDomParser::str_get_html($html);
                    }

                    if (empty($html)) {
                        $html = HtmlDomParser::file_get_html($source_url);
                    }

                    if ($html) {
//                        $newChap = 0;
                        foreach ($html->find($xpathNewLink) as $k => $v) {
                            $href = $v->href;
                            $checkBookExists = $redis->sismember('sListBook', md5($href));
                            if ($checkBookExists == 0) {
                                $dataBook = $this->getBookInfo($href);

                                if (!empty($dataBook)) {

                                    $dataBookLink = array(
                                        'cat_id' => $dataBook['id_cat'],
                                        'source_url' => $href,
                                        'hash_url' => md5($href),
                                        'created_at' => $now
                                    );

                                    $insBookLink = DB::table('booklink')->insert($dataBookLink);
                                    if ($insBookLink) {
                                        $newChap = $this->scanNewChap($href);
                                        $redis->sadd('sListBook', md5($href));
                                        $redis->smembers('sListBook');
                                    }
                                }
                            } else {
                                $newChap = $this->scanNewChap($v->href);
                            }
                        }
                        return response()->json(1, 200);
                    }
                }
            }
        }
    }

    //get book info
    function getBookInfo($url)
    {
        if (!empty($url)) {
            if ($this->checkUrl($url) == true) {
                $redis = Redis::connection();
                try {
                    // get html by guzzle
                    $client = new \GuzzleHttp\Client();
                    $res = $client->request('GET', $url);
                    $body = $res->getBody();
                    $remainingBytes = $body->getContents();

                    // using htmldomParser get content html
                    $html = HtmlDomParser::str_get_html($remainingBytes);
                } catch (exception $e) {
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $html = curl_exec($ch);
                    curl_close($ch);

                    // using htmldomParser get content html
                    $html = HtmlDomParser::str_get_html($html);
                }

                if (empty($html)) {
                    $html = HtmlDomParser::file_get_html($url);
                }

                if ($html) {
                    //GET CONFIG XPATH
                    $authorXpath = DB::table('xpath')->select('id', 'xpath_name', 'xpath_options')->where('id', 4)->first();
                    $descriptionsXpath = DB::table('xpath')->select('id', 'xpath_name', 'xpath_options')->where('id', 3)->first();

                    $bookNameXpath = DB::table('xpath')->select('id', 'xpath_name', 'xpath_options')->where('id', 11)->first();
                    $BookThumbnailXpath = DB::table('xpath')->select('id', 'xpath_name', 'xpath_options')->where('id', 12)->first();

                    $catXpath = DB::table('xpath')->select('id', 'xpath_name', 'xpath_options')->where('id', 15)->first();

                    $realeaseXpath = DB::table('xpath')->select('id', 'xpath_name', 'xpath_options')->where('id', 17)->first();
                    // END GET CONFIG PATH

                    // GET LIST DATA EXITS
                    $listAuthor = DB::table('author')->select('author_name')->get();

                    foreach ($listAuthor as $k => $v) {
                        $auNameEncode = md5($v->author_name);
                        $redis->sadd('sAuthorName', $auNameEncode);
                    }

                    $listChap = DB::table('chapterlinks')->select('hash_url')->get();

                    foreach ($listChap as $key => $Hashlc) {
                        $redis->sadd('sListChapter', $Hashlc->hash_url);
                    }

                    $listBook = DB::table('books')->select('slug')->get();

                    foreach ($listBook as $kBook => $vBook) {
                        $hashSlug = md5($vBook->slug);
                        $redis->sadd('sListBook', $hashSlug);
                    }


                    // END LIST DATA EXIT

                    $catId = '';
                    foreach ($html->find($catXpath->xpath_options) as $k => $items) {
                        $catIdQ = DB::table('categories')->select('id', 'name', 'slug')->where('name', 'like', '%' . $items->innertext() . '%')->first();
                        if (!empty($catIdQ)) {
                            $catId = $catIdQ->id;
                            break;
                        }
                    }

                    // CREATE VALUE
                    $bookInfo = (object)[
                        'slug' => $url,
                        'id_cat' => $catId
                    ];

                    // GET AUTHOR
                    $authorXpath = $authorXpath->xpath_options;

                    foreach ($html->find($authorXpath) as $author) {

                        $authorName = strip_tags($author->innertext);
                        $authorSlug = $author->href;
                        $checkExits = $redis->SISMEMBER('sAuthorName', md5($authorName));

                        if ($checkExits == 0) {
                            $now = Carbon::now();
                            $idAuthor = DB::table('author')->insertGetId(['author_name' => $authorName, 'slug' => $authorSlug, 'created_at' => $now]);
                            $bookInfo->author = $idAuthor;

                            $authorNameEnc = md5($authorName);
                            $redis->sadd('sAuthorName', $authorNameEnc);
                        } else {
                            $dataAuthor = DB::table('author')->select('author_name', 'id')->where('author_name', 'like', '%' . $authorName . '%')->orderBy('id', 'asc')->first();
                            $bookInfo->author = $dataAuthor->id;

                            $authorNameEnc = md5($authorName);
                            $redis->sadd('sAuthorName', $authorNameEnc);
                        }
                    }
                    // END GET AUTHOR

                    // GET DESCRIPTIONS
                    $descriptionsXpath = $descriptionsXpath->xpath_options;

                    foreach ($html->find($descriptionsXpath) as $descript) {
                        $bookInfo->descriptions = strip_tags($descript->innertext());
                    }
                    // END GET DESCRIPTIONS

                    // GET BOOK THUMBNAIL
                    $BookThumbnailXpath = $BookThumbnailXpath->xpath_options;

                    foreach ($html->find($BookThumbnailXpath) as $thumbnail) {
                        $bookInfo->bookThumbnail = $thumbnail->src;
                    }
                    // END GET BOOK THUMBNAIL

                    // GET BOOK NAME
                    $bookNameXpath = $bookNameXpath->xpath_options;
                    $checkBookExist = 0;
                    $bookUrl = '';
                    foreach ($html->find($bookNameXpath) as $bookName) {
                        $bookUrl = $bookName->href;
                        $hashBookSlug = md5($bookName->href);
                        $checkBookExist = $redis->SISMEMBER('sListBook', $hashBookSlug);
                        $bookInfo->bookName = trim($bookName->innertext());
                    }
                    // END GET BOOK NAME

                    // GET RELEASE STATUS
                    foreach ($html->find($realeaseXpath->xpath_options) as $releaseStatus) {
                        $bookInfo->status = trim(strip_tags($releaseStatus->children(3)->innertext()));
                    }
                    // END RELEASE STATUS

                    // END CREATE VALUES

                    //HANDLE BOOK INFO
                    if ($checkBookExist == 0) {
                        if ($bookInfo) {
                            $now = Carbon::now();
                            $arrIns = array(
                                'slug' => $bookInfo->slug,
                                'id_cat' => $bookInfo->id_cat,
                                'id_author' => $bookInfo->author,
                                'description' => $bookInfo->descriptions,
                                'name' => $bookInfo->bookName,
                                'releaseStatus' => $bookInfo->status,
                                'images' => $bookInfo->bookThumbnail,
                                'status' => 1,
                                'created_at' => $now
                            );

                            $insertIdBook = DB::table('books')->insertGetId($arrIns);
                            if ($insertIdBook) {
                                return $arrIns;
                            }

                        }
                    }
                }
            }
        }
    }

    // scan new chap
    function scanNewChap($url)
    {
        if (!empty($url)) {
            $checkUrl = $this->checkUrl($url);
            $hashUrl = md5($url);

            if ($checkUrl == true) {
                $redis = Redis::connection();
                try {
                    // get html by guzzle
                    $client = new \GuzzleHttp\Client();
                    $res = $client->request('GET', $url);
                    $body = $res->getBody();
                    $remainingBytes = $body->getContents();

                    // using htmldomParser get content html
                    $html = HtmlDomParser::str_get_html($remainingBytes);
                } catch (exception $e) {
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $html = curl_exec($ch);
                    curl_close($ch);

                    // using htmldomParser get content html
                    $html = HtmlDomParser::str_get_html($html);
                }

                if (empty($html)) {
                    $html = HtmlDomParser::file_get_html($url);
                }

                if ($html) {
                    $xpath_newChap = DB::table('xpath')->select('xpath_options')->where('id', 6)->first();

                    if (!empty($xpath_newChap)) {
                        $now = Carbon::now();
                        foreach ($html->find($xpath_newChap->xpath_options) as $newChapItems) {
                            $newChap = $newChapItems->href;
                            $hashChap = md5($newChapItems->href);

                            $checkChapExists = $redis->sismember('sListChapter', $hashChap);

                            if ($checkChapExists == 0) {
                                $id_book = DB::table('books')->select('id')->where('slug', '=', $url)->first();
                                if ($id_book !== null) {
                                    $insNewChapData = array(
                                        'created_at' => $now,
                                        'source_url' => $newChap,
                                        'hash_url' => $hashChap,
                                        'chapName' => trim($newChapItems->innertext()),
                                        'id_books' => $id_book->id
                                    );

                                    $insChap = DB::table('chapterlinks')->insert($insNewChapData);
                                    if ($insChap) {
                                        $redis->sadd('sListChapter', $hashChap);
                                    }
                                } else {
                                    /** Cat ID **/
                                    $xpathGetCat = DB::table('xpath')->select('xpath_options')->where('id', 15)->first();
                                    if (!empty($xpathGetCat)) {
                                        foreach ($html->find($xpathGetCat->xpath_options) as $catItemsName) {
                                            $catUrl = str_replace('http://webtruyen.com/', '', $catItemsName->href);
                                            if (!empty($catUrl)) {
                                                $catIdQ = DB::table('categories')->select('id')->where('slug', '=', $catUrl)->first();
                                                if (!empty($catIdQ->id)) {
                                                    $catId = $catIdQ->id;
                                                }
                                            }
                                        }
                                    }

                                    /** Author ID **/
                                    $xpath_Author = DB::table('xpath')->select('xpath_options')->where('id', 4)->first();
                                    if (!empty($xpath_Author)) {
                                        foreach ($html->find($xpath_Author->xpath_options) as $authorItem) {
                                            $authorSlug = $authorItem->href;
                                            if (!empty($authorSlug)) {
                                                $authorIdQ = DB::table('author')->select('id')->where('slug', '=', $authorSlug)->first();
                                                if (!empty($authorIdQ->id)) {
                                                    $authorId = $authorIdQ->id;
                                                } else {
                                                    $authorArrData = array(
                                                        'author_name' => trim($authorItem->innertext()),
                                                        'slug' => trim($authorSlug),
                                                        'created_at' => $now
                                                    );
                                                    if ($authorArrData) {
                                                        $insAuthor = DB::table('author')->insertGetId($authorArrData);
                                                        if ($insAuthor) {
                                                            $authorId = $insAuthor;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }


                                }

                            }

                        }
                        return 1;
                    }
                }
            }
        }
    }

    // get config index
    static function getindexConfig()
    {
        /** New Link Xpath **/
        $xpathNewLinkQ = DB::table('xpath')->select('xpath_options')->where('id', 16)->first();
        /** New chap Xpath **/
        $xpath_newChap = DB::table('xpath')->select('xpath_options')->where('id', 6)->first();
        /** Category xpath **/
        $xpathGetCat = DB::table('xpath')->select('xpath_options')->where('id', 15)->first();
        /** Author ID **/
        $xpath_Author = DB::table('xpath')->select('xpath_options')->where('id', 4)->first();
        /** Release Status **/
        $release_Status = DB::table('xpath')->select('xpath_options')->where('id', 17)->first();

        $arrRet = array(
            'xpathNewLinkQ' => $xpathNewLinkQ,
            'xpath_newChap' => $xpath_newChap,
            'xpathGetCat' => $xpathGetCat,
            'release_Status' => $release_Status,
            'xpath_author' => $xpath_Author
        );

        return $arrRet;
    }

    function getindexByCron($configSetting)
    {

        $redis = Redis::connection();
        $redis->del('sListBookUpdate');

        $xpathNewLinkQ = $configSetting['xpathNewLinkQ'];
        $xpath_newChap = $configSetting['xpath_newChap'];
        $xpathGetCat = $configSetting['xpathGetCat'];
        $xpath_Author = $configSetting['xpath_author'];

        if (!empty($xpathNewLinkQ)) {
            $xpathNewLink = $xpathNewLinkQ->xpath_options;

            if (!empty($xpathNewLink)) {
                $source_url = 'http://webtruyen.com/';
                $now = Carbon::now();

                if ($this->checkUrl($source_url) == true) {
                    try {
                        // get html by guzzle
                        $client = new \GuzzleHttp\Client();
                        $res = $client->request('GET', $source_url);
                        $body = $res->getBody();
                        $remainingBytes = $body->getContents();

                        // using htmldomParser get content html
                        $html = HtmlDomParser::str_get_html($remainingBytes);
                    } catch (exception $e) {
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
                        curl_setopt($ch, CURLOPT_URL, $source_url);
                        curl_setopt($ch, CURLOPT_HEADER, 0);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        $html = curl_exec($ch);
                        curl_close($ch);

                        // using htmldomParser get content html
                        $html = HtmlDomParser::str_get_html($html);
                    }

                    if (empty($html)) {
                        $html = HtmlDomParser::file_get_html($source_url);
                    }

                    if ($html) {

                        foreach ($html->find($xpathNewLink) as $k => $v) {
                            $href = $v->href;
                            $checkBookExists = $redis->sismember('sListBook', md5($href));
                            if ($checkBookExists == 0) {
                                $dataBook = $this->getBookInfo($href);

                                if (!empty($dataBook)) {

                                    $dataBookLink = array(
                                        'cat_id' => $dataBook['id_cat'],
                                        'source_url' => $href,
                                        'hash_url' => md5($href),
                                        'created_at' => $now,
                                        'status'=>1,
                                    );

                                    $insBookLink = DB::table('booklink')->insert($dataBookLink);
                                    if ($insBookLink) {
                                        $newChap = $this->scanNewChapByCron($href, $xpath_newChap, $xpathGetCat, $xpath_Author);
                                        $redis->sadd('sListBook', md5($href));
                                        $redis->smembers('sListBook');
                                    }
                                }
                            } else {
                                $newChap = $this->scanNewChapByCron($v->href, $xpath_newChap, $xpathGetCat, $xpath_Author);
                            }
                        }
                    }
                    /** End foreach **/
                }
            }
        }
    }

    // scanNewChapByCron
    function scanNewChapByCron($url, $xpath_newChap, $xpathGetCat, $xpath_Author)
    {
        if (!empty($url)) {
            $checkUrl = $this->checkUrl($url);
            $hashUrl = md5($url);
            $now = Carbon::now();

            if ($checkUrl == true) {
                $redis = Redis::connection();
                try {
                    // get html by guzzle
                    $client = new \GuzzleHttp\Client();
                    $res = $client->request('GET', $url);
                    $body = $res->getBody();
                    $remainingBytes = $body->getContents();

                    // using htmldomParser get content html
                    $html = HtmlDomParser::str_get_html($remainingBytes);
                } catch (exception $e) {
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $html = curl_exec($ch);
                    curl_close($ch);

                    // using htmldomParser get content html
                    $html = HtmlDomParser::str_get_html($html);
                }

                if (empty($html)) {
                    $html = HtmlDomParser::file_get_html($url);
                }

                if ($html) {
                    if (!empty($xpath_newChap)) {
                        foreach ($html->find($xpath_newChap->xpath_options) as $newChapItems) {
                            $newChap = $newChapItems->href;
                            $hashChap = md5($newChapItems->href);

                            $checkChapExists = $redis->sismember('sListChapter', $hashChap);

                            if ($checkChapExists == 0) {
                                $id_book = DB::table('books')->select('id')->where('slug', '=', $url)->first();
                                if ($id_book !== null) {
                                    $insNewChapData = array(
                                        'created_at' => $now,
                                        'source_url' => $newChap,
                                        'hash_url' => $hashChap,
                                        'chapName' => trim($newChapItems->innertext()),
                                        'id_books' => $id_book->id
                                    );

                                    $insChap = DB::table('chapterlinks')->insert($insNewChapData);
                                    if ($insChap) {
                                        $redis->sadd('sListChapter', $hashChap);
                                    }
                                } else {

                                    if (!empty($xpathGetCat)) {
                                        foreach ($html->find($xpathGetCat->xpath_options) as $catItemsName) {
                                            $catUrl = str_replace('http://webtruyen.com/', '', $catItemsName->href);
                                            if (!empty($catUrl)) {
                                                $catIdQ = DB::table('categories')->select('id')->where('slug', '=', $catUrl)->first();
                                                if (!empty($catIdQ->id)) {
                                                    $catId = $catIdQ->id;
                                                }
                                            }
                                        }
                                    }


                                    if (!empty($xpath_Author)) {
                                        foreach ($html->find($xpath_Author->xpath_options) as $authorItem) {
                                            $authorSlug = $authorItem->href;
                                            if (!empty($authorSlug)) {
                                                $authorIdQ = DB::table('author')->select('id')->where('slug', '=', $authorSlug)->first();
                                                if (!empty($authorIdQ->id)) {
                                                    $authorId = $authorIdQ->id;
                                                } else {
                                                    $authorArrData = array(
                                                        'author_name' => trim($authorItem->innertext()),
                                                        'slug' => trim($authorSlug),
                                                        'created_at' => $now
                                                    );
                                                    if ($authorArrData) {
                                                        $insAuthor = DB::table('author')->insertGetId($authorArrData);
                                                        if ($insAuthor) {
                                                            $authorId = $insAuthor;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        return 1;
                    }
                }
            }
        }
    }


    //get book config
    static function getbookConfig()
    {
        $redis = Redis::connection();
        $redis->del('sListChapter');

        $bookDataQ = DB::table('books')
            ->select('id', 'id_cat', 'id_author', 'paginate_left', 'status', 'slug')
            ->where('status', 1)
            ->where('paginate_left', '>', 0)
            ->orderBy('id', 'asc')
            ->first();

        $bookData = $bookDataQ;
        dump($bookDataQ->slug);

        $listChap = DB::table('chapterlinks')->select('source_url')->get();

        if (!empty($listChap)) {
            foreach ($listChap as $key => $Hashlc) {
                $redis->sadd('sListChapter', $Hashlc->source_url);
            }
        }


        return $bookDataQ;
    }

    function getChapterLinkByidBookCron($configs)
    {

        if (!empty($configs)) {
            $redis = Redis::connection();

            $source_url = $configs->slug;
            $catId = $configs->id_cat;
            $idBook = $configs->id;
            $paginate = $configs->paginate_left;

            if ($paginate > 0) {
                try {
                    // get html by guzzle
                    $client = new \GuzzleHttp\Client();
                    $res = $client->request('GET', $source_url . '/' . $paginate);
                    $body = $res->getBody();
                    $remainingBytes = $body->getContents();

                    // using htmldomParser get content html
                    $html = HtmlDomParser::str_get_html($remainingBytes);
                } catch (exception $e) {
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
                    curl_setopt($ch, CURLOPT_URL, $source_url . '/' . $paginate);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $html = curl_exec($ch);
                    curl_close($ch);

                    // using htmldomParser get content html
                    $html = HtmlDomParser::str_get_html($html);
                }

                if (empty($html)) {
                    $html = HtmlDomParser::file_get_html($source_url . '/' . $paginate);
                }

                if ($html) {
                    //HANDLE BOOK INFO
                    if (!empty($idBook)) {
                        $now = Carbon::now();
                        dump('fullURI: ' . $source_url . '/' . $paginate);

                        $chapterLinkOld = "#divtab h4 a";

                        if (!empty($chapterLinkOld)) {
                            foreach ($html->find($chapterLinkOld) as $chapOld) {
                                $chapLinkPage1Hash = md5($chapOld->href);

                                dump('Chap page:' . $chapOld->href);

                                $checkChap1Exists = $redis->SISMEMBER('sListChapter', $chapOld->href);
                                if ($checkChap1Exists == 0) {
                                    $chapLinkPage1 = array(
                                        'id_books' => $idBook,
                                        'source_url' => $chapOld->href,
                                        'hash_url' => $chapLinkPage1Hash,
                                        'chapName' => trim($chapOld->innertext()),
                                        'created_at' => $now
                                    );
                                    if (!empty($chapOld->href)) {
                                        $insChapLinkPage1 = DB::table('chapterlinks')->insert($chapLinkPage1);

                                        if ($insChapLinkPage1) {
                                            $redis->sadd('sListChapter', $chapLinkPage1Hash);
                                        }
                                    }
                                }
                            }
                            if ($paginate > 0) {
                                $updateBookPaginateLeft = DB::table('books')->where('id', $idBook)->update(['paginate_left' => ($paginate - 1)]);
                            } else {
                                $updateBookPaginateLeft = DB::table('books')->where('id', $idBook)->update(['status' => 0]);
                            }
                        }
                    } else {
                        return "không thuộc sách nào lỗi";
                    }
                }

            }
        }
    }

    //check url return response() 200 status
    function checkUrl($url)
    {
        if (!empty($url)) {
            try {
                $client = new \GuzzleHttp\Client();
                $res = $client->get($url, ['exceptions' => false]);
                $status = $res->getStatusCode();
            } catch (Exception $ex) {
                $status = @get_headers($url);
            }
            if ($status == '200') return true;
            else return false;
        }
    }

    //add array link return a Array()
    public function addArrayLink($arrLinkBook, $catId)
    {
        if (!empty($arrLinkBook)) {
            $arrRet = [];
            foreach ($arrLinkBook as $k => $v) {
                $now = Carbon::now();
                $arrRet[] = array(
                    'cat_id' => $catId,
                    'source_url' => trim($v),
                    'hash_url' => md5(trim($v)),
                    'created_at' => $now
                );
            }
            if ($arrRet) {
                return $arrRet;
            }
        }
    }

    // add array List link chap one shot story
    public function addListLinkChap($arrListLink)
    {
        if (!empty($arrListLink)) {
            $arrRet = [];
            /* $idBook = 1 default is Tong hop */
            $idBook = 1;
            foreach ($arrListLink as $k => $v) {
                $now = Carbon::now();
                $arrRet[] = array(
                    'source_url' => trim($v),
                    'hash_url' => md5(trim($v)),
                    'id_books' => $idBook,
                    'created_at' => $now
                );
            }

            if ($arrRet) {
                return $arrRet;
            }
        }
    }

}
