<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Sunra\PhpSimple\HtmlDomParser;
use Illuminate\Support\Facades\Redis;
use App\Jobs\getPaginateItem;


class getBookLinkController extends Controller
{
    //

    function getBookLinkByCategory()
    {
        $categoryConfig = DB::table('categories')
            ->select('id', 'slug', 'paginate_left')
            ->where('status', 1)
            ->whereNull('deleted_at')->first();


        if (!empty($categoryConfig)) {
            $idCategories = $categoryConfig->id;
            $slug = $categoryConfig->slug;
            $paginate_left = $categoryConfig->paginate_left;


            $fullUrl = 'http://webtruyen.com/' . $slug;

            $arrPostCatLink = array(
                'paginate' => $paginate_left,
                'catLink' => $fullUrl,
                'idCat' => $idCategories
            );

            if ($this->checkUrl($fullUrl) == true) {
                if ($paginate_left > 1) {
                    $updateCategoryPaginate = DB::table('categories')
                        ->where('id', $idCategories)
                        ->update(['paginate_left' => ($paginate_left - 1)]);

                    getPaginateItem::dispatch($arrPostCatLink)->onQueue('getPaginateItem');
                } else {
                    $updateCategoryPaginate = DB::table('categories')->where('id', $idCategories)->update(['status' => 0]);
                }
            }

        }
    }


    function getPaginateItem($arrData)
    {

        if (!empty($arrData)) {
            $paginate = $arrData['paginate'];
            $catLink = $arrData['catLink'];
            $idCat = $arrData['idCat'];

            $linkCategory = $catLink . '/' . $paginate;

            dump('url: ' . $linkCategory);

            $redis = Redis::connection();

            $dom = HtmlDomParser::file_get_html($linkCategory);


            $listLinkApp = DB::table('booklink')->select('source_url')->get();

            if (!empty($listLinkApp)) {
                foreach ($listLinkApp as $k => $v) {
                    $redis->sadd('booklink', $v->source_url);
                }
            }
            $redis->smembers('booklink');


            $xpathPostLinkPage1 = '.row-info a';

            foreach ($dom->find($xpathPostLinkPage1) as $items) {
                $checkExits = $redis->SISMEMBER('booklink', $items->href);

                if ($checkExits == 0) {
                    $redis->sadd('InsertBooklinkPaginate', $items->href);
                }
            }

            $arrItem = $redis->smembers('InsertBooklinkPaginate');
            if (!empty($arrItem) || $arrItem !== null) {
                $now = Carbon::now();
                $arrInsert = array();
                foreach ($arrItem as $k => $v) {
                    $arrInsert[$k]['created_at'] = $now;
                    $arrInsert[$k]['source_url'] = $v;
                    $arrInsert[$k]['hash_url'] = md5($v);
                    $arrInsert[$k]['status'] = 1;
                    $arrInsert[$k]['cat_id'] = $idCat;
                    DB::table('booklink')->insert($arrInsert[$k]);
                }
                $redis->del('InsertBooklinkPaginate');
            }

        }
    }

    //check url return response() 200 status
    function checkUrl($url)
    {
        if (!empty($url)) {
            try {
                $client = new \GuzzleHttp\Client();
                $res = $client->get($url, ['exceptions' => false]);
                $status = $res->getStatusCode();
            } catch (Exception $ex) {
                $status = @get_headers($url);
            }
            if ($status == '200') return true;
            else return false;
        }
    }
}
