<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class XpathController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }
    //
    public function index(Request $request){
        $xpathDataQuery = DB::table('xpath as x')
            ->select('x.id','x.xpath_name','x.xpath_options', DB::raw("DATE_FORMAT(x.created_at, '%d-%m-%Y %H:%i:%s') as created_at"))
            ->whereNull('x.deleted_at');
        $xpathData['data'] = $xpathDataQuery->orderBy('x.id','desc')->get()->toArray();

        if ($request->ajax()){
            return response()->json($xpathData,200);

        }
        return view('admins.xpath.index',['title'=>'Xpath Manage']);
    }

    public function getXpathByid(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data['id'];
            if(!empty($id)){
                $selectData = DB::table('xpath')->select('id','xpath_name','xpath_options')->where('id',$id)->first();
                if(!empty($selectData)){
                    return response()->json($selectData,200);
                }
            }
        }
    }

    public function updateXpathByid(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data['id'];
            $now = Carbon::now();
            if(!empty($id)){
                $arrUpdate = array(
                    'xpath_options'=>$data['xpathOptions'],
                    'xpath_name'=>$data['xpathName'],
                    'updated_at'=>$now
                );
                $update = DB::table('xpath')->where('id',$id)->update($arrUpdate);
                if($update){
                    return response()->json(1,200);
                }else{
                    return response()->json(0,200);
                }
            }
        }
    }

    public function deleteXpathByid(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data['id'];
            $now = Carbon::now();
            if(!empty($id)){
                $deleteData = DB::table('xpath')->where('id',$id)->update(['deleted_at'=>$now]);
                if($deleteData){
                    return response()->json(1,200);
                }else{
                    return response()->json(1,200);
                }
            }
        }
    }

    public function insertXpath(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $now = Carbon::now();
            $arrData = array(
                'xpath_name'=>$data['insXpathName'],
                'xpath_options'=>$data['insXpathOptions'],
                'created_at'=>$now
            );
            if(!empty($arrData)){
                $insertXpath = DB::table('xpath')->insert($arrData);
                if($insertXpath){
                    return response()->json(1,200);
                }else{
                    return response()->json(0,200);
                }
            }
        }
    }

    public function findXpathbyName(Request $request){
        $find = trim($request->q);
        if (empty($find)) {
            return response()->json([]);
        }

        $data = DB::table('xpath')
            ->select('id','xpath_name as name')
            ->where('xpath_name','like','%'.$find.'%')
            ->get();

        return response()->json($data);
    }
}
