<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

ini_set('max_execution_time', 300);
define('MAX_FILE_SIZE', 6000000);

class BookLinkController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }
    //
    public function index(Request $request){
        $bookDataQuery = DB::table('booklink as b')
            ->select('b.id','b.cat_id',
                'b.source_url',DB::raw("DATE_FORMAT(b.created_at, '%d-%m-%Y %H:%i:%s') as created_at"),
                'c.name as category')
            ->leftJoin('categories as c','c.id','=','b.cat_id')
            ->whereNull('b.deleted_at');

        $bookData['data'] = $bookDataQuery->orderBy('b.id','desc')->paginate(50);
        if ($request->ajax()){
//            if(!empty($bookData)){
//                return response()->json($bookData,200);
//            }else{
//                return response()->json([],200);
//            }
            return view('admins.bookLink.load',['bookLinkData'=>$bookData['data']]);
        }
        return view('admins.bookLink.index',['title'=>'Books Link Manage','bookLinkData'=>$bookData['data']]);
    }

    public function getBookLinkByid(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data['id'];
            if(!empty($id)){
                $dataBookLink = DB::table('booklink as b')
                    ->select('b.id','b.cat_id','b.source_url',DB::raw("DATE_FORMAT(b.created_at, '%d-%m-%Y %H:%i:%s') as created_at"),'c.name as category')
                    ->leftJoin('categories as c','c.id','=','b.cat_id')
                    ->where('b.id',$id)
                    ->first();
                if(!empty($dataBookLink)){
                    return response()->json($dataBookLink,200);
                }
            }
        }
    }

    public function updateBookLinkByid(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data['id'];
            if(!empty($id)){
                $now = Carbon::now();
                $dataUpdate = array(
                    'source_url'=>trim($data['source_url']),
                    'hash_url'=>trim(md5($data['source_url'])),
                    'cat_id'=>$data['cat_id'],
                    'updated_at'=>$now,
                    );
                if(!empty($dataUpdate)){
                    $update = DB::table('booklink')->where('id',$id)->update($dataUpdate);
                    if($update){
                        return response()->json(1,200);
                    }
                }
            }
        }
    }

    public function deleteBookLinkByid(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data['id'];
            if(!empty($id)){
                $now = Carbon::now();
                $delete = DB::table('booklink')->where('id',$id)->update(['deleted_at'=>$now]);
                if($delete){
                    return response()->json(1,200);
                }else{
                    return response()->json(0,200);
                }
            }
        }
    }

    public function insertBookLink(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $now = Carbon::now();
            $arrIns = array(
                'cat_id'=>trim($data['insSelectCat']),
                'source_url'=>trim($data['insInputSourceBook']),
                'hash_url'=>md5(trim($data['insInputSourceBook'])),
                'created_at'=>$now,
            );
            if(!empty($arrIns)){
                $insertBookLink = DB::table('booklink')->insert($arrIns);
                if($insertBookLink){
                    return response()->json(1,200);
                }else{
                    return response()->json(0,200);
                }
            }
        }
    }

//    public function findBookLinkbySource(Request $request){
//        $find = trim($request->q);
//        if (empty($find)) {
//            return response()->json([]);
//        }
//
//        $data = DB::table('booklink')
//            ->select('id','source_url as name')
//            ->where('source_url','like','%'.$find.'%')
//            ->get();
//
//        return response()->json($data);
//    }
}
