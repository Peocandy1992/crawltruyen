<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Sunra\PhpSimple\HtmlDomParser;
use Illuminate\Support\Facades\Redis;
use App\Jobs\getPaginateItem;

class getBookController extends Controller
{
    //

    function getBookDetailByBookLink()
    {
        $dataBookLink = DB::table('booklink')->select('id', 'cat_id', 'source_url')
            ->where('status',1)
            ->orderBy('id', 'asc')->first();

        if (!empty($dataBookLink)) {

            $id = $dataBookLink->id;
            $category_id = $dataBookLink->cat_id;
            $sourceUrl = $dataBookLink->source_url;

            if (!empty($sourceUrl)) {
                $html = HtmlDomParser::file_get_html($sourceUrl);

                if (!empty($html)) {

                    /** Get Xpath Configs */
                    $xpath_page = DB::table('xpath')->select('xpath_options')->where('id', 2)->first();
                    $xpath_paginate = $xpath_page->xpath_options;

                    /** Insert book Infomations */
                    $now = Carbon::now();

                    $arrBookIns = array(
                        'name' => '',
                        'slug' => $sourceUrl,
                        'description' => '',
                        'id_cat'=> $category_id,
                        'id_author' => '',
                        'created_at' => $now,
                        'releaseStatus' => '',
                        'paginate' => '',
                        'paginate_left' => '',
                        'status' => 1,
                    );


                    if (!empty($xpath_paginate)) {
                        $paginate = '1';
                        foreach ($html->find($xpath_paginate) as $paginate_items) {
                            $paginate = $paginate_items->innertext();
                            $paginate = str_replace('Trang 1 / ', '', $paginate);

                            $arrBookIns['paginate'] = $paginate;
                            $arrBookIns['paginate_left'] = $paginate;

                            break;
                        }
                    }

                    /** get Img thumbnail **/
                    $xpath_image = ".detail-thumbnail img";
                    foreach ($html->find($xpath_image) as $images) {
                        $arrBookIns['images'] = $images->src;
                        break;
                    }

                    /** get Name book **/
                    $xpath_name = ".detail-right > h1 a";
                    foreach ($html->find($xpath_name) as $name){
                        $arrBookIns['name'] = $name->innertext;
                    }

                    /** get description book **/
                    $xpath_description = ".summary";
                    foreach ($html->find($xpath_description) as $descriptions){
                        $arrBookIns['description'] = $descriptions->innertext;
                    }


                    /** get release status book **/
                    $xpath_release_status = ".detail-info .w3-ul span";
                    foreach ($html->find($xpath_release_status) as $status){
                        $arrBookIns['releaseStatus'] = $status->innertext;
                    }

                    /** get author book **/
                    $authorList = DB::table('author')->select('id','slug')->get();
                    if(!empty($authorList)){
                        foreach ($authorList as $k=>$v){

                        }
                    }

                    $authorUrl = '';
                    $authorName = '';
                    $xpath_author = ".detail-info > li h2 a";
                    foreach ($html->find($xpath_author) as $authorName){
                        $authorUrl = $authorName->href;
                        $authorName = $authorName->innertext;
                        break;
                    }

                    dump($authorName,$authorUrl);

                    $checkAuthorUrl = DB::table('author')->select('id','author_name')->where('author_name','like',$authorName)->first();

                    if(empty($checkAuthorUrl)){
                        $now = Carbon::now();
                        $arrAuthorIns = array(
                            'author_name' => $authorName,
                            'slug' => $authorUrl,
                            'created_at'=>$now
                        );

                        $authorId = DB::table('author')->insertGetId($arrAuthorIns);

                        $arrBookIns['id_author'] = $authorId;
                    }else{
                        $authorId = $checkAuthorUrl->id;
                        $arrBookIns['id_author'] = $authorId;
                    }
                    dump($arrBookIns);

                    try{
                        $insertBook = DB::table('books')->insert($arrBookIns);
                    }catch (\Exception $ex){
                       dump($ex);
                        $updateBookLink = DB::table('booklink')->where('id',$id)->update(['status'=>-1]);
                        return $ex;
                    }

                    $updateBookLink = DB::table('booklink')->where('id',$id)->update(['status'=>0]);

                    if(!empty($updateBookLink)){
                        return 'lay: '.$id;
                    }
                }
            }
        }
    }
}
