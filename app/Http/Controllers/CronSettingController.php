<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class cronSettingController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }
    //
    public function index(Request $request){
        $cronDataQuery = DB::table('cronsettings as cs')
            ->select('cs.id','cs.cron_name','cs.cron_configs as cron_setting','cs.level','x.xpath_options as xpath')
            ->leftJoin('xpath as x','x.id','=','cs.id_xpath')
            ->whereNull('cs.deleted_at');

        $cronData['data'] = $cronDataQuery->orderBy('cs.id','desc')->get()->toArray();

        if ($request->ajax()){
            return response()->json($cronData,200);

        }
        return view('admins.cronConfig.index',['title'=>'Cron Manage']);
    }

    public function editCronByid(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data['id'];
            if(!empty($id)){
                $now = Carbon::now();
                $arrData = array(
                    'cron_name'=>$data['cronName'],
                    'cron_configs'=>$data['cronSetting'],
                    'id_xpath'=>$data['xpath'],
                    'level'=>$data['level'],
                    'updated_at'=>$now
                );
                if(!empty($arrData)){
                    $updateDb = DB::table('cronsettings')->where('id',$id)->update($arrData);
                    if($updateDb){
                        return response()->json(1,200);
                    }else{
                        return response()->json(0,200);
                    }
                }

            }
        }
    }

    public function getCronByid(Request $request){
        $data = $request->all();
        if(!empty($data))
        {
            $id = $data['id'];
            if(!empty($id)){
                $getData = DB::table('cronsettings as cs')
                    ->select('cs.id_xpath','cs.cron_name','cs.id','cs.cron_configs','cs.level','x.xpath_name')
                    ->leftJoin('xpath as x','x.id','=','cs.id_xpath')
                    ->where('cs.id',$id)
                    ->first();

                if(!empty($getData)){
                    return response()->json($getData,200);
                }
            }
        }
    }

    public function deleteCronByid(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data['id'];
            if(!empty($id)){
                $now = Carbon::now();
                $deleteData = DB::table('cronsettings')->where('id',$id)->update(['deleted_at'=>$now]);
                if ($deleteData){
                    return response()->json(1,200);
                }else{
                    return response()->json(0,200);
                }
            }
        }
    }

    public function insertCronSetting(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $now = Carbon::now();
            $arrData = array(
                'cron_name'=>$data['cronName'],
                'cron_configs'=>$data['insinputCronSetting'],
                'level'=>$data['insinputLevel'],
                'id_xpath'=>$data['insselect_xpath'],
                'created_at'=>$now
            );
            if(!empty($arrData)){
                $insData = DB::table('cronsettings')->insert($arrData);
                if ($insData){
                    return response()->json(1,200);
                }else{
                    return response()->json(0,200);
                }
            }
        }
    }
}
