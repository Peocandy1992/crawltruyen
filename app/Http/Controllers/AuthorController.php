<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AuthorController extends Controller
{
    //
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $authorDataQuery = DB::table('author as a')
            ->select('a.author_name as name','a.id','a.slug')
            ->whereNull('a.deleted_at');

        $authorData['data'] = $authorDataQuery->orderBy('a.id','desc')->get()->toArray();
        if ($request->ajax()){
            return response()->json($authorData,200);

        }
        return view('admins.author.index',['title'=>'Author Manage']);
    }

    public function findAuthbyName(Request $request){
        $find = trim($request->q);
        if (empty($find)) {
            return response()->json([]);
        }
        $data = DB::table('author')
            ->select('id','author_name as name')
            ->where('author_name','like','%'.$find.'%')
            ->get();
        return response()->json($data);
    }

    public function getAuthorByid(Request $request){
        $data = $request->all();

        if(!empty($data)){
            $id = $data['id'];
            if(!empty($id)){
                $dataAuthor = DB::table('author')->select('id','author_name as name','slug')->where('id',$id)->first();

                if(!empty($dataAuthor)) {
                    return response()->json($dataAuthor, 200);
                }
            }
        }
    }

    public function updateAuthor(Request $request){
        $data = $request->all();

        if(!empty($data)){
            $id = $data['id'];
            if(!empty($id)){
                $now = Carbon::now();
                $arrData = array(
                    'author_name'=>$data['authorname'],
                    'slug'=>$data['authorslug'],
                    'updated_at'=>$now
                );
                if($arrData){
                    $update = DB::table('author')->where('id',$id)->update($arrData);
                    if($update){
                        return response()->json(1,200);
                    }else{
                        return response()->json(0,200);
                    }

                }
            }
        }
    }

    public function deleteAuthorByid(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data['id'];
            if(!empty($id)){
                $now = Carbon::now();
                $delete = DB::table('author')->where('id',$id)->update(['deleted_at'=>$now]);
                if($delete){
                    return response()->json(1,200);
                }else{
                    return response()->json(0,200);
                }
            }
        }
    }

    public function insertAuthor(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $now = Carbon::now();
            $arrIns = array(
                'author_name'=>trim($data['insAuthorname']),
                'slug'=>trim($data['insauthorslug']),
                'created_at'=>$now
            );
            if(!empty($arrIns)){
                $insertAuth = DB::table('author')->insert($arrIns);
                if($insertAuth){
                    return response()->json(1,200);
                }else{
                    return response()->json(0,200);
                }
            }
        }
    }
}
