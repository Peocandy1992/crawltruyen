<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }
    //
    public function index(Request $request){
        $bookDataQuery = DB::table('books as b')
            ->select('b.id','b.name','b.slug','b.description',DB::raw("DATE_FORMAT(b.created_at, '%d-%m-%Y %H:%i:%s') as created_at"),'c.name as category','a.author_name as author_name','b.images')
            ->leftJoin('categories as c','c.id','=','b.id_cat')
            ->leftJoin('author as a','a.id','=','b.id_author')
            ->whereNull('b.deleted_at');

        $bookData['data'] = $bookDataQuery->orderBy('b.id','desc')->get()->toArray();

        if ($request->ajax()){
            return response()->json($bookData,200);

        }
        return view('admins.book.index',['title'=>'Books Manage']);
    }

    public function getBookByid(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $bookDataQuery = DB::table('books as b')
                ->select('b.id','b.name','b.slug','b.description',DB::raw("DATE_FORMAT(b.created_at, '%d-%m-%Y %H:%i:%s') as created_at"),'c.name as category','c.id as catID','a.author_name as author_name','b.id_author','b.images')
                ->leftJoin('categories as c','c.id','=','b.id_cat')
                ->leftJoin('author as a','a.id','=','b.id_author')
                ->whereNull('b.deleted_at')
                ->where('b.id',$data['id']);

            $bookData = $bookDataQuery->first();
            if(!empty($bookData)){
                return response()->json($bookData,200);
            }
        }

    }

    public function editBookByid(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data['id'];
            $now = Carbon::now();

            $arrayData = array(
                'name'=>$data['bookname'],
                'slug'=>$data['bookslug'],
                'description'=>$data['description'],
                'id_cat'=>$data['select_category'],
                'id_author'=>$data['select_author'],
                'updated_at' => $now
            );
            if($arrayData){
                $dataUpdate = DB::table('books')->where('id',$id)->update($arrayData);
                if($dataUpdate){
                    return response()->json(1,200);
                }else{
                    return response()->json(0,200);
                }
            }
        }
    }

    public function deleteBookByid(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data['id'];
            $now = Carbon::now();
            if($id){
                $arrDelete = array(
                    'deleted_at'=>$now,
                );
                if($arrDelete){
                    $delete = DB::table('books')->where('id',$id)->update($arrDelete);
                    if($delete){
                        return response()->json(1,200);
                    }else{
                        return response()->json(0,200);
                    }
                }
            }
        }
    }

    public function insertBook(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $now = Carbon::now();
            $arrayData = array(
                'name'=>$data['name'],
                'slug'=>$data['slug'],
                'description'=>$data['descriptions'],
                'id_cat'=>$data['id_cat'],
                'created_at'=>$now
            );
            if(!empty($arrayData)){
                $insertDb = DB::table('books')->insert($arrayData);
                if($insertDb){
                    return response()->json(1,200);
                }else{
                    return response()->json(0,200);
                }
            }
        }
    }

    public function findBookbyName(Request $request)
    {
        $find = trim($request->q);
        if (empty($find)) {
            return response()->json([]);
        }

        $data = DB::table('books')
            ->select('id','name')
            ->where('name','like','%'.$find.'%')
            ->get();

        return response()->json($data);
    }

    public function previewBook(Request $request){
        $data = $request->all();
//        $redis = \Redis::connection();
//        $redis->del('');
        if(!empty($data)){
            $id = $data['id'];
            if(!empty($id)){
                $listChap = DB::table('chapterdetail')->select('chapName','chapContent','source_url')->where('id_book',$id)
                    ->orderBy('id','asc')
                    ->get();


                if(!empty($listChap)){
                    return response()->json($listChap,200);
                }else{
                    return response()->json(0,200);
                }
            }
        }
    }
}
