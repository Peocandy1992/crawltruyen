<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Illuminate\Support\MessageBag;

class LoginController extends Controller
{
    //
    public function getLogin(){
        return view('admins.layout.login');
    }

    public function postLogin(Request $request){
        $rules = [
            'username' =>'required',
            'password' => 'required'
        ];
        $messages = [
            'username.required' => 'Email is required',
//            'email.email' => 'Email wrong format',
            'password.required' => 'Password is required',
//            'password.min' => 'Password must be at least 8 characters',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            $email = $request->username;
            $password = $request->password;
            if( Auth::attempt(['email' => $email, 'password' =>$password])) {
                return redirect()->intended('/');
            } else {
                $errors = new MessageBag(['errorlogin' => 'Email or Password wrong']);
                return redirect()->back()->withInput()->withErrors($errors);
            }
        }
    }

    public function getLogout(Request $request){
        Auth::logout();
        return redirect('/login');
    }
}
