<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Sunra\PhpSimple\HtmlDomParser;
use Illuminate\Support\Facades\Redis;
use GuzzleHttp\Client;

ini_set('max_execution_time', 0);


class getChapDetailController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    // getChapdetailByidChapLink
    public function getChapdetailByidChapLink(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data['id'];
            $now = Carbon::now();
            if(!empty($id)){
                $dataChapQ = DB::table('chapterlinks')->select('id_books','source_url','id','chapName')->where('id',$id);
                $updateStatus = $dataChapQ->update(['status'=>'-1']);

                $dataChap = $dataChapQ->first();
                if(!empty($dataChap)){
                    $idChapterLink = $dataChap->id;
                    $urlChapterLink = $dataChap->source_url;
                    $bookIdChapter = $dataChap->id_books;
                    $chapName = $dataChap->chapName;

                    if(!empty($urlChapterLink)){
                        $checkUrl = $this->checkUrl($urlChapterLink);
                        if($checkUrl == true){
                            try{
                                // get html by guzzle
                                $client = new \GuzzleHttp\Client();
                                $res = $client->request('GET', $urlChapterLink);
                                $body  = $res->getBody();
                                $remainingBytes = $body->getContents();

                                // using htmldomParser get content html
                                $html = HtmlDomParser::str_get_html($remainingBytes);
                            }catch (exception $e){
                                $ch = curl_init();
                                curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
                                curl_setopt($ch, CURLOPT_URL, $urlChapterLink);
                                curl_setopt($ch, CURLOPT_HEADER, 0);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                $html = curl_exec($ch);
                                curl_close($ch);

                                // using htmldomParser get content html
                                $html = HtmlDomParser::str_get_html($html);
                            }

                            if(!empty($html)){
                                $html = HtmlDomParser::file_get_html($urlChapterLink);
                            }

                            if ($html){
                                $xpathContent = DB::table('xpath')->select('id','xpath_options')->where('id',13)->first();

                                if(!empty($xpathContent)){
                                    foreach ($html->find($xpathContent->xpath_options) as $content){
                                        $text = $content->innertext;

                                        $contents = $this->delete_all_between('<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">','Function: require_once',$text);
                                        if(!empty($contents)){

                                            $insChapData = array(
                                                'id_link'=>$idChapterLink,
                                                'id_book'=>$bookIdChapter,
                                                'source_url'=>$urlChapterLink,
                                                'hash_url'=>md5($urlChapterLink),
                                                'chapContent'=>trim($contents),
                                                'chapName'=>$chapName,
                                                'created_at'=>$now,
                                            );
                                            if(!empty($insChapData)){
                                                $insertChap = DB::table('chapterdetail')->insert($insChapData);
                                                if($insertChap){
                                                    $dataChapQ->update(['status'=>1,'getTime'=>$now]);
                                                    return response()->json(1,200);
                                                }else{
                                                    $dataChapQ->update(['status'=>null]);
                                                    return response()->json(0,200);
                                                }
                                            }
                                        }
                                    }
                                }
                            }else{
                                $dataChapQ->update(['status'=>null]);
                                return response()->json(0,200);
                            }

                        }
                    }
                }
            }
        }
    }

    // getChapterDetail static
    public function getChapterDetailByChapLinkData($listChap){
        if(!empty($listChap)){
            $now = Carbon::now();
            $insertChap = null;
            foreach ($listChap as $k=>$dataChap){

                $idChapterLink = $dataChap->id;
                $urlChapterLink = $dataChap->source_url;
                $bookIdChapter = $dataChap->id_books;
                $chapName = $dataChap->chapName;

                $dataChapQ = DB::table('chapterlinks')->select('id_books','source_url','id','chapName')->where('id',$idChapterLink);
                $updateStatus = $dataChapQ->update(['status'=>'-1']);


                if(!empty($urlChapterLink)){
                    $checkUrl = $this->checkUrl($urlChapterLink);
                    if($checkUrl == true){
                        try{
                            // get html by guzzle
                            $client = new \GuzzleHttp\Client();
                            $res = $client->request('GET', $urlChapterLink);
                            $body  = $res->getBody();
                            $remainingBytes = $body->getContents();

                            // using htmldomParser get content html
                            $html = HtmlDomParser::str_get_html($remainingBytes);
                        }catch (exception $e){
                            $ch = curl_init();
                            curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
                            curl_setopt($ch, CURLOPT_URL, $urlChapterLink);
                            curl_setopt($ch, CURLOPT_HEADER, 0);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            $html = curl_exec($ch);
                            curl_close($ch);

                            // using htmldomParser get content html
                            $html = HtmlDomParser::str_get_html($html);
                        }

                        if(!empty($html)){
                            $html = HtmlDomParser::file_get_html($urlChapterLink);
                        }

                        if ($html){
                            $xpathContent = DB::table('xpath')->select('id','xpath_options')->where('id',13)->first();

                            if(!empty($xpathContent)){
                                foreach ($html->find($xpathContent->xpath_options) as $content){
                                    $text = $content->innertext;

                                    $contents = $this->delete_all_between('<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">','Function: require_once',$text);
                                    if(!empty($contents)){

                                        $insChapData = array(
                                            'id_link'=>$idChapterLink,
                                            'id_book'=>$bookIdChapter,
                                            'source_url'=>$urlChapterLink,
                                            'hash_url'=>md5($urlChapterLink),
                                            'chapContent'=>trim($contents),
                                            'chapName'=>$chapName,
                                            'created_at'=>$now,
                                        );
                                        if(!empty($insChapData)){
                                            $insertChap = DB::table('chapterdetail')->insertGetId($insChapData);

                                            if($insertChap){
                                                $dataChapQ->update(['status'=>1,'getTime'=>$now]);
                                            }else{
                                                $dataChapQ->update(['status'=>null]);
                                            }
                                        }
                                    }
                                }
                            }
                        }else{
                            $dataChapQ->update(['status'=>null]);
                        }
                    }
                }
            }
        }
    }

    //get chap detail by id book
    public function getChapDetailByidBook(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data['id'];
            if(!empty($id)){{
                $listChap = DB::table('chapterlinks')
                    ->select('id_books','source_url','id','chapName','id')
                    ->where('id_books',$id)
                    ->where('status','=',null)
                    ->get();

                if(!empty($listChap)){
                    $getChapDetail = $this->getChapterDetailByChapLinkData($listChap);

                    return 1;
                }
            }}
        }
    }


    //
    static function getChapDetailConf(){
        $dataChapQ = DB::table('chapterlinks')
            ->select('id_books','source_url','id','chapName')
            ->whereNull('getTime')
            ->orderBy('id','asc');

        $dataChap = $dataChapQ->first();

        dump($dataChap);

        $xpathContent = DB::table('xpath')->select('id','xpath_options')->where('id',13)->first();

        $arrData = array(
            'dataChap'=> $dataChap,
            'dataChapQ'=> $dataChapQ,
            'xpathContent'=> $xpathContent
        );
//        dump(json_encode($arrData));

        return $arrData;
    }

    function getChapDetailByCron($configs){
        $dataChap = $configs['dataChap'];
        $dataChapQ = $configs['dataChapQ'];
        $xpathContent = $configs['xpathContent'];

        $now = Carbon::now();

        if(!empty($dataChap)) {

            $idChapterLink = $dataChap->id;

            $urlChapterLink = $dataChap->source_url;
            $bookIdChapter = $dataChap->id_books;
            $chapName = $dataChap->chapName;

            if(!empty($urlChapterLink) && $urlChapterLink !== 0) {
                $checkUrl = $this->checkUrl($urlChapterLink);
                if ($checkUrl == true) {
                    try {
                        // get html by guzzle
                        $client = new \GuzzleHttp\Client();
                        $res = $client->request('GET', $urlChapterLink);
                        $body = $res->getBody();
                        $remainingBytes = $body->getContents();

                        // using htmldomParser get content html
                        $html = HtmlDomParser::str_get_html($remainingBytes);
                    } catch (exception $e) {
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
                        curl_setopt($ch, CURLOPT_URL, $urlChapterLink);
                        curl_setopt($ch, CURLOPT_HEADER, 0);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        $html = curl_exec($ch);
                        curl_close($ch);

                        // using htmldomParser get content html
                        $html = HtmlDomParser::str_get_html($html);
                    }

                    if (!empty($html)) {
                        $html = HtmlDomParser::file_get_html($urlChapterLink);
                    }

                    if ($html) {
                        if(!empty($xpathContent)){
                            foreach ($html->find($xpathContent->xpath_options) as $content){
                                $text = $content->innertext;

                                $contents = self::delete_all_between('<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">','Function: require_once',$text);
                                $contents = str_replace('<!-- ADS CHAPTER 1 <div class="ads-chapter-1"><script type="text/javascript" src="http://webtruyen.com/wp-admin/qt/qc.js"></script></div>-->','',$contents);

                                if(!empty($contents)){

                                    $insChapData = array(
                                        'id_link'=>$idChapterLink,
                                        'id_book'=>$bookIdChapter,
                                        'source_url'=>$urlChapterLink,
                                        'hash_url'=>md5($urlChapterLink),
                                        'chapContent'=>trim($contents),
                                        'chapName'=>$chapName,
                                        'created_at'=>$now,
                                    );
                                    if(!empty($insChapData)){
                                        $insertChap = DB::table('chapterdetail')->insert($insChapData);
                                        if($insertChap){
                                            $dataChapQ->update(['status'=>1,'getTime'=>$now]);
                                        }else{
                                            dump('insert loi');
                                            $dataChapQ->update(['status'=>1]);
                                        }
                                    }
                                }
                            }
                        }
                    }else{
                        dump('xpath loi');
                        $dataChapQ->update(['status'=>1]);
                    }
                }else{
                    dump('link k hop le');
                    DB::table('chapterlinks')->where('id',$idChapterLink)->update(['status'=>1]);
                }
            }else{
                dump('link  = 0');
                DB::table('chapterlinks')->where('id',$idChapterLink)->update(['status'=> 1]);
            }
        }
    }

    //check url return response() 200 status
    public function checkUrl($url){
        if(!empty($url)) {
            try{
                $client = new \GuzzleHttp\Client();
                $res = $client->get($url, ['exceptions' => false]);
                $status = $res->getStatusCode();
            }catch (Exception $ex){
                $status = @get_headers($url);
            }
            if ($status == '200') return true;
            else return false;
        }
    }

    function delete_all_between($beginning, $end, $string) {

      $content =   preg_replace('/'.$beginning.'[\s\S]+?'.$end.'/', '', $string);
      $content = str_replace('</p></div>','',$content);

      $arrRemove = array('&lt;!--','--&gt;','<!--</p>-->','<!--<p>-->','<!-- ADS CHAPTER 1 -->');
      $content = str_replace($arrRemove,array('','','','',''),$content);
      return $content;

    }
}
