<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }
    // index
    public function index(Request $request){
        $postCatQuery = DB::table('categories')
            ->select('id','name','slug', DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y %H:%i:%s') as created_at"))
            ->whereNull('deleted_at');

        $postCat['data'] = $postCatQuery->orderBy('id','desc')->get()->toArray();

        if ($request->ajax()){
            return response()->json($postCat,200);

        }
        return view('admins.category.index',['title'=>'Category Manage']);
    }

    public function getCatByid(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data["id"];
            if($id){
                $dataCate = DB::table('categories')->select('id','name')->where('id',$id)->first();
                if($dataCate){
                    return response()->json($dataCate,200);
                }
            }
        }
    }

    public function updateCateById(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data['id'];

            $now = Carbon::now();

            $array = array('name'=>$data['catname'],'slug'=>$data['slug'],'updated_at'=>$now);

            if($id){
                $update = DB::table('categories')->where('id',$id)->update($array);
                if($update){
                    return response()->json(1,200);
                }else{
                    return response()->json(0,200);
                }
            }
        }
    }

    public function deleteCatByid(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data['id'];
            if($id){
                $now = Carbon::now();
                $delete = DB::table('categories')->where('id',$id)->update(['deleted_at'=>$now]);
                if($delete){
                    return response()->json(1,200);
                }else{
                    return response()->json(0,200);
                }
            }
        }
    }

    public function insertCat(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $now = Carbon::now();
            $array = array(
                'name'=>trim($data['insertCatname']),
                'slug'=>trim($data['slug']),
                'created_at'=>$now
            );
            if($array){
                $insertCat = DB::table('categories')->insert($array);
                if($insertCat){
                    return response()->json(1,200);
                }else{
                    return response()->json(0,200);
                }
            }
        }
    }

    public function findCatbyName(Request $request){
        $find = trim($request->q);
        if (empty($find)) {
            return response()->json([]);
        }
        $data = DB::table('categories')
            ->select('id','name')
            ->where('name','like','%'.$find.'%')
            ->get();
        return response()->json($data);
    }
}
