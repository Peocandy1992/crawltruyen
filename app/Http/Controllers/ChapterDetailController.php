<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

ini_set('memory_limit', '2024M');

class ChapterDetailController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    //
    public function index(Request $request){
        $postCatQuery = DB::table('chapterdetail as cd')
            ->select('cd.id as id','cd.chapName'
                ,'cd.status'
//                ,'cd.chapContent'
                ,'cd.source_url','cd.hash_url',
//                ,'a.author_name',
                'b.name as book_name'
                ,DB::raw("DATE_FORMAT(cd.created_at, '%d-%m-%Y %H:%i:%s') as created_at")

            )
            ->leftJoin('books as b','b.id','=','cd.id_book')
            ->leftJoin('author as a','a.id','=','cd.author_id')
            ->whereNull('cd.deleted_at');

//        $postCat['data'] = $postCatQuery->orderBy('cd.id','desc')->get()->toArray();
        $postCat['data'] = $postCatQuery->orderBy('cd.id','desc')->paginate(50);

        if ($request->ajax()){
//            return response()->json($postCat,200);
            return view('admins.chapterDetail.load',['chapterDetailData'=>$postCat['data']]);
        }
        return view('admins.chapterDetail.index',['title'=>'Crawl Book | DashBoard','chapterDetailData'=>$postCat['data']]);
    }

    public function getChapterDetailByid(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data['id'];
            if(!empty($id)){
                $dataGet = DB::table('chapterdetail as cd')
                    ->select('cd.id','cd.chapName','cd.chapContent','cd.source_url','cd.hash_url','a.author_name','b.id as book_id','b.name as book_name',DB::raw("DATE_FORMAT(cd.created_at, '%d-%m-%Y %H:%i:%s') as created_at"))
                    ->leftJoin('books as b','b.id','=','cd.id_book')
                    ->leftJoin('author as a','a.id','=','cd.author_id')
                    ->where('cd.id',$id)->first();
                if(!empty($dataGet)){
                    return response()->json($dataGet,200);
                }
            }
        }
    }

    public function updateChapDetail(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data['id'];
            if(!empty($id)){
                $now = Carbon::now();
                $arrUpdate = array(
                    'chapName'=>trim($data['chapName']),
                    'source_url'=>trim($data['chapUrl']),
                    'hash_url'=>md5(trim($data['chapUrl'])),
                    'id_book'=>trim($data['book_id']),
                    'chapContent'=>trim($data['chapContent']),
//                    ''=>$data[''],
                    'updated_at'=>$now
                );
                if(!empty($arrUpdate)){
                    $Update = DB::table('chapterdetail')->where('id',$id)->update($arrUpdate);
                    if($Update){
                        return response()->json(1,200);
                    }else{
                        return response()->json(0,200);
                    }
                }
            }
        }
    }

    public function deleteChapdetail(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data['id'];
            if(!empty($id)){
                $now = Carbon::now();
                $deleteData = DB::table('chapterdetail')->where('id',$id)->update(['deleted_at'=>$now]);
                if($deleteData){
                    return response()->json(1,200);
                }else{
                    return response()->json(0,200);
                }
            }
        }
    }

    public function insertChapDetail(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $now = Carbon::now();
            $dataIns = array(
//                'id_link'=>$data[''],
                'id_book'=>$data['book_id'],
                'chapName'=>trim($data['chapName']),
                'chapContent'=>trim($data['chapContent']),
                'source_url'=>trim($data['chapUrl']),
                'hash_url'=>md5(trim($data['chapUrl'])),
                'created_at'=>$now
            );
            if(!empty($dataIns)){
                $insertData = DB::table('chapterdetail')->insert($dataIns);
                if($insertData){
                    return response()->json(1,200);
                }else{
                    return response()->json(0,200);
                }
            }
        }
    }
}
