<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class publishController extends Controller
{
    //

    public function publishByChapDetail(Request $request){
        $data = $request->all();
        if(!empty($data)){
            $id = $data['id'];
            if(!empty($id)){
                $dataPublish = DB::table('chapterdetail as c')
                    ->select('c.id','c.id_book','c.chapName as post_title_chap',
                        'c.chapContent as post_content_chap','b.name as post_title_book','b.images as img',
                        'b.description as post_content_book','b.id_cat','auth.author_name as post_author','b.releaseStatus as tw_status')
                    ->leftJoin('books as b','b.id','=','c.id_book')
                    ->leftJoin('author as auth','auth.id','=','b.id_author')
                    ->where('c.id',$id)->first();


                if(!empty($dataPublish)){
                    $idCat = $dataPublish->id_cat;
                    if(!empty($idCat)){
                        switch ($idCat){
                            case $idCat == 1:
                                $dataPublish->post_category = "2";
                                break;
                            case $idCat == 2:
                                $dataPublish->post_category = "10";
                                break;
                            case $idCat == 3:
                                $dataPublish->post_category = "3";
                                break;
                            case $idCat == 4:
                                $dataPublish->post_category = "11";
                                break;
                            case $idCat == 5:
                                $dataPublish->post_category = "4";
                                break;
                            case $idCat == 6:
                                $dataPublish->post_category = "12";
                                break;
                            case $idCat == 7:
                                $dataPublish->post_category = "5";
                                break;
                            case $idCat == 8:
                                $dataPublish->post_category = "13";
                                break;
                            case $idCat == 9:
                                $dataPublish->post_category = "6";
                                break;
                            case $idCat == 10:
                                $dataPublish->post_category = "14";
                                break;
                            case $idCat == 11:
                                $dataPublish->post_category = "7";
                                break;
                            case $idCat == 12:
                                $dataPublish->post_category = "15";
                                break;
                            case $idCat == 13:
                                $dataPublish->post_category = "8";
                                break;
                            case $idCat == 14:
                                $dataPublish->post_category = "1";
                                break;
                            case $idCat == 15:
                                $dataPublish->post_category = "2";
                                break;
                            case $idCat == 16:
                                $dataPublish->post_category = "2";
                                break;
                            case $idCat == 17:
                                $dataPublish->post_category = "9";
                                break;
                            default:
                                $dataPublish->post_category = $idCat;
                                break;
                        }
                    }
                    $dataPublish->post_type_book = "post";
                    $dataPublish->post_type_chap = "chap";
                    $dataPublish->post_status = "publish";

                    $releaseStatus = $dataPublish->tw_status;
                    if(empty($releaseStatus) || $releaseStatus == null){
                        $dataPublish->tw_status = "Đang cập nhật";
                    }else{
                        $dataPublish->tw_status = $releaseStatus;
                    }

                    $content = $dataPublish->post_content_chap;
                    if(!empty($content)){
                        $dataPublish->post_content = trim($this->contentHandle($content));
                    }


                    $client = new \GuzzleHttp\Client();

                    $url = 'http://doctruyenfull.net/API/api.php';

                    $res = $client->post(
                        $url,
                        [
                            'json' => $dataPublish
                        ]
                    );


                    $body = $res->getBody();
                    $content = $body->getContents();
                    $arrRes = json_decode($content);
                    if($res->getStatusCode() == 200){
                        if($arrRes->code == 1){
                            /** status = null is unpublish , 1 published , 0 deleted**/
                            DB::table('chapterdetail')->where('id',$id)->update(['status'=>1]);

                            return response()->json('1',200);
                        }else{
                            return response()->json("failed publish:".$arrRes->desc,200);
                        }
                    }else{
                        return response()->json('0',$res->getStatusCode());
                    }

                }

            }
        }
    }

    public function contentHandle($content){
        if(!empty($content)){
            $arrRemove = array('&lt;!--','--&gt;','<!--</p>-->','<!--<p>-->','<!-- ADS CHAPTER 1 -->');
            //$text = str_ireplace($arrRemove,'',$content);
            $text = str_replace($arrRemove,array('','','','',''),$content);
            return $text;
        }
    }
}
