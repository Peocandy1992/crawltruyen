<?php

namespace App\Console\Commands;

use App\Book;
use Illuminate\Console\Command;

use App\Jobs\getBookDetailByBookLinkJob;

class getBookByBookLink extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'book:getBookDetail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get book detail by book link';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $book = new Book();

        getBookDetailByBookLinkJob::dispatch($book->toArray())->onQueue('getBookDetail');
    }
}
