<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Chapterlink;
use App\Jobs\getChapLinkBook;

class getChaplinkById extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'book:getChap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get chap book by id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        $chapterLink = new Chapterlink();
        dump(getChapLinkBook::dispatch($chapterLink->toArray())->toQueue('getLinkChap'));
    }
}
