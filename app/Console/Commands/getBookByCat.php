<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\booklink;
use App\Jobs\getBookByCat as getByCat;

class getBookByCat extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:BookByCat {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get Book By Category';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $booklink = new booklink();
        $booklink->id = $this->argument('id');
        dump(getByCat::dispatch($booklink->toArray())->toQueue('getBookLink'));
    }
}
