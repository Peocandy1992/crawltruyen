<?php

namespace App\Console\Commands;

use App\booklink;
use App\Jobs\getBookLinkOldJob;
use Illuminate\Console\Command;

class getBookLinkOldCmd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bookLink:getOldBookLink';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Book Link from paginate';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        $bookLink = new booklink();

        getBookLinkOldJob::dispatch($bookLink->toArray())->onQueue('getBookLinkOld');
    }
}
