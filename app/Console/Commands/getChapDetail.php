<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Chapterdetail;
use App\Jobs\getChapDetail as getDetail;

class getChapDetail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:chapDetail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get chap detail by link';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $chapterDetail = new Chapterdetail();
        dump(getDetail::dispatch($chapterDetail->toArray())->toQueue('getChapDetail'));
    }
}
