<?php

namespace App\Console\Commands;

use App\Chapterlink;
use Illuminate\Console\Command;
use App\Jobs\scanIndex as scIndex;

class scanIndex extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'index:scan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scan new Chap index';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $chapterLink = new Chapterlink();

        dump(scIndex::dispatch($chapterLink->toArray())->toQueue('scanIndex'));
    }
}
