<?php

namespace App\Console;

use App\Jobs\getBookByBookLinkJob;
use App\Jobs\getBookDetailByBookLinkJob;
use App\Jobs\getBookLinkOldJob;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;

use App\Jobs\scanIndex;
use App\Jobs\getChapLinkBook;
use App\Jobs\getChapDetail;
use App\Jobs\getBookByCat;


class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        try {
            /** Quét Link Truyện tại index 10p / lần */
            $schedule->job(new scanIndex(), 'scanIndex')->cron('* * * * *');

            /** Lấy Link truyện theo chuyên mục */
//            $category = DB::table('categories')->select('id')->get();
//            foreach ($category as $k=>$v){
//                $schedule->job(new getBookByCat($v->id), 'getBookLink')->cron('* * * * *');
//            }


            for ($i = 0; $i <= 10; $i++) {
                /*** Luồng lấy link truyện theo page kèm theo queue getPaginateItem */
//                $schedule->job(new getBookLinkOldJob(), 'getBookLinkOld')->cron('* * * * *');
                /** Lấy chi tiết truyện **/
//                $schedule->job(new getBookDetailByBookLinkJob(), 'getBookDetail')->cron('* * * * *');

                /** Lấy đường dẫn chap thông qua truyện */
//                $schedule->job(new getChapLinkBook(), 'getLinkChap')->cron('* * * * *');

                /** Lấy Chi tiết chương thông qua link chap */
//                $schedule->job(new getChapDetail(), 'getChapDetail')->cron('* * * * *');
            }

        } catch (\Exception $e) {
            dump($e->getMessage());
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
