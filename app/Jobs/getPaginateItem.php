<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Http\Controllers\getBookLinkController;

class getPaginateItem implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $arrData;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($arrData)
    {
        //
        $this->arrData = $arrData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //

        sleep(2);
        $getBookLinkController = new getBookLinkController();

        dump($getBookLinkController->getPaginateItem($this->arrData));
    }
}
