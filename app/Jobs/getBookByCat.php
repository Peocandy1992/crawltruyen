<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


use App\Http\Controllers\getLinkChapController;

class getBookByCat implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        //
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        sleep(2);
        $getLinkChapController = new  getLinkChapController();

        $getLinkChapConf = getLinkChapController::getBookLinkConf($this->id);
        dump($getLinkChapConf);
        $getLinkChapController->getBookLink($getLinkChapConf);
    }
}
