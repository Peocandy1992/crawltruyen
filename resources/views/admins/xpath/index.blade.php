@extends('admins.layout.backend')
@section('content')
    <section class="content-header">
        <h1>
            Xpath
            <small>Xpath table</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="active">Data tables Xpath</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Xpath table</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <div class="btn-group">
                                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-wrench"></i></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#" class="btn_add_xpath">Add Xpath</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="box-body" id="xpath_body">
                        <table id="tbl_xpath" class="table table-bordered table-striped" style="width: 100%">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Xpath Name</th>
                                <th>Xpath Options</th>
                                <th>Created time</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- .modal -->
    <div class="modal fade in" id="modal_edit_xpath">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header callout callout-info">
                    <button type="button" class="close btn_close_book" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Xpath</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" id="form_modal_xpath">
                        <div class="form-group clearfix">
                            <label for="inputSourceUrl" class="col-sm-3 control-label">Xpath name:</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control effect-1" id="inputXpathName" placeholder="Xpath Name" name="inputXpathName">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="inputXpathOptions" class="col-sm-3 control-label">Xpath Option:</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control effect-1" id="inputXpathOptions" placeholder="Xpath Options" name="inputXpathOptions">
                                <span class="focus-border"></span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn_save_xpath" data-id="">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal_add_xpath">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header callout callout-info">
                    <button type="button" class="close btn_close_cat" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Xpath</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" id="form_ins_xpath">
                        <div class="form-group clearfix">
                            <label for="insSourceUrl" class="col-sm-3 control-label">Xpath Name:</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control effect-1" id="insXpathName" placeholder="Xpath Name" name="insXpathName">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="insXpathOptions" class="col-sm-3 control-label">Xpath Options:</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control effect-1" id="insXpathOptions" placeholder="Xpath Options" name="insXpathOptions">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn_ins_xpath" data-id="">Insert Xpath</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection