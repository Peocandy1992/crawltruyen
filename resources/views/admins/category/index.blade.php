@extends('admins.layout.backend')
@section('content')
    <section class="content-header">
        <h1>
            Categories
            <small>category tables</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            {{--<li><a href="#">Tables</a></li>--}}
            <li class="active">Data tables category</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Category table</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <div class="btn-group">
                                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-wrench"></i></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#" class="btn_add_cat">Add Category</a></li>

                                    <li class="divider"></li>
                                    <li><a href="#" class="btn_crawl_cat">Get Category</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="box-body" id="category_body">
                        <table id="tbl_category" class="table table-bordered table-striped" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Time create</th>
                                    <th>Options</th>
                                </tr>
                            </thead>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- .modal -->
    <div class="modal fade" id="modal_edit_cat">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header callout callout-info">
                    <button type="button" class="close btn_close_cat" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Category</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" id="form_modal_cat">
                        <div class="form-group clearfix">
                            <label for="inputCatname" class="col-sm-3 control-label">Category name:</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control effect-1" id="inputCatname" placeholder="Category name" name="catname">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="inputSlug" class="col-sm-3 control-label">Category slug:</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control effect-1" id="inputCatslug" placeholder="Category slug" name="catslug">
                                <span class="focus-border"></span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left btn_close_cat" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn_save_cat" data-id="">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade" id="modal_add_cat">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header callout callout-info">
                    <button type="button" class="close btn_close_cat" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Category</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" id="form_ins_cat">
                        <div class="form-group clearfix">
                            <label for="insCatname" class="col-sm-3 control-label">Category name:</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control effect-1" id="insCatname" placeholder="Category name" name="insertCatname">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="insCatslug" class="col-sm-3 control-label">Category slug:</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control effect-1" id="insCatslug" placeholder="Category slug" name="insertCatslug">
                                <span class="focus-border"></span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left btn_close_cat" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn_ins_cat" data-id="">Save Category</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal_get_cat">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header callout callout-info">
                    <button type="button" class="close btn_close_cat" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Get Book from Category</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" id="form_get_cat" class="clearfix">
                        <label for="selectCat" class="col-sm-2 control-label">Category:</label>

                        <div class="col-sm-4">
                            <select class="get_select_category form-control " name="select_category" style="">
                                <option>-- Find Category --</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left btn_close_cat" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn_get_cat" data-id="">Get Category</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- / .modal -->
@endsection
