@extends('admins.layout.backend')
@section('content')
    <section class="content-header">
        <h1>
            Books Link
            <small>Book Link tables</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="active">Data tables Book Link</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Book Link table</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            <button type="button" class="btn btn-box-tool refresh_tbl_bookLink"><i class="fa fa-fw fa-refresh"></i></button>
                            </button>
                            <div class="btn-group">
                                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-wrench"></i></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#" class="btn_add_book_link">Add Book Link</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="box-body" id="book_link_body">
                        @include('admins.bookLink.load')

                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- .modal -->
    <div class="modal fade in" id="modal_edit_book_link">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header callout callout-info">
                    <button type="button" class="close btn_close_book" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Book Link</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" id="form_modal_book_link">
                        <div class="form-group clearfix">
                            <label for="inputSourceBook" class="col-sm-2 control-label">Book link:</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control effect-1" id="inputSourceBook" placeholder="Book source" name="inputSourceBook">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="selectCat" class="col-sm-2 control-label">Category:</label>

                            <div class="col-sm-4">
                                <select class="select_category  form-control " name="select_category" style="">
                                </select>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn_save_book_link" data-id="">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal_add_book_link">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header callout callout-info">
                    <button type="button" class="close btn_close_cat" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Books Link</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" id="form_ins_book">
                        <form action="" method="POST" id="form_modal_book_link">
                            <div class="form-group clearfix">
                                <label for="insInputSourceBook" class="col-sm-2 control-label">Book link:</label>

                                <div class="col-sm-6">
                                    <input type="text" class="form-control effect-1" id="insInputSourceBook" placeholder="Book source" name="insInputSourceBook">
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="form-group clearfix">
                                <label for="insSelectCat" class="col-sm-2 control-label">Category:</label>

                                <div class="col-sm-4">
                                    <select class="insSelectCat  form-control " name="insSelectCat" style="">
                                        <option >-- Find Category --</option>
                                    </select>
                                </div>
                            </div>

                        </form>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn_ins_book_link" data-id="">Insert Book Link</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    {{--<div class="modal fade" id="modal_get_book_link">--}}
        {{--<div class="modal-dialog ">--}}
            {{--<div class="modal-content">--}}
                {{--<div class="modal-header callout callout-info">--}}
                    {{--<button type="button" class="close btn_close_cat" data-dismiss="modal" aria-label="Close">--}}
                        {{--<span aria-hidden="true">&times;</span></button>--}}
                    {{--<h4 class="modal-title">Get Books Link</h4>--}}
                {{--</div>--}}
                {{--<div class="modal-body">--}}
                    {{--<form action="" method="POST" id="form_get_book">--}}
                        {{--<form action="" method="POST" id="form_modal_get_book_link">--}}
                            {{--<div class="form-group clearfix">--}}
                                {{--<label for="getBookLink" class="col-sm-2 control-label">Book Link:</label>--}}

                                {{--<div class="col-sm-4">--}}
                                    {{--<select class="getBookLink  form-control " name="getBookLink" style="">--}}
                                        {{--<option >-- Find Book link --</option>--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                        {{--</form>--}}

                    {{--</form>--}}
                {{--</div>--}}
                {{--<div class="modal-footer">--}}
                    {{--<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>--}}
                    {{--<button type="button" class="btn btn-primary btn_get_book_link" data-id="">Get Book Link</button>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- /.modal-content -->--}}
        {{--</div>--}}
        {{--<!-- /.modal-dialog -->--}}
    {{--</div>--}}
@endsection
