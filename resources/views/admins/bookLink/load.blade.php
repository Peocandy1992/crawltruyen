<table id="tbl_book_link" class="table table-bordered table-striped" style="width: 100%">
    <thead>
    <tr>
        <th>Id</th>
        <th>Category</th>
        <th>Source url</th>
        <th>Created at</th>
        <th>Options</th>
    </tr>
    </thead>
    <tbody>
    @foreach($bookLinkData as $k=>$v)
        <tr id="link_{{$v->id}}">
            <td tabindex="1" class="editContent col-xs-1">
                {{$v->id}}

            </td>

            <td tabindex="1" class="editContent col-xs-2" style="max-width: 400px">
                {{$v->category}}

            </td>


            <td tabindex="1" class="editContent  col-xs-3">
                <a href="{{$v->source_url}}">{{$v->source_url}}</a>
            </td>

            <td tabindex="1" class="editContent  col-xs-3">
                {{$v->created_at}}
            </td>

            <td tabindex="1" class="editContent  option_col col-xs-3">
                <div class="ui-group-buttons"><a href="" class="btn btn-primary btn_get_book_link" role="button"
                                                 data-id="{{$v->id}}"><span
                                class="glyphicon glyphicon-download-alt"></span></a>
                    <div class="or"></div>
                    <a href="" class="btn btn-success btn_edit_book_link" role="button" data-id="{{$v->id}}"><span
                                class="glyphicon glyphicon-edit"></span></a>
                    <div class="or"></div>
                    <a href="" class="btn btn-danger btn_remove_book_link" role="button"
                       data-id="{{$v->id}}"><span class="glyphicon glyphicon-trash"></span></a></div>
            </td>
    @endforeach
    </tbody>
</table>