@extends('admins.layout.backend')
@section('content')
    <section class="content-header">
        <h1>
            Chapter Url
            <small>chapter tables</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="active">Data tables chapter link</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Chapter link table</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool refresh_dataBook"><i class="fa fa-fw fa-refresh"></i></button>
                            <div class="btn-group">
                                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-wrench"></i></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#" class="btn_add_chap_link">Add Chapter link</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#" class="scan_new_chap_link">Scan New Chap link</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="box-body" id="chapter_link_body">

                        @include('admins.chapterlink.load')

                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- .modal -->
    <div class="modal fade in" id="modal_edit_chapter_link">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header callout callout-info">
                    <button type="button" class="close btn_close_book" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Chapter Link</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" id="form_modal_chapter_link">
                        <div class="form-group clearfix">
                            <label for="inputSourceUrl" class="col-sm-3 control-label">Source url:</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control effect-1" id="inputSourceUrl" placeholder="Source url" name="inputSourceUrl">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="selectBook" class="col-sm-3 control-label">Books:</label>

                            <div class="col-sm-4">
                                <select class="select_book  form-control " name="select_book" style="">
                                    <option>--Find Book--</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn_save_chapter_link" data-id="">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal_add_chapter_link">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header callout callout-info">
                    <button type="button" class="close btn_close_cat" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Chapter Link</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" id="form_ins_chapter_link">
                        <div class="form-group clearfix">
                            <label for="insSourceUrl" class="col-sm-3 control-label">Source url:</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control effect-1" id="insSourceUrl" placeholder="Source url" name="insSourceUrl">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="insSelectBook" class="col-sm-3 control-label">Books:</label>

                            <div class="col-sm-4">
                                <select class="ins_select_book  form-control " name="ins_select_book" style="">
                                    <option>--Find Book--</option>
                                </select>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn_ins_chapter_link" data-id="">Insert Chapter Link</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection