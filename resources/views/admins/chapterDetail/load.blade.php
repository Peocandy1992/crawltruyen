<table id="tbl_chapter_detail" class="table table-bordered table-striped" style="width: 100%">
    <thead>
    <tr>
        <th>Id</th>
        <th>Truyện</th>
        <th>Tiêu đề</th>
        <th>Ngày Tạo</th>
        <th>Tùy Chọn</th>
    </tr>
    </thead>
    <tbody>
    @foreach($chapterDetailData as $k=>$v)
        <tr id="chapterDetail_{{$v->id}}">
            <td tabindex="1" class="editContent col-xs-1">
                {{$v->id}}

            </td>

            <td tabindex="1" class="editContent col-xs-2" style="max-width: 400px">
                {{$v->book_name}}
            </td>

            <td tabindex="1" class="editContent  col-xs-3">
                <a href="{{$v->source_url}}">
                    {{$v->chapName}}
                </a>
            </td>

            <td tabindex="1" class="editContent  col-xs-2">
                {{$v->created_at}}
            </td>

            <td tabindex="1" class="editContent  option_col col-xs-3">

                <div class="ui-group-buttons"><a href="" class="btn btn-primary btn_publish_chap_detail" role="button"
                                                 data-id="{{$v->id}}">
                        <span class="glyphicon glyphicon-cloud-upload"></span></a>
                    <div class="or"></div>
                    <a href="" class="btn btn-success btn_edit_chap_detail" role="button" data-id="{{$v->id}}"><span
                                class="glyphicon glyphicon-edit"></span></a>
                    <div class="or"></div>
                    <a href="" class="btn btn-danger btn_remove_chap_detail" role="button"
                       data-id="{{$v->id}}"><span class="glyphicon glyphicon-trash"></span></a>
                </div>

            </td>
    @endforeach
    </tbody>

</table>

{{ $chapterDetailData->links() }}