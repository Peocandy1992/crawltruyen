@extends('admins.layout.backend')
@section('content')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Chapter detail table</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool refresh_tbl_dashboard"><i class="fa fa-fw fa-refresh"></i></button>
                            <div class="btn-group">
                                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-wrench"></i></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#" class="btn_add_chapter_detail">Add Chapter Detail</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="box-body" id="chapter_detail_body">
                        @include('admins.chapterDetail.load')

                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- .modal -->
    <div class="modal fade in" id="modal_edit_chapter_detail">
        <div class="modal-dialog modal-xs">
            <div class="modal-content">
                <div class="modal-header callout callout-info">
                    <button type="button" class="close btn_close_chapter_detail" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Chapter Detail</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" id="form_modal_book">
                        <div class="form-group clearfix">
                            <label for="inputChapname" class="col-sm-2 control-label">Chapter name:</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control effect-1" id="inputChapname" placeholder="Chapter name" name="chapname">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="inputChapUrl" class="col-sm-2 control-label">Chapter url:</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control effect-1" id="inputChapurl" placeholder="Chapter url" name="chapurl">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="selectBook" class="col-sm-2 control-label">Book:</label>

                            <div class="col-sm-4">
                                <select class="select_book  form-control " name="select_book" style="">
                                </select>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="input_chap_content" class="col-sm-2 control-label">Chap content:</label>

                            <div class="col-sm-12">

                                <textarea rows="2" class="form-control no-resize chap_content" placeholder="Please input chap content..."
                                          name="chap_content" id="chap_content"></textarea>
                            </div>
                        </div>



                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn_save_chapter_detail" data-id="">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal_add_chapter_detail">
        <div class="modal-dialog modal-xs">
            <div class="modal-content">
                <div class="modal-header callout callout-info">
                    <button type="button" class="close btn_close_cat" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Chapter Detail</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" id="form_ins_chap_detail">
                        <div class="form-group clearfix">
                            <label for="insinputChapname" class="col-sm-2 control-label">Chapter name:</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control effect-1" id="insinputChapname" placeholder="Chapter name" name="inschapname">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="insinputChapUrl" class="col-sm-2 control-label">Chapter url:</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control effect-1" id="insinputChapurl" placeholder="Chapter url" name="inschapurl">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="insselectBook" class="col-sm-2 control-label">Book:</label>

                            <div class="col-sm-4">
                                <select class="insselect_book  form-control " name="insselect_book" style="">
                                    <option >-- Find Book --</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="insinput_chap_content" class="col-sm-2 control-label">Chap content:</label>

                            <div class="col-sm-12">

                                <textarea rows="2" class="form-control no-resize inschap_content" placeholder="Please input chap content..." name="inschap_content" id="inschap_content"></textarea>
                            </div>
                        </div>



                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn_ins_chap_detail" data-id="">Insert Chapter Detail</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection