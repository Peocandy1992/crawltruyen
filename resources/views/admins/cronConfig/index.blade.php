@extends('admins.layout.backend')
@section('content')
    <section class="content-header">
        <h1>
            Cron Config
            <small>Cron Configs table</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="active">Data tables Cron Config</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Cron Config table</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <div class="btn-group">
                                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-wrench"></i></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#" class="btn_add_cron_cf">Add Cron Config</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="box-body" id="cron_config_body">
                        <table id="tbl_cron_config" class="table table-bordered table-striped" style="width: 100%">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Cron Name</th>
                                <th>Xpath</th>
                                <th>Cron Setting</th>
                                <th>Level</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- .modal -->
    <div class="modal fade in" id="modal_edit_cron_config">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header callout callout-info">
                    <button type="button" class="close btn_close_cron_setting" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Chapter Link</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" id="form_modal_cron_config">
                        <div class="form-group clearfix">
                            <label for="inputCronName" class="col-sm-3 control-label">Cron name:</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control effect-1" id="inputCronName" placeholder="Cron Name" name="inputCronName">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="selectXpath" class="col-sm-3 control-label">Xpath:</label>

                            <div class="col-sm-4">
                                <select class="select_xpath  form-control " name="select_xpath" style="">
                                    <option>--Find Xpath--</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="inputCronSetting" class="col-sm-3 control-label">Cron setting:</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control effect-1" id="inputCronSetting" placeholder="Cron Setting" name="inputCronSetting">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="inputLevel" class="col-sm-3 control-label">Level:</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control effect-1" id="inputLevel" placeholder="Cron Level" name="inputLevel">
                                <span class="focus-border"></span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn_save_cron_setting" data-id="">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal_add_cron_setting">
        <div class="modal-dialog modal-xs">
            <div class="modal-content">
                <div class="modal-header callout callout-info">
                    <button type="button" class="close btn_close_cron_setting" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Cron setting</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" id="form_ins_chapter_link">
                        <div class="form-group clearfix">
                            <label for="inputCronName" class="col-sm-3 control-label">Cron name:</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control effect-1" id="insinputCronName" placeholder="Cron Name" name="insinputCronName">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="selectXpath" class="col-sm-3 control-label">Xpath:</label>

                            <div class="col-sm-4">
                                <select class="insselect_xpath  form-control " name="insselect_xpath" style="">
                                    <option>--Find Xpath--</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="inputCronSetting" class="col-sm-3 control-label">Cron setting:</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control effect-1" id="insinputCronSetting" placeholder="Cron Setting" name="insinputCronSetting">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="insinputLevel" class="col-sm-3 control-label">Level:</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control effect-1" id="insinputLevel" placeholder="Cron Level" name="insinputLevel">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn_ins_cron_setting" data-id="">Insert Cron setting</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection