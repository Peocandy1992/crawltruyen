@extends('admins.layout.backend')
@section('content')
    <section class="content-header">
        <h1>
            Author
            <small>author tables</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="active">Data tables author</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Author table</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            <button type="button" class="btn btn-box-tool refresh_author"><i class="fa fa-fw fa-refresh"></i></button>
                            </button>
                            <div class="btn-group">
                                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-wrench"></i></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#" class="btn_add_author">Add Author</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="box-body" id="author_body">
                        <table id="tbl_author" class="table table-bordered table-striped" style="width: 100%">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Slug</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- .modal -->
    <div class="modal fade in" id="modal_edit_author">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header callout callout-info">
                    <button type="button" class="close btn_close_book" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Author</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" id="form_modal_author">
                        <div class="form-group clearfix">
                            <label for="inputAuthorname" class="col-sm-3 control-label">Author name:</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control effect-1" id="inputAuthorname" placeholder="Author name" name="authorname">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="inputAuthorslug" class="col-sm-3 control-label">Slug:</label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control effect-1" id="inputAuthorslug" placeholder="Author slug" name="authorslug">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn_save_author" data-id="">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal_add_author">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header callout callout-info">
                    <button type="button" class="close btn_close_cat" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Author</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" id="form_ins_author">
                        <div class="form-group clearfix">
                            <label for="insAuthorname" class="col-sm-3 control-label">Author name:</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control effect-1" id="insAuthorname" placeholder="Author name" name="insAuthorname">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="insAuthorslug" class="col-sm-3 control-label">Slug:</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control effect-1" id="insAuthorslug" placeholder="Author slug" name="insauthorslug">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn_ins_author" data-id="">Insert Author</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection