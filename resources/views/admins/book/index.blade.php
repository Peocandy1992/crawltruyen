@extends('admins.layout.backend')
@section('content')
    <section class="content-header">
        <h1>
            Books
            <small>book tables</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="active">Data tables book</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Book table</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool refresh_datatblBook"><i class="fa fa-fw fa-refresh"></i></button>
                            <div class="btn-group">
                                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-wrench"></i></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#" class="btn_add_book">Add Book</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="box-body" id="book_body">
                        <table id="tbl_book" class="table table-bordered table-striped" style="width: 100%">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Thumbnail</th>
                                <th>Description</th>
                                <th>Category</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- .modal -->
    <div class="modal fade in" id="modal_edit_book">
        <div class="modal-dialog modal-xs">
            <div class="modal-content">
                <div class="modal-header callout callout-info">
                    <button type="button" class="close btn_close_book" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Book</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" id="form_modal_book">
                        <div class="form-group clearfix">
                            <label for="inputBookname" class="col-sm-2 control-label">Book name:</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control effect-1" id="inputBookname" placeholder="Book name" name="bookname">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="inputBookslug" class="col-sm-2 control-label">Slug:</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control effect-1" id="inputBookslug" placeholder="Book slug" name="bookslug">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="inputBookdescription" class="col-sm-2 control-label">Description:</label>

                            <div class="col-sm-9">

                                <textarea rows="2" class="form-control no-resize bookdescription" placeholder="Please input book description..." name="bookdescription" id="bookdescription"></textarea>
                                {{--<input type="text" class="form-control" id="inputBookdescription" placeholder="Book descriptions" name="bookdescription">--}}
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="selectCat" class="col-sm-2 control-label">Category:</label>

                            <div class="col-sm-4">
                                <select class="select_category  form-control " name="select_category" style="">
                                </select>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="selectCat" class="col-sm-2 control-label">Author:</label>

                            <div class="col-sm-4">
                                <select class="select_author  form-control " name="select_author" style="">
                                    <option>--Find Author--</option>
                                </select>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn_save_book" data-id="">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal_add_book">
        <div class="modal-dialog modal-xs">
            <div class="modal-content">
                <div class="modal-header callout callout-info">
                    <button type="button" class="close btn_close_cat" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Books</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" id="form_ins_book">
                        <div class="form-group clearfix">
                            <label for="insBookname" class="col-sm-2 control-label">Book name:</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control effect-1" id="insBookname" placeholder="Book name" name="insBookname">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="insBookslug" class="col-sm-2 control-label">Slug:</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control effect-1" id="insBookslug" placeholder="Book slug" name="insbookslug">
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="inputBookdescription" class="col-sm-2 control-label">Description:</label>

                            <div class="col-sm-9">
                                <textarea rows="2" class="form-control no-resize insBookdescription" placeholder="Please input book description..." name="insBookdescription" id="insBookdescription"></textarea>
                                {{--<input type="text" class="form-control" id="inputBookdescription" placeholder="Book descriptions" name="bookdescription">--}}
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="selectCat" class="col-sm-2 control-label">Category:</label>

                            <div class="col-sm-6">
                                <select class="ins_select_category  form-control " name="select_category" style="">
                                    <option>--Find category--</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="selectAuthor" class="col-sm-2 control-label">Author:</label>

                            <div class="col-sm-4">
                                <select class="ins_select_author  form-control " name="select_author" style="">
                                    <option>--Find Author--</option>
                                </select>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn_ins_book" data-id="">Insert Book</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade in" id="modal_book_preview"  role="dialog" >
        <div class=" modal-dialog modal-lg card" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="js-title-step"></h4>
                </div>
                <div class="modal-body body clearfix">

                </div>


                <div class="modal-footer">
                    <button type="button" class="btn btn-warning waves-effect js-btn-step" data-orientation="previous"></button>
                    <button type="button" class="btn btn-success waves-effect js-btn-step " data-orientation="next"></button>
                </div>
            </div>
        </div>
    </div>
@endsection
