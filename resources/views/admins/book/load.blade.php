<table id="tbl_book" class="table table-bordered table-striped" style="width: 100%">
    <thead>
    <tr>
        <th>Id</th>
        <th>Chap title</th>
        <th>Source Url</th>
        <th>Hash Url</th>
        <th>GetTime</th>
        <th>Options</th>
    </tr>
    </thead>
    <tbody>
    @foreach($bookData as $k=>$v)
        <tr id="link_{{$v->id}}">
            <td tabindex="1" class="editContent col-xs-1">
                {{$v->id}}

            </td>

            <td tabindex="1" class="editContent col-xs-2" style="max-width: 400px">
                <strong><i class="fa fa-fw fa-bookmark"></i>{{$v->chapName}}</strong>
                <div class="book bold" style="padding-top: 5px; color: red;"><span
                            class="glyphicon glyphicon-book"></span> {{$v->book}}</div>

            </td>


            <td tabindex="1" class="editContent  col-xs-3">
                @if($v->getTime == null)
                    <a href='{{$v->source_url}}' title="source url">{{$v->source_url}}</a>
                @else
                    <del><a href='{{$v->source_url}}' title="source url">{{$v->source_url}}</a></del>
                @endif
            </td>

            <td tabindex="1" class="editContent col-xs-3">
                {{$v->hash_url}}
            </td>

            <td tabindex="1" class="editContent  col-xs-1">
                {{$v->created_at}}
            </td>

            <td tabindex="1" class="editContent  option_col col-xs-3">

                @if($v->getTime == null)
                    <div class="ui-group-buttons"><a href="" class="btn btn-primary btn_crawl_chapter" role="button"
                                                     data-id="{{$v->id}}"><span
                                    class="glyphicon glyphicon-download-alt"></span></a>
                        <div class="or"></div>
                        <a href="" class="btn btn-success btn_edit_chap_link" role="button" data-id="{{$v->id}}"><span
                                    class="glyphicon glyphicon-edit"></span></a>
                        <div class="or"></div>
                        <a href="" class="btn btn-danger btn_remove_chap_link" role="button"
                           data-id="{{$v->id}}"><span class="glyphicon glyphicon-trash"></span></a></div>
                @else
                    <div class="ui-group-buttons"><a href="" class="btn btn-primary btn_crawl_chapter disabled"
                                                     role="button" data-id="{{$v->id}}" disabled><span
                                    class="glyphicon glyphicon-download-alt"></span></a>
                        <div class="or"></div>
                        <a href="" class="btn btn-success btn_edit_chap_link" role="button" data-id="{{$v->id}}"><span
                                    class="glyphicon glyphicon-edit"></span></a>
                        <div class="or"></div>
                        <a href="" class="btn btn-danger btn_remove_chap_link" role="button"
                           data-id="{{$v->id}}"><span class="glyphicon glyphicon-trash"></span></a></div>
                @endif
            </td>
    @endforeach
    </tbody>

</table>

{{ $bookData->links() }}