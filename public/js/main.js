$(document).ready(function () {
    /*** 1 so ham thu vien sdung de tang thoi gian code **/
    /***get Param from Url **/
    var getParam = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    // Add active on menu
    var addActive = function () {
        var sPageURL = window.location.href;
        sPageURL = sPageURL.replace('#', '')
        var childA = $('.sidebar-menu .treeview-menu a');
        $.each(childA, function () {
            var href = $(this).attr('href');
            if (href == sPageURL) {
                $(this).parent().parent().parent().addClass('menu-open');
                $(this).addClass('active');
                $(this).parent().parent().css('display', 'block');
            }

        })
    }


    /***tinymceLoad **/
    var tinymceLoad = function (className) {
        /***TinyMCE **/
        tinymce.init({
            selector: "textarea" + className,
            theme: "modern",
            height: 150,
            plugin_preview_width: 1050,
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools'
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            /*** toolbar2: 'print preview media | forecolor backcolor emoticons', **/
            image_advtab: true,
            branding: false
        });
        tinymce.suffix = ".min";
        tinyMCE.baseURL = '../../plugins/tinymce';
    }

    /*** Ajax Post block **/
    function ajaxPostBlock(url, data, callBack, block, async) {
        /*** async = async || 'true'; **/
        /*** setup csrf for laravel proj **/
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: data,
            async: async,
            beforeSend: function () {
                window.totR++;
                /***Block UI. **/
                if (block) {
                    $(block).block({
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        },
                        message: '<img src="/img/Spinner-1s-44px.gif" alt="">',
                    });

                }
            },
            success: function (res) {
                if (res.error) {
                    alert(res.error);
                    return;
                }
                if (typeof callBack === 'function') callBack(res);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                /*** console.log(textStatus); **/
            },
            complete: function () {
                window.totR--
                /***Unblock UI. **/
                if (block) {
                    $(block).unblock();

                }
            }
        });
    }

    /***Handle Time!
     dataTable categories **/
    /**** Category page ***/
    var catTable = $('#tbl_category').DataTable({
        ajax: {
            "url": '/category',
        },
        "deferRender": true,
        "columns": [
            {
                "data": "id",
                className: "col-xs-1"
            },
            {
                "data": "name",
                className: "col-xs-4"
            },
            {
                "data": "created_at",
                className: "col-xs-2"
            },
            {
                "data": null,
                "render": function (data, type, full, meta) {
                    var buttonID = full.id;
                    return '<div class="ui-group-buttons"><a href="" class="btn btn-info btn_get_cat" role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-download-alt"></span></a><div class="or"></div><a href="" class="btn btn-success btn_edit_cat" role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-edit"></span></a><div class="or"></div><a href="" class="btn btn-danger btn_remove_cat" role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-trash"></span></a></div>';
                },
                className: "option_col col-xs-2",
            }
        ],

    });

    /** update category by id **/
    $(document).on('click', '.btn_edit_cat', function (e) {
        e.preventDefault();
        var idCat = $(this).attr('data-id');
        $('#modal_edit_cat').modal('show');
        ajaxPostBlock('/getCatByid', {id: idCat}, function (res) {
            $('.btn_save_cat').attr('data-id', res.id);
            $('#inputCatname').val(res.name);
        }, '.content')
    });

    var updateCat = function () {
        $('.btn_save_cat').on('click', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            var data = $('#form_modal_cat').serialize();
            data += '&id=' + id;

            ajaxPostBlock('/updateCateById', data, function (res) {
                if (res == 1) {
                    $.notify({
                        title: '<strong>Success</strong>',
                        icon: 'glyphicon glyphicon-star',
                        message: "Data update success !"
                    }, {
                        type: 'success',
                        animate: {
                            enter: 'animated fadeInUp',
                            exit: 'animated fadeOutRight'
                        },
                        placement: {
                            from: "bottom",
                            align: "left"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                    });
                    $('#modal_edit_cat').modal('hide');
                    $('#tbl_category').DataTable().ajax.reload();
                } else {
                    $.notify({
                        title: '<strong>Failed</strong>',
                        icon: 'glyphicon glyphicon-star',
                        message: "Data update failed"
                    }, {
                        type: 'danger',
                        animate: {
                            enter: 'animated fadeInUp',
                            exit: 'animated fadeOutRight'
                        },
                        placement: {
                            from: "bottom",
                            align: "left"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                    });
                    $('#modal_edit_cat').modal('hide');
                    $('#tbl_category').DataTable().ajax.reload();
                }

            })
        })
    }

    /**delete category by id **/
    var deleteCat = function () {
        $(document).on('click', '.btn_remove_cat', function (e) {
            e.preventDefault();

            var id = $(this).attr('data-id');

            swal({
                title: 'Are You sure ?',
                text: 'Once deleted, you will not be able to recover this Category!',
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    swal('Poof! Your Category has been deleted!', {
                        icon: "success",
                    });

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: '/deleteCatByid',
                        type: 'POST',
                        dataType: 'json',
                        data: {id: id},
                        beforeSend: function () {
                            window.totR++;
                            /***Block UI. **/
                            $('.body').block();
                        },
                        success: function (res) {
                            if (res == 1) {
                                $.notify({
                                    title: '<strong>Success</strong>',
                                    icon: 'glyphicon glyphicon-star',
                                    message: "Delete Category Successfull!"
                                }, {
                                    type: 'success',
                                    animate: {
                                        enter: 'animated fadeInUp',
                                        exit: 'animated fadeOutRight'
                                    },
                                    placement: {
                                        from: "bottom",
                                        align: "left"
                                    },
                                    offset: 20,
                                    spacing: 10,
                                    z_index: 1031,
                                });
                                $('#tbl_category').DataTable().ajax.reload();
                            } else {
                                $.notify({
                                    title: '<strong>Failed</strong>',
                                    icon: 'glyphicon glyphicon-star',
                                    message: "Delete Category Failed!"
                                }, {
                                    type: 'danger',
                                    animate: {
                                        enter: 'animated fadeInUp',
                                        exit: 'animated fadeOutRight'
                                    },
                                    placement: {
                                        from: "bottom",
                                        align: "left"
                                    },
                                    offset: 20,
                                    spacing: 10,
                                    z_index: 1031,
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        complete: function (ret) {
                            window.totR--
                            /***Unblock UI. **/
                            $('.body').unblock();
                        }
                    });

                } else {
                    swal('Your Category is safe!');
                }
            });

        })
    }

    /***add category **
     **show modal  ***/
    $(document).on('click', '.btn_add_cat', function (e) {
        e.preventDefault();
        $('#modal_add_cat').modal('show');
    });

    /*** insert cat **/
    $(document).on('click', '.btn_ins_cat', function (e) {
        e.preventDefault();
        var data = $('#form_ins_cat').serialize();
        ajaxPostBlock('/insertCat', data, function (res) {
            if (res == 1) {
                $.notify({
                    title: '<strong>Success</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data insert success !"
                }, {
                    type: 'success',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#modal_edit_cat').modal('hide');
                $('#tbl_category').DataTable().ajax.reload();
            } else {
                $.notify({
                    title: '<strong>Failed</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data insert failed"
                }, {
                    type: 'danger',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#modal_edit_cat').modal('hide');
                $('#tbl_category').DataTable().ajax.reload();
            }
        }, '.wrapper')
    });

    $(document).on('click', '.btn_add_cat', function (e) {
        e.preventDefault();
        $('#modal_add_cat').modal('show');
    });

    $('.btn_crawl_cat').on('click', function (e) {
        e.preventDefault();
        $('#modal_get_cat').modal('show');

        $('.get_select_category').select2(
            {
                ajax: {
                    url: "/findCatbyName",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term /** search term **/
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }

                            })
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            }
        );
    });

    $(document).on('click', '.btn_get_cat', function (e) {
        e.preventDefault();
        var idCat = $(this).attr('data-id');
        ajaxPostBlock('/getBookByCatid', {id: idCat}, function (res) {
            if (res == 1) {
                $.notify({
                    title: '<strong>Success</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data get success !"
                }, {
                    type: 'success',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#modal_get_cat').modal('hide');
            } else {
                $.notify({
                    title: '<strong>Failed</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data get failed"
                }, {
                    type: 'danger',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#modal_get_cat').modal('hide');
            }
        }, '#tbl_category');
    })

    $('.btn_get_cat_modal').on('click', function (e) {
        e.preventDefault();
        var idCat = $('.get_select_category').val();
        ajaxPostBlock('/getBookByCatid', {id: idCat}, function (res) {
            if (res == 1) {
                $.notify({
                    title: '<strong>Success</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data get success !"
                }, {
                    type: 'success',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#modal_get_cat').modal('hide');
            } else {
                $.notify({
                    title: '<strong>Failed</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data get failed"
                }, {
                    type: 'danger',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#modal_get_cat').modal('hide');
            }
        }, '#tbl_category');
    })
    /*** end Category page ***/

    /** Books page **/
    var bookTable = $('#tbl_book').DataTable({
        ajax: {
            "url": '/book',
        },
        "deferRender": true,
        "columns": [
            {
                "data": "id",
                className: "col-xs-1"
            },
            {
                "data": null,
                "render": function (data, type, full, meta) {
                    var name = full.name;
                    var author = full.author_name;
                    var source_url = full.slug;
                    if (author != null) {
                        return '<a class="author_name".table .author_info href="' + source_url + '">' + name + '</a></div><div class="author_info"><i class="fa fa-fw fa-user"></i>' + author + '</div>';
                    } else {
                        return '<div class="author_name"><a href="' + source_url + '">' + name + '</a></div>';
                    }
                },
                className: "col-xs-2"
            },
            {
                "data": null,
                "render": function (data, type, full, meta) {
                    var images = full.images;
                    if (images != null) {
                        return '<img src="' + images + '" style="max-height: 100px" />';
                    } else {
                        return '';
                    }
                },
                className: "col-xs-1"
            },
            {
                "data": null,
                "render": function (data, type, full, meta) {
                    var descriptions = full.description;
                    if (descriptions != null) {
                        if (descriptions.length > 155) {
                            var textDisplay = descriptions.substring(0, 150) + '.....';
                            return textDisplay;
                        } else {
                            return descriptions;
                        }
                    } else {
                        return '';
                    }
                },
                className: "col-xs-4"
            },
            {
                "data": "category",
                className: "col-xs-2"
            },
            {
                "data": null,
                "render": function (data, type, full, meta) {
                    var buttonID = full.id;
                    if (buttonID == 1) {
                        return '<div class="ui-group-buttons"><a href="" class="btn btn-info btn_get_chapter_detail" role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-download-alt"></span></a><div class="or"></div>' +
                            // '<a href="" class="btn btn-primary btn_preview disabled" role="button" data-id="'+buttonID+'"><span class="glyphicon glyphicon-eye-open"></span></a><div class="or"></div>' +
                            '<a href="" class="btn btn-success btn_edit_book" role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-edit"></span></a><div class="or"></div><a href="" class="btn btn-danger btn_remove_book" role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-trash"></span></a></div>';
                    } else {
                        return '<div class="ui-group-buttons"><a href="" class="btn btn-info btn_get_chapter_detail" role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-download-alt"></span></a><div class="or"></div>' +
                            //'<a href="" class="btn btn-primary btn_preview" role="button" data-id="'+buttonID+'"><span class="glyphicon glyphicon-eye-open"></span></a><div class="or"></div>' +
                            '<a href="" class="btn btn-success btn_edit_book" role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-edit"></span></a><div class="or"></div><a href="" class="btn btn-danger btn_remove_book" role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-trash"></span></a></div>';
                    }
                },
                className: "col-xs-2",
            }
        ],
    });

    $(document).on('click', '.btn_get_chapter_detail', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        ajaxPostBlock('/getChapDetailByidBook', {id: id}, function (res) {
            if (res == 1) {
                $.notify({
                    title: '<strong>Success</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Crawl book success !"
                }, {
                    type: 'success',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#tbl_book').DataTable().ajax.reload();
            } else {
                $.notify({
                    title: '<strong>Failed</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Crawl book failed"
                }, {
                    type: 'danger',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#tbl_book').DataTable().ajax.reload();
            }
        }, '#tbl_book');
    })

    $('#modal_book_preview').modalSteps();

    $(document).on('click', '.btn_preview', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        $('#modal_book_preview').modal('show');
        ajaxPostBlock('/previewBook', {id: id}, function (res) {
            var html = '';
            $.each(res, function (key, val) {
                if (key == 0) {
                    $('.modal-header').html('<h4 class="js-title-step"><span class="label label-success">1</span> ' + val.chapName + '</h4>')
                    html += '<div class="row" data-step="' + (key + 1) + '" data-title="' + val.chapName + '"><div class="step_content">' + val.chapContent + '</div></div>';
                } else {
                    html += '<div class="row hide" data-step="' + (key + 1) + '" data-title="' + val.chapName + '"><div class="step_content">' + val.chapContent + '</div></div>';
                }
            });
            $('#modal_book_preview .modal-body').append(html);


        });

    })

    $('#modal_book_preview').on('hidden.bs.modal', function () {
        $('#modal_book_preview .modal-body').empty();
    });
    /** End book page **/

    /** edit book **/
    $(document).on('click', '.btn_edit_book', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        $('#modal_edit_book').modal('show');

        /**tinymceLoad get name attribute **/
        var ed = tinyMCE.get('bookdescription');

        ajaxPostBlock('/getBookByid', {id: id}, function (res) {
            $('#inputBookname').val(res.name);
            $('#inputBookslug').val(res.slug);
            $('#inputBookdescription').val(res.description);
            $('.btn_save_book').attr('data-id', res.id);
            ed.setContent(res.description);

            $('.select_category').select2({
                ajax: {
                    url: "/findCatbyName",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term /** search term **/
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }

                            })
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });

            var newOption = new Option(res.category, res.catID, false, false);
            newOption.selected = true;
            $('.select_category').append(newOption).trigger('change');

            $('.select_author').select2({
                ajax: {
                    url: "/findAuthbyName",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term /** search term **/
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }

                            })
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });

            var newOption2 = new Option(res.author_name, res.author_id, false, false);
            newOption2.selected = true;
            $('.select_author').append(newOption2).trigger('change');
        })
    });

    var editBook = function () {
        $('.btn_save_book').on('click', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            var bookname = $('#inputBookname').val();
            var bookslug = $('#inputBookslug').val();
            var select_category = $('.select_category').val();
            var select_author = $('.select_author').val();

            var description = tinyMCE.get('bookdescription').getContent();
            ajaxPostBlock('/editBookByid', {
                id: id,
                select_category: select_category,
                bookslug: bookslug,
                bookname: bookname,
                description: description,
                select_author: select_author
            }, function (res) {
                if (res == 1) {
                    $.notify({
                        title: '<strong>Success</strong>',
                        icon: 'glyphicon glyphicon-star',
                        message: "Data update success !"
                    }, {
                        type: 'success',
                        animate: {
                            enter: 'animated fadeInUp',
                            exit: 'animated fadeOutRight'
                        },
                        placement: {
                            from: "bottom",
                            align: "left"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                    });
                    $('#modal_edit_book').modal('hide');
                    $('#tbl_book').DataTable().ajax.reload();
                } else {
                    $.notify({
                        title: '<strong>Failed</strong>',
                        icon: 'glyphicon glyphicon-star',
                        message: "Data update failed"
                    }, {
                        type: 'danger',
                        animate: {
                            enter: 'animated fadeInUp',
                            exit: 'animated fadeOutRight'
                        },
                        placement: {
                            from: "bottom",
                            align: "left"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                    });
                    $('#modal_edit_book').modal('hide');
                    $('#tbl_book').DataTable().ajax.reload();
                }
            })
        })
    }

    $('#modal_edit_book').on('hidden.bs.modal', function () {
        var ed = tinyMCE.get('bookdescription');
        $('#inputBookname').val('');
        $('#inputBookslug').val('');
        ed.setContent('');
        $('.select_category').val(null).trigger('change');
        $('.select_author').val(null).trigger('change');
    });


    /**delete book **/
    var deleteBook = function () {
        $(document).on('click', '.btn_remove_book', function (e) {
            e.preventDefault();

            var id = $(this).attr('data-id');

            swal({
                title: 'Are You sure ?',
                text: 'Once deleted, you will not be able to recover this book!',
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    swal('Poof! Your Book has been deleted!', {
                        icon: "success",
                    });

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: '/deleteBookByid',
                        type: 'POST',
                        dataType: 'json',
                        data: {id: id},
                        beforeSend: function () {
                            window.totR++;
                            /**Block UI. **/
                            $('.body').block();
                        },
                        success: function (res) {
                            if (res == 1) {
                                $.notify({
                                    title: '<strong>Success</strong>',
                                    icon: 'glyphicon glyphicon-star',
                                    message: "Delete Book Successfull!"
                                }, {
                                    type: 'success',
                                    animate: {
                                        enter: 'animated fadeInUp',
                                        exit: 'animated fadeOutRight'
                                    },
                                    placement: {
                                        from: "bottom",
                                        align: "left"
                                    },
                                    offset: 20,
                                    spacing: 10,
                                    z_index: 1031,
                                });
                                $('#tbl_book').DataTable().ajax.reload();
                            } else {
                                $.notify({
                                    title: '<strong>Failed</strong>',
                                    icon: 'glyphicon glyphicon-star',
                                    message: "Delete Book Failed!"
                                }, {
                                    type: 'danger',
                                    animate: {
                                        enter: 'animated fadeInUp',
                                        exit: 'animated fadeOutRight'
                                    },
                                    placement: {
                                        from: "bottom",
                                        align: "left"
                                    },
                                    offset: 20,
                                    spacing: 10,
                                    z_index: 1031,
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        complete: function (ret) {
                            window.totR--
                            /**Unblock UI. **/
                            $('.body').unblock();
                        }
                    });

                } else {
                    swal('Your Book is safe!');
                }
            });

        })
    }

    $(document).on('click', '.btn_get_book', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        ajaxPostBlock('/getChapterLinkByBookId', {id: id}, function (res) {
            if (res == 1) {
                $.notify({
                    title: '<strong>Success</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Chapter link get success !"
                }, {
                    type: 'success',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });

            } else {
                $.notify({
                    title: '<strong>Failed</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Chapter link get failed"
                }, {
                    type: 'danger',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });

            }
        }, '#tbl_book');
    });

    /**show modal **/
    /**show modal add book **/
    $(document).on('click', '.btn_add_book', function (e) {
        e.preventDefault();
        $('#modal_add_book').modal('show');

        $('.ins_select_category').select2({
            ajax: {
                url: "/findCatbyName",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term /** search term **/
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }

                        })
                    };
                },
                cache: true
            },
            minimumInputLength: 2
        });

        $('.ins_select_author').select2({
            ajax: {
                url: "/findAuthbyName",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term /** search term **/
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }

                        })
                    };
                },
                cache: true
            },
            minimumInputLength: 2
        });


    });

    /** insert book **/
    $(document).on('click', '.btn_ins_book', function (e) {
        e.preventDefault();
        var name = $('#insBookname').val();
        var slug = $('#insBookslug').val();
        var descriptions = $('#insBookdescription').val();
        var cat_id = $('#select_category').val();
        ajaxPostBlock('/insertBook', {
            name: name,
            slug: slug,
            description: descriptions,
            cat_id: cat_id
        }, function (res) {
            if (res == 1) {
                $.notify({
                    title: '<strong>Success</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data insert success !"
                }, {
                    type: 'success',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#modal_add_book').modal('hide');
                $('#tbl_book').DataTable().ajax.reload();
            } else {
                $.notify({
                    title: '<strong>Failed</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data insert failed"
                }, {
                    type: 'danger',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#modal_add_book').modal('hide');
                $('#tbl_book').DataTable().ajax.reload();
            }
        }, '.wrapper')
    });
    /** End Books page **/

    /** author page**/
    var authorTable = $('#tbl_author').DataTable({
        ajax: {
            "url": '/author',
        },
        "deferRender": true,
        "columns": [
            {"data": "id", className: "col-xs-1"},
            {
                "data": "name",
                className: "col-xs-2"
            },
            {
                "data": "slug",
                className: "col-xs-6"
            },
            {
                "data": null,
                "render": function (data, type, full, meta) {
                    var buttonID = full.id;
                    return '<div class="ui-group-buttons"><a href="" class="btn btn-success btn_edit_author role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-edit"></span></a><div class="or"></div><a href="" class="btn btn-danger btn_remove_author" role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-trash"></span></a></div>';
                },
                className: "option_col",
            }
        ],
    });

    /** edit author **/
    $(document).on('click', '.btn_edit_author', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        $('#modal_edit_author').modal('show');

        ajaxPostBlock('/getAuthorByid', {id: id}, function (res) {
            $('#inputAuthorname').val(res.name);
            $('#inputAuthorslug').val(res.slug);
            $('.btn_save_author').attr('data-id', res.id);
        })
    });

    var editAuthor = function () {
        $('.btn_save_author').on('click', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            var data = $('#form_modal_author').serialize();
            data += '&id=' + id;
            ajaxPostBlock('/updateAuthor', data, function (res) {
                if (res == 1) {
                    $.notify({
                        title: '<strong>Success</strong>',
                        icon: 'glyphicon glyphicon-star',
                        message: "Data update success !"
                    }, {
                        type: 'success',
                        animate: {
                            enter: 'animated fadeInUp',
                            exit: 'animated fadeOutRight'
                        },
                        placement: {
                            from: "bottom",
                            align: "left"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                    });
                    $('#modal_edit_author').modal('hide');
                    $('#tbl_author').DataTable().ajax.reload();
                } else {
                    $.notify({
                        title: '<strong>Failed</strong>',
                        icon: 'glyphicon glyphicon-star',
                        message: "Data update failed"
                    }, {
                        type: 'danger',
                        animate: {
                            enter: 'animated fadeInUp',
                            exit: 'animated fadeOutRight'
                        },
                        placement: {
                            from: "bottom",
                            align: "left"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                    });
                    $('#modal_edit_author').modal('hide');
                    $('#tbl_author').DataTable().ajax.reload();
                }
            })
        });
    }

    $('.refresh_author').on('click', function (e) {
        e.preventDefault();
        $('#tbl_author').DataTable().ajax.reload();
        $('#tbl_author').block();
        setTimeout(function () {
            $('#tbl_author').unblock();
        }, 200);
    })

    $('.refresh_datatblBook').on('click', function (e) {
        e.preventDefault();
        $('#tbl_book').DataTable().ajax.reload();
        $('#tbl_book').block();
        setTimeout(function () {
            $('#tbl_book').unblock();
        }, 200);
    })
    $('.refresh_tbl_bookLink').on('click', function (e) {
        e.preventDefault();
        // $('#tbl_book_link').DataTable().ajax.reload();
        // $('#tbl_book_link').block();/
        // setTimeout(function () {
        //     $('#tbl_book_link').unblock();
        // }, 200);
        ReloadBookLink('#tbl_book_link');
    })

    var ReloadBookLink =function (block) {
        $.ajax(
            {
                url: '/bookLink',
                type: "get",
                datatype: "html",
                beforeSend: function () {
                    window.totR++;
                    //Block UI.
                    if (block) {
                        $(block).block();
                    }
                },
            })

            .done(function (data) {
                $("#book_link_body").empty().html(data);

            })
            .fail(function (jqXHR, ajaxOptions, thrownError) {
                alert('No response from server');
            });

    };

    $('.refresh_tbl_dashboard').on('click', function (e) {
        e.preventDefault();

        ReloadChapterDetail('#tbl_chapter_detail');

        // $('#tbl_chapter_detail').DataTable().ajax.reload();
        // $('#tbl_chapter_detail').block();
        // setTimeout(function () {
        //     $('#tbl_chapter_detail').unblock();
        // },200);
    });

    var ReloadChapterDetail = function (block) {
        $.ajax(
            {
                url: '/chapterDetail',
                type: "get",
                datatype: "html",
                beforeSend: function () {
                    window.totR++;
                    //Block UI.
                    if (block) {
                        $(block).block();
                    }
                },
            })

            .done(function (data) {
                $("#chapter_detail_body").empty().html(data);

            })
            .fail(function (jqXHR, ajaxOptions, thrownError) {
                alert('No response from server');
            });

    };

    /** delete Author **/
    var deleteAuthor = function () {
        $(document).on('click', '.btn_remove_author', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            swal({
                title: 'Are You sure ?',
                text: 'Once deleted, you will not be able to recover this Author!',
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    swal('Poof! Your Author has been deleted!', {
                        icon: "success",
                    });

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: '/deleteAuthorByid',
                        type: 'POST',
                        dataType: 'json',
                        data: {id: id},
                        beforeSend: function () {
                            window.totR++;
                            /**Block UI. **/
                            $('.body').block();
                        },
                        success: function (res) {
                            if (res == 1) {
                                $.notify({
                                    title: '<strong>Success</strong>',
                                    icon: 'glyphicon glyphicon-star',
                                    message: "Delete Author Successfull!"
                                }, {
                                    type: 'success',
                                    animate: {
                                        enter: 'animated fadeInUp',
                                        exit: 'animated fadeOutRight'
                                    },
                                    placement: {
                                        from: "bottom",
                                        align: "left"
                                    },
                                    offset: 20,
                                    spacing: 10,
                                    z_index: 1031,
                                });
                                $('#tbl_author').DataTable().ajax.reload();
                            } else {
                                $.notify({
                                    title: '<strong>Failed</strong>',
                                    icon: 'glyphicon glyphicon-star',
                                    message: "Delete Author Failed!"
                                }, {
                                    type: 'danger',
                                    animate: {
                                        enter: 'animated fadeInUp',
                                        exit: 'animated fadeOutRight'
                                    },
                                    placement: {
                                        from: "bottom",
                                        align: "left"
                                    },
                                    offset: 20,
                                    spacing: 10,
                                    z_index: 1031,
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        complete: function (ret) {
                            window.totR--
                            /**Unblock UI. **/
                            $('.body').unblock();
                        }
                    });

                } else {
                    swal('Your Author is safe!');
                }
            });
        })
    }

    $('.btn_add_author').on('click', function (e) {
        e.preventDefault();
        $('#modal_add_author').modal('show');
    });

    $('.btn_ins_author').on('click', function (e) {
        var data = $('#form_ins_author').serialize();
        ajaxPostBlock('/insertAuthor', data, function (res) {
            if (res == 1) {
                $.notify({
                    title: '<strong>Success</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data insert success !"
                }, {
                    type: 'success',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#modal_add_author').modal('hide');
                $('#tbl_author').DataTable().ajax.reload();
            } else {
                $.notify({
                    title: '<strong>Failed</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data insert failed"
                }, {
                    type: 'danger',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#modal_add_author').modal('hide');
                $('#tbl_author').DataTable().ajax.reload();
            }
        })
    });
    /** End author **/

    /** chapter link page **/
    // var chapterLinkTable = $('#tbl_chapLink').DataTable({
    //     "processing": true,
    //     "serverSide": true,
    //     ajax: {
    //         "url":'/linkChapter',
    //     },
    //     scrollY: 200,
    //     deferRender:    true,
    //     scroller:       true,
    //     stateSave: true,
    //     "deferRender": true,
    //     "columns": [
    //         {
    //             "data": "id",
    //             className: "col-xs-1"
    //         },
    //         {
    //             "data": null,
    //             "render": function ( data, type, full, meta ) {
    //                 var name = full.book;
    //                 var chapName = full.chapName;
    //                 return '<strong><i class="fa fa-fw fa-bookmark"></i>'+chapName+'</strong><div class="book bold" style="padding-top: 5px; color: red;"><span class="glyphicon glyphicon-book"></span> '+name+'</div>';
    //
    //             },
    //             className: "col-xs-2"
    //         },
    //         {
    //             "data": null,
    //             "render": function ( data, type, full, meta ) {
    //                 var url = full.source_url;
    //                 var getTime = full.getTime;
    //                 if(getTime == null){
    //                     return '<a href='+url+' title="source url">'+url+'</a>';
    //                 }else{
    //                     return '<del><a href='+url+' title="source url">'+url+'</a></del>';
    //                 }
    //             },
    //                 className: "col-xs-3"
    //         },
    //         {
    //             "data": "hash_url",
    //             className: "col-xs-3"
    //         },
    //         {
    //             "data": "getTime",
    //             className: "col-xs-1"
    //         },
    //         {
    //             "data": null,
    //             "render": function ( data, type, full, meta ) {
    //                 var buttonID = full.id;
    //                 var getTime = full.getTime;
    //                 if(getTime == null){
    //                     return '<div class="ui-group-buttons"><a href="" class="btn btn-primary btn_crawl_chapter" role="button" data-id="'+buttonID+'"><span class="glyphicon glyphicon-download-alt"></span></a><div class="or"></div><a href="" class="btn btn-success btn_edit_chap_link" role="button" data-id="'+buttonID+'"><span class="glyphicon glyphicon-edit"></span></a><div class="or"></div><a href="" class="btn btn-danger btn_remove_chap_link" role="button" data-id="'+buttonID+'"><span class="glyphicon glyphicon-trash"></span></a></div>';
    //                 }else{
    //                     return '<div class="ui-group-buttons"><a href="" class="btn btn-primary btn_crawl_chapter disabled" role="button" data-id="'+buttonID+'" disabled><span class="glyphicon glyphicon-download-alt"></span></a><div class="or"></div><a href="" class="btn btn-success btn_edit_chap_link" role="button" data-id="'+buttonID+'"><span class="glyphicon glyphicon-edit"></span></a><div class="or"></div><a href="" class="btn btn-danger btn_remove_chap_link" role="button" data-id="'+buttonID+'"><span class="glyphicon glyphicon-trash"></span></a></div>';
    //                 }
    //             },
    //             className: "option_col col-xs-3",
    //         }
    //     ],
    // });

    $(document).on('click', '.btn_crawl_chapter', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        swal({
            title: 'Are You sure ?',
            text: 'Once click this Chap will be crawl!',
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                swal('Poof! Your Chap Link has been crawl!', {
                    icon: "success",
                });

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: '/getChapdetailByidChapLink',
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                    beforeSend: function () {
                        window.totR++;
                        /***Block UI. **/
                        $('#tbl_chapLink').block();
                    },
                    success: function (res) {
                        if (res == 1) {
                            $.notify({
                                title: '<strong>Success</strong>',
                                icon: 'glyphicon glyphicon-star',
                                message: "Crawl Chap Link Successfull!"
                            }, {
                                type: 'success',
                                animate: {
                                    enter: 'animated fadeInUp',
                                    exit: 'animated fadeOutRight'
                                },
                                placement: {
                                    from: "bottom",
                                    align: "left"
                                },
                                offset: 20,
                                spacing: 10,
                                z_index: 1031,
                            });
                            $('#tbl_chapLink').DataTable().ajax.reload();
                        } else {
                            $.notify({
                                title: '<strong>Failed</strong>',
                                icon: 'glyphicon glyphicon-star',
                                message: "Crawl Chap Link Failed!"
                            }, {
                                type: 'danger',
                                animate: {
                                    enter: 'animated fadeInUp',
                                    exit: 'animated fadeOutRight'
                                },
                                placement: {
                                    from: "bottom",
                                    align: "left"
                                },
                                offset: 20,
                                spacing: 10,
                                z_index: 1031,
                            });
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    },
                    complete: function (ret) {
                        window.totR--
                        /***Unblock UI. **/
                        $('#tbl_chapLink').unblock();
                    }
                });

            } else {
                swal('Your Chap Link is safe!');
            }
        });

    })

    /** edit chapter link **/
    $(document).on('click', '.btn_edit_chap_link', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        $('#modal_edit_chapter_link').modal('show');

        ajaxPostBlock('/getChapterLinkByid', {id: id}, function (res) {
            $('#inputSourceUrl').val(res.source_url);
            $('.btn_save_chapter_link').attr('data-id', res.id);

            $('.select_book').select2({
                ajax: {
                    url: "/findBookbyName",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term /** search term **/
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }

                            })
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });

            var newOption = new Option(res.book, res.id_books, false, false);
            newOption.selected = true;
            $('.select_book').append(newOption).trigger('change');
        })
    });

    $('#modal_edit_chapter_link').on('hidden.bs.modal', function () {
        $('#inputSourceUrl').val('');
        $('.select_book').val(null).trigger('change');
    });

    var editChaplink = function () {
        $('.btn_save_chapter_link').on('click', function () {
            var select_book = $('.select_book').val();
            var inputSourceUrl = $('#inputSourceUrl').val();
            var id = $(this).attr('data-id');

            ajaxPostBlock('/updateChapterLink', {
                id: id,
                select_book: select_book,
                inputSourceUrl: inputSourceUrl
            }, function (res) {
                if (res == 1) {
                    $.notify({
                        title: '<strong>Success</strong>',
                        icon: 'glyphicon glyphicon-star',
                        message: "Data update success !"
                    }, {
                        type: 'success',
                        animate: {
                            enter: 'animated fadeInUp',
                            exit: 'animated fadeOutRight'
                        },
                        placement: {
                            from: "bottom",
                            align: "left"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                    });
                    $('#modal_edit_chapter_link').modal('hide');
                    $('#tbl_chapLink').DataTable().ajax.reload();
                } else {
                    $.notify({
                        title: '<strong>Failed</strong>',
                        icon: 'glyphicon glyphicon-star',
                        message: "Data update failed"
                    }, {
                        type: 'danger',
                        animate: {
                            enter: 'animated fadeInUp',
                            exit: 'animated fadeOutRight'
                        },
                        placement: {
                            from: "bottom",
                            align: "left"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                    });
                    $('#modal_edit_chapter_link').modal('hide');
                    $('#tbl_chapLink').DataTable().ajax.reload();
                }

            }, '#modal_edit_chapter_link');
        });
    }

    /** delete chap link **/
    var deleteChapLink = function () {
        $(document).on('click', '.btn_remove_chap_link', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            swal({
                title: 'Are You sure ?',
                text: 'Once deleted, you will not be able to recover this Chap Link!',
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    swal('Poof! Your Chap Link has been deleted!', {
                        icon: "success",
                    });

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: '/deleteChapLinkByid',
                        type: 'POST',
                        dataType: 'json',
                        data: {id: id},
                        beforeSend: function () {
                            window.totR++;
                            /***Block UI. **/
                            $('.body').block();
                        },
                        success: function (res) {
                            if (res == 1) {
                                $.notify({
                                    title: '<strong>Success</strong>',
                                    icon: 'glyphicon glyphicon-star',
                                    message: "Delete Chap Link Successfull!"
                                }, {
                                    type: 'success',
                                    animate: {
                                        enter: 'animated fadeInUp',
                                        exit: 'animated fadeOutRight'
                                    },
                                    placement: {
                                        from: "bottom",
                                        align: "left"
                                    },
                                    offset: 20,
                                    spacing: 10,
                                    z_index: 1031,
                                });
                                $('#tbl_chapLink').DataTable().ajax.reload();
                            } else {
                                $.notify({
                                    title: '<strong>Failed</strong>',
                                    icon: 'glyphicon glyphicon-star',
                                    message: "Delete Chap Link Failed!"
                                }, {
                                    type: 'danger',
                                    animate: {
                                        enter: 'animated fadeInUp',
                                        exit: 'animated fadeOutRight'
                                    },
                                    placement: {
                                        from: "bottom",
                                        align: "left"
                                    },
                                    offset: 20,
                                    spacing: 10,
                                    z_index: 1031,
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        complete: function (ret) {
                            window.totR--
                            /***Unblock UI. **/
                            $('.body').unblock();
                        }
                    });

                } else {
                    swal('Your Chap Link is safe!');
                }
            });
        });
    }

    /** Refresh data chapter link **/
    $('.refresh_dataBook').on('click', function (e) {
        // e.preventDefault();
        // $('#tbl_chapLink').DataTable().ajax.reload();
        // $('#tbl_chapLink').block();
        // setTimeout(function () {
        //     $('#tbl_chapLink').unblock();
        // },200);
    })


    $('.btn_add_chap_link').on('click', function (e) {
        e.preventDefault();
        $('#modal_add_chapter_link').modal('show');
        $('.ins_select_book').select2({
            ajax: {
                url: "/findBookbyName",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term /** search term **/
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }

                        })
                    };
                },
                cache: true
            },
            minimumInputLength: 2
        });
    })

    /** End chapter link **/

    /** xpath setting **/
    var xpathTable = $('#tbl_xpath').DataTable({
        ajax: {
            "url": '/xpath',
        },
        "deferRender": true,
        "columns": [
            {"data": "id", className: "col-xs-1"},
            {
                "data": "xpath_name",
                className: "col-xs-4"
            },
            {
                "data": "xpath_options",
                className: "col-xs-4"
            },
            {
                "data": "created_at",
                className: "col-xs-2"
            },
            {
                "data": null,
                "render": function (data, type, full, meta) {
                    var buttonID = full.id;
                    return '<div class="ui-group-buttons"><a href="" class="btn btn-success btn_edit_xpath" role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-edit"></span></a><div class="or"></div><a href="" class="btn btn-danger btn_remove_xpath" role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-trash"></span></a></div>';
                },
                className: "option_col col-xs-1",
            }
        ],
    });

    /** edit xpath **/
    $(document).on('click', '.btn_edit_xpath', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        $('#modal_edit_xpath').modal('show');

        ajaxPostBlock('/getXpathByid', {id: id}, function (res) {
            $('#inputXpathName').val(res.xpath_name);
            $('#inputXpathOptions').val(res.xpath_options);
            $('.btn_save_xpath').attr('data-id', res.id);
        })
    });

    $('#modal_edit_xpath').on('hidden.bs.modal', function () {
        $('#inputXpathName').val();
        $('#inputXpathOptions').val();
        $('.btn_save_xpath').attr('data-id', '');
    });

    var editXpath = function () {
        $('.btn_save_xpath').on('click', function () {
            var xpathName = $('#inputXpathName').val();
            var xpathOptions = $('#inputXpathOptions').val();
            var id = $('.btn_save_xpath').attr('data-id');

            ajaxPostBlock('/updateXpath', {id: id, xpathOptions: xpathOptions, xpathName: xpathName}, function (res) {
                if (res == 1) {
                    $.notify({
                        title: '<strong>Success</strong>',
                        icon: 'glyphicon glyphicon-star',
                        message: "Data update success !"
                    }, {
                        type: 'success',
                        animate: {
                            enter: 'animated fadeInUp',
                            exit: 'animated fadeOutRight'
                        },
                        placement: {
                            from: "bottom",
                            align: "left"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                    });
                    $('#modal_edit_xpath').modal('hide');
                    $('#tbl_xpath').DataTable().ajax.reload();
                } else {
                    $.notify({
                        title: '<strong>Failed</strong>',
                        icon: 'glyphicon glyphicon-star',
                        message: "Data update failed"
                    }, {
                        type: 'danger',
                        animate: {
                            enter: 'animated fadeInUp',
                            exit: 'animated fadeOutRight'
                        },
                        placement: {
                            from: "bottom",
                            align: "left"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                    });
                    $('#modal_edit_xpath').modal('hide');
                    $('#tbl_xpath').DataTable().ajax.reload();
                }

            }, '#modal_edit_xpath');
        });
    }

    /** delete xpath **/
    var deleteXpath = function () {
        $(document).on('click', '.btn_remove_xpath', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            swal({
                title: 'Are You sure ?',
                text: 'Once deleted, you will not be able to recover this Xpath!',
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    swal('Poof! Your Chap Xpath has been deleted!', {
                        icon: "success",
                    });

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: '/deleteXpathByid',
                        type: 'POST',
                        dataType: 'json',
                        data: {id: id},
                        beforeSend: function () {
                            window.totR++;
                            /***Block UI. **/
                            $('.body').block();
                        },
                        success: function (res) {
                            if (res == 1) {
                                $.notify({
                                    title: '<strong>Success</strong>',
                                    icon: 'glyphicon glyphicon-star',
                                    message: "Delete Xpath Successfull!"
                                }, {
                                    type: 'success',
                                    animate: {
                                        enter: 'animated fadeInUp',
                                        exit: 'animated fadeOutRight'
                                    },
                                    placement: {
                                        from: "bottom",
                                        align: "left"
                                    },
                                    offset: 20,
                                    spacing: 10,
                                    z_index: 1031,
                                });
                                $('#tbl_xpath_wrapper').DataTable().ajax.reload();
                            } else {
                                $.notify({
                                    title: '<strong>Failed</strong>',
                                    icon: 'glyphicon glyphicon-star',
                                    message: "Delete Xpath Failed!"
                                }, {
                                    type: 'danger',
                                    animate: {
                                        enter: 'animated fadeInUp',
                                        exit: 'animated fadeOutRight'
                                    },
                                    placement: {
                                        from: "bottom",
                                        align: "left"
                                    },
                                    offset: 20,
                                    spacing: 10,
                                    z_index: 1031,
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        complete: function (ret) {
                            window.totR--
                            /***Unblock UI. **/
                            $('.body').unblock();
                        }
                    });

                } else {
                    swal('Your Chap Link is safe!');
                }
            });
        });
    }

    /** add Xpath **/
    $('.btn_add_xpath').on('click', function (e) {
        e.preventDefault();
        $('#modal_add_xpath').modal('show');
    })

    $('.btn_ins_xpath').on('click', function (e) {
        e.preventDefault();
        var data = $('#form_ins_xpath').serialize();
        ajaxPostBlock('/insertXpath', data, function (res) {
            if (res == 1) {
                $.notify({
                    title: '<strong>Success</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data insert success !"
                }, {
                    type: 'success',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#modal_add_xpath').modal('hide');
                $('#tbl_xpath').DataTable().ajax.reload();
            } else {
                $.notify({
                    title: '<strong>Failed</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data insert failed"
                }, {
                    type: 'danger',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#modal_add_xpath').modal('hide');
                $('#tbl_xpath').DataTable().ajax.reload();
            }
        }, '#modal_add_xpath');
    })


    /** End xpath setting **/

    /** Cron Setting **/
    var cronSettingTable = $('#tbl_cron_config').DataTable({
        ajax: {
            "url": '/cronSetting',
        },
        "deferRender": true,
        "columns": [
            {"data": "id"},
            {
                "data": "cron_name",
                className: "col-xs-2"
            },
            {
                "data": "xpath",
                className: "col-xs-3"
            },
            {
                "data": "cron_setting",
                className: "col-xs-3"
            },
            {
                "data": "level",
                className: "col-xs-2"
            },
            {
                "data": null,
                "render": function (data, type, full, meta) {
                    var buttonID = full.id;
                    return '<div class="ui-group-buttons"><a href="" class="btn btn-success btn_edit_cron" role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-edit"></span></a><div class="or"></div><a href="" class="btn btn-danger btn_remove_cron" role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-trash"></span></a></div>';
                },
                className: "option_col",
            }
        ],
    })

    $(document).on('click', '.btn_edit_cron', function (e) {
        e.preventDefault();
        $('#modal_edit_cron_config').modal('show');
        var id = $(this).attr('data-id');
        $('.btn_save_cron_setting').attr('data-id', id);

        ajaxPostBlock('/getCronById', {id: id}, function (res) {
            $('#inputCronName').val(res.cron_name);
            $('#inputCronSetting').val(res.cron_configs);
            $('#inputLevel').val(res.level);

            $('.select_xpath').select2({
                ajax: {
                    url: "/findXpathbyName",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term /** search term **/
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }

                            })
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            })

            var newOption = new Option(res.xpath_name, res.id_xpath, false, false);
            newOption.selected = true;
            $('.select_xpath').append(newOption).trigger('change');
        })

    })

    var editCron = function () {
        $(document).on('click', '.btn_save_cron_setting', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            var cronName = $('#inputCronName').val();
            var cronSetting = $('#inputCronSetting').val();
            var xpath = $('.select_xpath').val();
            var level = $('#inputLevel').val();

            ajaxPostBlock('/editCronByid', {
                id: id,
                cronName: cronName,
                cronSetting: cronSetting,
                xpath: xpath,
                level: level
            }, function (res) {
                if (res == 1) {
                    $.notify({
                        title: '<strong>Success</strong>',
                        icon: 'glyphicon glyphicon-star',
                        message: "Data update success !"
                    }, {
                        type: 'success',
                        animate: {
                            enter: 'animated fadeInUp',
                            exit: 'animated fadeOutRight'
                        },
                        placement: {
                            from: "bottom",
                            align: "left"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                    });
                    $('#modal_edit_cron_config').modal('hide');
                    $('#tbl_cron_config').DataTable().ajax.reload();
                } else {
                    $.notify({
                        title: '<strong>Failed</strong>',
                        icon: 'glyphicon glyphicon-star',
                        message: "Data update failed"
                    }, {
                        type: 'danger',
                        animate: {
                            enter: 'animated fadeInUp',
                            exit: 'animated fadeOutRight'
                        },
                        placement: {
                            from: "bottom",
                            align: "left"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                    });
                    $('#modal_edit_cron_config').modal('hide');
                    $('#tbl_cron_config').DataTable().ajax.reload();
                }
            })
        })
    }

    var deleteCron = function () {
        $(document).on('click', '.btn_remove_cron', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-id');

            swal({
                title: 'Are You sure ?',
                text: 'Once deleted, you will not be able to recover this Cron Configs!',
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    swal('Poof! Your Cron Configs has been deleted!', {
                        icon: "success",
                    });

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: '/deleteCronByid',
                        type: 'POST',
                        dataType: 'json',
                        data: {id: id},
                        beforeSend: function () {
                            window.totR++;
                            /***Block UI. **/
                            $('.body').block();
                        },
                        success: function (res) {
                            if (res == 1) {
                                $.notify({
                                    title: '<strong>Success</strong>',
                                    icon: 'glyphicon glyphicon-star',
                                    message: "Delete Cron Configs Successfull!"
                                }, {
                                    type: 'success',
                                    animate: {
                                        enter: 'animated fadeInUp',
                                        exit: 'animated fadeOutRight'
                                    },
                                    placement: {
                                        from: "bottom",
                                        align: "left"
                                    },
                                    offset: 20,
                                    spacing: 10,
                                    z_index: 1031,
                                });
                                $('#tbl_cron_config').DataTable().ajax.reload();
                            } else {
                                $.notify({
                                    title: '<strong>Failed</strong>',
                                    icon: 'glyphicon glyphicon-star',
                                    message: "Delete Cron Configs Failed!"
                                }, {
                                    type: 'danger',
                                    animate: {
                                        enter: 'animated fadeInUp',
                                        exit: 'animated fadeOutRight'
                                    },
                                    placement: {
                                        from: "bottom",
                                        align: "left"
                                    },
                                    offset: 20,
                                    spacing: 10,
                                    z_index: 1031,
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        complete: function (ret) {
                            window.totR--
                            /***Unblock UI. **/
                            $('.body').unblock();
                        }
                    });

                } else {
                    swal('Your Category is safe!');
                }
            });
        })

    }

    $('.btn_add_cron_cf').on('click', function (e) {
        e.preventDefault();
        $('#modal_add_cron_setting').modal('show');
        $('.insselect_xpath').select2({
            ajax: {
                url: "/findXpathbyName",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term /** search term **/
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }

                        })
                    };
                },
                cache: true
            },
            minimumInputLength: 2
        })
    })

    $('.btn_ins_cron_setting').on('click', function (e) {
        e.preventDefault();
        var cronName = $('#insinputCronName').val();
        var insinputCronSetting = $('#insinputCronSetting').val();
        var insinputLevel = $('#insinputLevel').val();
        var insselect_xpath = $('.insselect_xpath').val();
        ajaxPostBlock('/insertCronSetting', {
            cronName: cronName,
            insinputCronSetting: insinputCronSetting,
            insinputLevel: insinputLevel,
            insselect_xpath: insselect_xpath
        }, function (res) {
            if (res == 1) {
                $.notify({
                    title: '<strong>Success</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data insert success !"
                }, {
                    type: 'success',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#modal_add_cron_setting').modal('hide');
                $('#tbl_cron_config').DataTable().ajax.reload();
            } else {
                $.notify({
                    title: '<strong>Failed</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data insert failed"
                }, {
                    type: 'danger',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#modal_add_cron_setting').modal('hide');
                $('#tbl_cron_config').DataTable().ajax.reload();
            }
        }, '#modal_add_cron_setting')
    })
    /** End Cron setting **/

    /** Chapter links **/
    // var chapterLinkTable = $('#tbl_chapter_detail').DataTable({
    //     ajax: {
    //         "url": '/',
    //     },
    //     // "processing": true,
    //     // "serverSide": true,
    //     "deferRender": true,
    //     "columns": [
    //         {
    //             "data": "id",
    //             className: "col-xs-1"
    //         },
    //         {
    //             "data": "book_name",
    //             className: "col-xs-2"
    //         },
    //         {
    //             "data": null,
    //             "render": function (data, type, full, meta) {
    //                 var chapterName = full.chapName;
    //                 var url = full.source_url;
    //                 return '<a href="' + url + '" class="source_link" title="source_link">' + chapterName + '</a>';
    //             },
    //             className: "col-xs-4"
    //         },
    //         {
    //             "data": "created_at",
    //             className: "col-xs-2"
    //         },
    //         /*         {
    //                      "data": "author_name",
    //                      className: "col-xs-2"
    //                  },*/
    //         {
    //             "data": null,
    //             "render": function (data, type, full, meta) {
    //                 var buttonID = full.id;
    //                 var status = full.status;
    //
    //                 if (status == 1) {
    //                     return '<div class="ui-group-buttons">' +
    //                         '<a href="" class="btn btn-primary btn_publish_chap_detail"  role="button" data-id="' + buttonID + '" disabled><span class="glyphicon glyphicon-cloud-upload"></span></a><div class="or"></div>' +
    //                         '<a href="" class="btn btn-success btn_edit_chap_detail" role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-edit"></span></a><div class="or"></div><a href="" class="btn btn-danger btn_remove_chap_detail" role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-trash"></span></a></div>';
    //                 } else {
    //                     return '<div class="ui-group-buttons">' +
    //                         '<a href="" class="btn btn-primary btn_publish_chap_detail" role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-cloud-upload"></span></a><div class="or"></div>' +
    //                         '<a href="" class="btn btn-success btn_edit_chap_detail" role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-edit"></span></a><div class="or"></div><a href="" class="btn btn-danger btn_remove_chap_detail" role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-trash"></span></a></div>';
    //                 }
    //
    //             },
    //             className: "option_col",
    //         }
    //     ],
    // })

    var updateChapterLink = function () {
        $(document).on('click', '.btn_edit_chap_detail', function (e) {
            e.preventDefault();
            $('#modal_edit_chapter_detail').modal('show');
            var id = $(this).attr('data-id');
            var ed = tinyMCE.get('chap_content');
            $('.btn_save_chapter_detail').attr('data-id', id);
            ajaxPostBlock('/getChapterDetailByid', {id: id}, function (res) {
                $('#inputChapname').val(res.chapName);
                $('#inputChapurl').val(res.source_url);
                ed.setContent(res.chapContent);

                $('.select_book').select2({
                    ajax: {
                        url: "/findBookbyName",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: params.term /** search term **/
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data, function (item) {
                                    return {
                                        text: item.name,
                                        id: item.id
                                    }

                                })
                            };
                        },
                        cache: true
                    },
                    minimumInputLength: 2
                });

                var newOption = new Option(res.book_name, res.book_id, false, false);
                newOption.selected = true;
                $('.select_book').append(newOption).trigger('change');
            })
        })
    }

    $('.btn_save_chapter_detail').on('click', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var chapName = $('#inputChapname').val();
        var chapUrl = $('#inputChapurl').val();
        var book_id = $('.select_book').val();
        var chapContent = tinyMCE.get('chap_content').getContent();
        ajaxPostBlock('/updateChapDetail', {
            id: id,
            chapName: chapName,
            chapUrl: chapUrl,
            book_id: book_id,
            chapContent: chapContent
        }, function (res) {
            if (res == 1) {
                $.notify({
                    title: '<strong>Success</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data update success !"
                }, {
                    type: 'success',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#modal_edit_chapter_detail').modal('hide');
                ReloadChapterDetail();
                // $('#tbl_chapLink').DataTable().ajax.reload();
            } else {
                $.notify({
                    title: '<strong>Failed</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data update failed"
                }, {
                    type: 'danger',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#modal_edit_chapter_detail').modal('hide');
                $('#tbl_chapLink').DataTable().ajax.reload();
            }
        }, '#modal_edit_chapter_detail');
    })

    $('#modal_edit_chapter_detail').on('hidden.bs.modal', function () {
        var ed = tinyMCE.get('chap_content');
        $('#inputChapname').val();
        $('#inputChapurl').val();
        ed.setContent('');
        $('.select_book').val(null).trigger('change');
    });

    var deleteChapdetail = function () {
        $(document).on('click', '.btn_remove_chap_detail', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-id');

            swal({
                title: 'Are You sure ?',
                text: 'Once deleted, you will not be able to recover thisChapter!',
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    swal('Poof! This Chapter has been deleted!', {
                        icon: "success",
                    });

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: '/deleteChapdetail',
                        type: 'POST',
                        dataType: 'json',
                        data: {id: id},
                        beforeSend: function () {
                            window.totR++;
                            /***Block UI. **/
                            $('.body').block();
                        },
                        success: function (res) {
                            if (res == 1) {
                                $.notify({
                                    title: '<strong>Success</strong>',
                                    icon: 'glyphicon glyphicon-star',
                                    message: "Delete Chapter Successfull!"
                                }, {
                                    type: 'success',
                                    animate: {
                                        enter: 'animated fadeInUp',
                                        exit: 'animated fadeOutRight'
                                    },
                                    placement: {
                                        from: "bottom",
                                        align: "left"
                                    },
                                    offset: 20,
                                    spacing: 10,
                                    z_index: 1031,
                                });
                                // $('#tbl_chapter_detail').DataTable().ajax.reload();
                                ReloadChapterDetail('#tbl_chapter_detail');
                            } else {
                                $.notify({
                                    title: '<strong>Failed</strong>',
                                    icon: 'glyphicon glyphicon-star',
                                    message: "Delete Chapter Failed!"
                                }, {
                                    type: 'danger',
                                    animate: {
                                        enter: 'animated fadeInUp',
                                        exit: 'animated fadeOutRight'
                                    },
                                    placement: {
                                        from: "bottom",
                                        align: "left"
                                    },
                                    offset: 20,
                                    spacing: 10,
                                    z_index: 1031,
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        complete: function (ret) {
                            window.totR--
                            /***Unblock UI. **/
                            $('.body').unblock();
                        }
                    });

                } else {
                    swal('Your Category is safe!');
                }
            });
        })
    }

    $('.btn_add_chapter_detail').on('click', function (e) {
        e.preventDefault();
        $('#modal_add_chapter_detail').modal('show');

        $('.insselect_book').select2(
            {
                ajax: {
                    url: "/findBookbyName",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term /** search term **/
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }

                            })
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            }
        );


    })

    $('.btn_ins_chap_detail').on('click', function (e) {
        var chapName = $('#insinputChapname').val();
        var chapUrl = $('#insinputChapurl').val();
        var book_id = $('.insselect_book').val();
        var chapContent = $('#inschap_content').val();
        ajaxPostBlock('/insertChapDetail', {
            chapName: chapName,
            chapUrl: chapUrl,
            book_id: book_id,
            chapContent: chapContent
        }, function (res) {
            if (res == 1) {
                $.notify({
                    title: '<strong>Success</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data insert success !"
                }, {
                    type: 'success',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#modal_add_chapter_detail').modal('hide');
                $('#tbl_chapter_detail').DataTable().ajax.reload();
            } else {
                $.notify({
                    title: '<strong>Failed</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data insert failed"
                }, {
                    type: 'danger',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#modal_add_chapter_detail').modal('hide');
                $('#tbl_chapter_detail').DataTable().ajax.reload();
            }
        }, '#modal_add_chapter_detail');
    })

    $('#modal_add_chapter_detail').on('hidden.bs.modal', function () {
        var ed = tinyMCE.get('inschap_content');
        $('#insinputChapname').val('');
        $('#insinputChapurl').val('');
        ed.setContent('');
        $('.insselect_book').val(null).trigger('change');
    });
    /** End Chapter Links**/

    /** Book link **/
    // var bookLinkTable = $('#tbl_book_link').DataTable({
    //     ajax: {
    //         "url": '/bookLink',
    //     },
    //     "deferRender": true,
    //     "columns": [
    //         {
    //             "data": "id",
    //             className: "col-xs-1"
    //         },
    //         {
    //             "data": null,
    //             "render": function (data, type, full, meta) {
    //                 var category = full.category;
    //                 var source_url = full.source_url;
    //                 return '<a href="' + source_url + '" title="' + category + '">' + category + '</a>';
    //             },
    //             className: "col-xs-2"
    //         },
    //         {
    //             "data": null,
    //             "render": function (data, type, full, meta) {
    //                 var source_url = full.source_url;
    //                 return '<a href="' + source_url + '" title="source url">' + source_url + '</a>';
    //             },
    //             className: "col-xs-5"
    //         },
    //         {
    //             "data": "created_at",
    //             className: "col-xs-2"
    //         },
    //         {
    //             "data": null,
    //             "render": function (data, type, full, meta) {
    //                 var buttonID = full.id;
    //                 return '<div class="ui-group-buttons"><a href="" class="btn btn-primary btn_get_book_link" role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-download-alt"></span></a><div class="or"></div><a href="" class="btn btn-success btn_edit_book_link" role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-edit"></span></a><div class="or"></div><a href="" class="btn btn-danger btn_remove_book_link" role="button" data-id="' + buttonID + '"><span class="glyphicon glyphicon-trash"></span></a></div>';
    //             },
    //             className: "option_col col-xs-2",
    //         }
    //     ],
    // })

    var updateBookLink = function () {
        $(document).on('click', '.btn_edit_book_link', function (e) {
            e.preventDefault();
            $('#modal_edit_book_link').modal('show');
            var id = $(this).attr('data-id');

            $('.btn_save_book_link').attr('data-id', id);
            ajaxPostBlock('/getBookLinkByid', {id: id}, function (res) {
                $('#inputSourceBook').val(res.source_url);

                $('.select_category').select2({
                    ajax: {
                        url: "/findCatbyName",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: params.term /** search term **/
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data, function (item) {
                                    return {
                                        text: item.name,
                                        id: item.id
                                    }

                                })
                            };
                        },
                        cache: true
                    },
                    minimumInputLength: 2
                });

                var newOption = new Option(res.category, res.cat_id, false, false);
                newOption.selected = true;
                $('.select_category').append(newOption).trigger('change');
            })
        });
    }

    $('.btn_save_book_link').on('click', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var source_url = $('#inputSourceBook').val();
        var cat_id = $('.select_category').val();
        ajaxPostBlock('/updateBookLinkByid', {id: id, source_url: source_url, cat_id: cat_id}, function (res) {
            if (res == 1) {
                $.notify({
                    title: '<strong>Success</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data update success !"
                }, {
                    type: 'success',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#modal_edit_book_link').modal('hide');
                ReloadBookLink('#tbl_book_link');
            } else {
                $.notify({
                    title: '<strong>Failed</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data update failed"
                }, {
                    type: 'danger',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#modal_edit_book_link').modal('hide');
                ReloadBookLink('#tbl_book_link');
            }
        }, '#modal_edit_book_link');
    })

    var deleteBookLink = function () {
        $(document).on('click', '.btn_remove_book_link', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-id');

            swal({
                title: 'Are You sure ?',
                text: 'Once deleted, you will not be able to recover this Link!',
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    swal('Poof! Your Category has been deleted!', {
                        icon: "success",
                    });

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: '/deleteBookLinkByid',
                        type: 'POST',
                        dataType: 'json',
                        data: {id: id},
                        beforeSend: function () {
                            window.totR++;
                            /***Block UI. **/
                            $('.body').block();
                        },
                        success: function (res) {
                            if (res == 1) {
                                $.notify({
                                    title: '<strong>Success</strong>',
                                    icon: 'glyphicon glyphicon-star',
                                    message: "Delete Link Successfull!"
                                }, {
                                    type: 'success',
                                    animate: {
                                        enter: 'animated fadeInUp',
                                        exit: 'animated fadeOutRight'
                                    },
                                    placement: {
                                        from: "bottom",
                                        align: "left"
                                    },
                                    offset: 20,
                                    spacing: 10,
                                    z_index: 1031,
                                });
                                ReloadBookLink('#tbl_book_link');
                            } else {
                                $.notify({
                                    title: '<strong>Failed</strong>',
                                    icon: 'glyphicon glyphicon-star',
                                    message: "Delete Link Failed!"
                                }, {
                                    type: 'danger',
                                    animate: {
                                        enter: 'animated fadeInUp',
                                        exit: 'animated fadeOutRight'
                                    },
                                    placement: {
                                        from: "bottom",
                                        align: "left"
                                    },
                                    offset: 20,
                                    spacing: 10,
                                    z_index: 1031,
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        complete: function (ret) {
                            window.totR--
                            /***Unblock UI. **/
                            $('.body').unblock();
                        }
                    });

                } else {
                    swal('Your Link is safe!');
                }
            });
        })
    }

    $('.btn_add_book_link').on('click', function () {
        $('#modal_add_book_link').modal('show');

        $('.insSelectCat').select2({
            ajax: {
                url: "/findCatbyName",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term /** search term **/
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }

                        })
                    };
                },
                cache: true
            },
            minimumInputLength: 2
        });
    });

    $('.btn_ins_book_link').on('click', function () {

        var data = $('#form_ins_book').serialize();
        ajaxPostBlock('/insertBookLink', data, function (res) {
            if (res == 1) {
                $.notify({
                    title: '<strong>Success</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data insert success !"
                }, {
                    type: 'success',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#modal_add_book_link').modal('hide');
                ReloadBookLink('#tbl_book_link');
            } else {
                $.notify({
                    title: '<strong>Failed</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data insert failed"
                }, {
                    type: 'danger',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });
                $('#modal_add_book_link').modal('hide');
                ReloadBookLink('#tbl_book_link');
            }
        }, '#modal_add_book_link');
    });

    $(document).on('click', '.btn_get_book_link', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');

        swal({
            title: 'Are You sure ?',
            text: 'This book will be crawl!',
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: '/getChapterLinkByBook',
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                    beforeSend: function () {
                        window.totR++;
                        /***Block UI. **/
                        $('#book_link_body').block();
                    },
                    success: function (res) {
                        if (res == 1) {
                            swal('Poof! This book has been get!', {
                                icon: "success",
                            });

                            $.notify({
                                title: '<strong>Success</strong>',
                                icon: 'glyphicon glyphicon-star',
                                message: "This book Crawl Successfull!"
                            }, {
                                type: 'success',
                                animate: {
                                    enter: 'animated fadeInUp',
                                    exit: 'animated fadeOutRight'
                                },
                                placement: {
                                    from: "bottom",
                                    align: "left"
                                },
                                offset: 20,
                                spacing: 10,
                                z_index: 1031,
                            });
                            ReloadBookLink('#tbl_book_link');
                        } else {
                            swal('Poof! This book get failed!', {
                                icon: "danger",
                            });
                            $.notify({
                                title: '<strong>Failed</strong>',
                                icon: 'glyphicon glyphicon-star',
                                message: "This book Crawl Failed!"
                            }, {
                                type: 'danger',
                                animate: {
                                    enter: 'animated fadeInUp',
                                    exit: 'animated fadeOutRight'
                                },
                                placement: {
                                    from: "bottom",
                                    align: "left"
                                },
                                offset: 20,
                                spacing: 10,
                                z_index: 1031,
                            });
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        swal('Poof! This book get failed!', {
                            icon: "error",
                        });
                        $.notify({
                            title: '<strong>Failed</strong>',
                            icon: 'glyphicon glyphicon-star',
                            message: "This book Crawl Failed!"
                        }, {
                            type: 'danger',
                            animate: {
                                enter: 'animated fadeInUp',
                                exit: 'animated fadeOutRight'
                            },
                            placement: {
                                from: "bottom",
                                align: "left"
                            },
                            offset: 20,
                            spacing: 10,
                            z_index: 1031,
                        });
                    },
                    complete: function (ret) {
                        window.totR--
                        /***Unblock UI. **/
                        $('#book_link_body').unblock();
                    }
                });
            }
        });
    })
    /** End Book link **/

    /** Scan new chap link ***/
    $('.scan_new_chap_link').on('click', function (e) {
        e.preventDefault();
        ajaxPostBlock('/getLinkChapFromIndex', '', function (res) {
            if (res == 1) {
                $.notify({
                    title: '<strong>Success</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data Scan success !"
                }, {
                    type: 'success',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });

            } else {
                $.notify({
                    title: '<strong>Failed</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data Scan failed"
                }, {
                    type: 'danger',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });

            }
        }, '#tbl_chapLink');
    })


    /**load init **/
    /**category load **/
    updateCat();
    deleteCat();

    /**book load **/
    editBook();
    deleteBook();

    /** Author load **/
    editAuthor();
    deleteAuthor();

    /** Chapter link **/
    editChaplink();
    deleteChapLink();

    /** Xpath **/
    editXpath();
    deleteXpath();

    /** Cron Setting **/
    editCron();
    deleteCron();

    /** chapter link **/
    updateChapterLink();
    deleteChapdetail();

    /** book link **/
    updateBookLink();
    deleteBookLink();

    tinymceLoad('.bookdescription');
    tinymceLoad('#insBookdescription');
    tinymceLoad('#chap_content');
    tinymceLoad('#inschap_content');

    addActive();


    /** publish **/
    $(document).on('click', '.btn_publish_chap_detail', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        ajaxPostBlock('/publishByChapDetail', {id: id}, function (res) {
            if (res == 1) {
                $.notify({
                    title: '<strong>Success</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data publish success !"
                }, {
                    type: 'success',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });

                $('#tbl_chapter_detail').DataTable().ajax.reload();
            } else {
                $.notify({
                    title: '<strong>Failed</strong>',
                    icon: 'glyphicon glyphicon-star',
                    message: "Data publish failed"
                }, {
                    type: 'danger',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    },
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                });

                $('#tbl_chapter_detail').DataTable().ajax.reload();
            }
        })
    });
});
