// 1 so ham thu vien sdung de tang thoi gian code
//get Param from Url
var getParam = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

// Ajax Post block
function ajaxPostBlock(url, data, callBack, block,async){
    // async = async || 'true';
    // setup csrf for laravel proj
    $.ajaxSetup({
        headers:{
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url:url,
        type: 'POST',
        dataType: 'json',
        data: data,
        async:async,
        beforeSend: function(){
            window.totR++;
            //Block UI.
            if (block){
                $(block).block({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    },
                    message: '<h4> Loading...</h4>',
                });

            }
        },
        success: function(res){
            if(res.error) {
                alert(res.error);
                return;
            }
            if (typeof callBack === 'function') callBack(res);
        },
        error: function(jqXHR, textStatus, errorThrown){
            // console.log(textStatus);
        },
        complete: function(){
            window.totR--
            //Unblock UI.
            if (block){
                $(block).unblock();

            }
        }
    });
}

//add token vao ajax
$.ajaxSetup({
    headers:{
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

//Handle Time!
//ajax paceStart
