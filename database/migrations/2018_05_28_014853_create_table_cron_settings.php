<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCronSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cronsettings', function (Blueprint $table) {
            $table->increments('id');
            $table->text('id_xpath')->nullable();
            $table->text('cron_name')->nullable();
            $table->text('cron_configs')->nullable();
            $table->text('level')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('cronsettings');
    }
}
