<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableChapterDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('chapterdetail', function (Blueprint $table) {
            $table->increments('id');
            $table->text('id_link')->nullable();
            $table->text('id_book')->nullable();
            $table->text('chapName')->nullable();
            $table->longText('chapContent')->nullable();
            $table->text('source_url')->nullable();
            $table->text('hash_url')->nullable();
            $table->string('status')->nullable();
            $table->text('author_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('chapterdetail');
    }
}
