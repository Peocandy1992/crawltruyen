<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableChapterLink extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('chapterlinks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_books')->nullable();
            $table->string('source_url')->nullable();
            $table->string('hash_url')->nullable();
            $table->string('getTime')->nullable();
            $table->string('chapName')->nullable();
            $table->text('status')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('chapterlinks');
    }
}
