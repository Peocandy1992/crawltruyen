<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBooks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->text('images')->nullable();
            $table->text('slug')->nullable();
            $table->longText('description')->nullable();
            $table->text('id_cat')->nullable();
            $table->text('id_author')->nullable();
            $table->string('releaseStatus')->nullable();
            $table->text('paginate')->nullable();
            $table->text('paginate_left')->nullable();
            $table->text('status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('books');
    }
}
