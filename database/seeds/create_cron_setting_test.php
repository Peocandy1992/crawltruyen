<?php

use Illuminate\Database\Seeder;

class create_cron_setting_test extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cronsettings')->truncate();
        //
//        $faker = Faker\Factory::create();
//
//        $limit = 100;
//        $now = \Carbon\Carbon::now();
//
//        for ($i = 0; $i< $limit ; $i++){
//            DB::table('cronsettings')->insert([
//                'id_xpath'=>$faker->numberBetween($min = 1, $max = 5),
//                'cron_name' => $faker->name,
//                'cron_configs' => '*****',
//                'level' => $faker->numberBetween($min = 0, $max = 3),
//                'created_at'=>$now
//            ]);
//        }
    }
}
