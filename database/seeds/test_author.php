<?php

use Illuminate\Database\Seeder;

class test_author extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('author')->truncate();
        //
//        $faker = Faker\Factory::create();
//
//        $limit = 100;
//        $now = \Carbon\Carbon::now();
//
//        for ($i = 0; $i< $limit ; $i++){
//            DB::table('author')->insert([
//                'author_name'=>$faker->name,
//                'slug' => $faker->slug,
//                'created_at'=>$now
//            ]);
//        }
    }
}
