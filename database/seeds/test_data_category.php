<?php

use Illuminate\Database\Seeder;

class test_data_category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('categories')->truncate();
        /* created default category */
        $now = \Carbon\Carbon::now();
        DB::table('categories')->insert([
            'created_at'=>$now,
            'name'=>'Tiên Hiệp',
            'slug'=>'tien-hiep'
        ]);

        DB::table('categories')->insert([
            'created_at'=>$now,
            'name'=>'Kiếm Hiệp',
            'slug'=>'kiem-hiep'
        ]);

        DB::table('categories')->insert([
            'created_at'=>$now,
            'name'=>'Ngôn Tình',
            'slug'=>'ngon-tinh'
        ]);

        DB::table('categories')->insert([
            'created_at'=>$now,
            'name'=>'Truyện teen',
            'slug'=>'truyen-teen'
        ]);

        DB::table('categories')->insert([
            'created_at'=>$now,
            'name'=>'Đô thị',
            'slug'=>'do-thi'
        ]);

        DB::table('categories')->insert([
            'created_at'=>$now,
            'name'=>'Quân sự',
            'slug'=>'quan-su'
        ]);

        DB::table('categories')->insert([
            'created_at'=>$now,
            'name'=>'Lịch sử',
            'slug'=>'lich-su'
        ]);

        DB::table('categories')->insert([
            'created_at'=>$now,
            'name'=>'Xuyên không',
            'slug'=>'xuyen-khong'
        ]);

        DB::table('categories')->insert([
            'created_at'=>$now,
            'name'=>'Truyện ma',
            'slug'=>'truyen-ma'
        ]);
        DB::table('categories')->insert([
            'created_at'=>$now,
            'name'=>'Truyện trinh thám',
            'slug'=>'truyen-trinh-tham'
        ]);
        DB::table('categories')->insert([
            'created_at'=>$now,
            'name'=>'Huyền Huyễn',
            'slug'=>'huyen-huyen'
        ]);
        DB::table('categories')->insert([
            'created_at'=>$now,
            'name'=>'Khoa Huyễn',
            'slug'=>'khoa-huyen'
        ]);
        DB::table('categories')->insert([
            'created_at'=>$now,
            'name'=>'Dị Giới',
            'slug'=>'di-gioi'
        ]);
        DB::table('categories')->insert([
            'created_at'=>$now,
            'name'=>'Võng du',
            'slug'=>'vong-du'
        ]);
        DB::table('categories')->insert([
            'created_at'=>$now,
            'name'=>'Truyện ngắn',
            'slug'=>'truyen-ngan'
        ]);
        DB::table('categories')->insert([
            'created_at'=>$now,
            'name'=>'Truyện cười',
            'slug'=>'truyen-cuoi'
        ]);
        DB::table('categories')->insert([
            'created_at'=>$now,
            'name'=>'Tiểu thuyết',
            'slug'=>'tieu-thuyet'
        ]);

    }
}
