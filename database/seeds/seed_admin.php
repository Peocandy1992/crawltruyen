<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class seed_admin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $now = Carbon::now();
        DB::table('users')->truncate();
        DB::table('users')->insert([
            'name'=>'admin',
            'email'=>'admin@gmail.com',
            'password' =>bcrypt('123456@123'),
            'created_at' => $now
        ]);
    }
}
