<?php

use Illuminate\Database\Seeder;

class create_demo_book_data extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = \Carbon\Carbon::now();
        DB::table('books')->truncate();

        DB::table('books')->insert([
            'id'=>0,
            'name'=>'Tổng hợp',
            'slug'=>'Tong-Hop',
            'description'=>'Truyện tổng hợp bao gồm 2 chuyên mục truyện ngắn và truyện cười',
            'id_cat'=> null,
            'id_author'=> null,
            'created_at' => $now
        ]);

        //
//        $faker = Faker\Factory::create();
//
//        $limit = 100;
//        $now = \Carbon\Carbon::now();
//
//        for ($i = 0; $i< $limit ; $i++){
//            DB::table('books')->insert([
//                'name'=>$faker->name,
//                'slug'=>$faker->slug,
//                'description'=>$faker->text,
//                'id_cat'=>$faker->numberBetween($min = 100, $max = 200),
//                'created_at' => $now
//            ]);
//        }
    }
}
