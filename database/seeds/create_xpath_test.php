<?php

use Illuminate\Database\Seeder;

class create_xpath_test extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('xpath')->truncate();
        //
        $now = \Carbon\Carbon::now();
        DB::table('xpath')->insert([
            'created_at'=>$now,
            'xpath_name'=>'Category content xpath',
            'xpath_options'=>'.list-content .row-info a'
        ]);

        DB::table('xpath')->insert([
            'created_at'=>$now,
            'xpath_name'=>'Category paginate xpath',
            'xpath_options'=>'.pages .numbpage'
        ]);

        DB::table('xpath')->insert([
            'created_at'=>$now,
            'xpath_name'=>'Book descriptions xpath',
            'xpath_options'=>'.summary'
        ]);

        DB::table('xpath')->insert([
            'created_at'=>$now,
            'xpath_name'=>'Book Author xpath',
            'xpath_options'=>'.detail-info >li h2 a'
        ]);

        DB::table('xpath')->insert([
            'created_at'=>$now,
            'xpath_name'=>'Chapter Link old xpath',
            'xpath_options'=>'#divtab > .w3-ul a'
        ]);

        DB::table('xpath')->insert([
            'created_at'=>$now,
            'xpath_name'=>'Chapter Link new xpath',
            'xpath_options'=>'#list > .list-chapter > .w3-ul a'
        ]);

        DB::table('xpath')->insert([
            'created_at'=>$now,
            'xpath_name'=>'Chapter Link paginate xpath',
            'xpath_options'=>'.pages .numbpage'
        ]);

        DB::table('xpath')->insert([
            'created_at'=>$now,
            'xpath_name'=>'Index new Short story xpath',
            'xpath_options'=>'.list-shortstory a'
        ]);

        DB::table('xpath')->insert([
            'created_at'=>$now,
            'xpath_name'=>'Index new chap update xpath',
            'xpath_options'=>'.list-update a'
        ]);

        DB::table('xpath')->insert([
            'created_at'=>$now,
            'xpath_name'=>'Index new Book xpath',
            'xpath_options'=>'.list-content a'
        ]);

        DB::table('xpath')->insert([
            'created_at'=>$now,
            'xpath_name'=>'Book Name',
            'xpath_options'=>'.detail-right > h1 a'
        ]);

        DB::table('xpath')->insert([
            'created_at'=>$now,
            'xpath_name'=>'Book Thumbnail',
            'xpath_options'=>'.detail-thumbnail img'
        ]);

        DB::table('xpath')->insert([
            'created_at'=>$now,
            'xpath_name'=>'ChapDetail content',
            'xpath_options'=>'#content'
        ]);

        DB::table('xpath')->insert([
            'created_at'=>$now,
            'xpath_name'=>'one shot xpath',
            'xpath_options'=>'.list-content .post-title a'
        ]);

        DB::table('xpath')->insert([
            'created_at'=>$now,
            'xpath_name'=>'	category xpath',
            'xpath_options'=>'.detail-info li:nth-child(2) > a'
        ]);
        DB::table('xpath')->insert([
            'created_at'=>$now,
            'xpath_name'=>'index new update xpath',
            'xpath_options'=>'#list .list-update .list-content a'
        ]);
        DB::table('xpath')->insert([
            'created_at'=>$now,
            'xpath_name'=>'Book Release status',
            'xpath_options'=>'.detail-info .w3-ul'
        ]);

    }
}
