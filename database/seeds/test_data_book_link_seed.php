<?php

use Illuminate\Database\Seeder;

class test_data_book_link_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('booklink')->truncate();
        //
//        $faker = Faker\Factory::create();
//
//        $limit = 100;
//        $now = \Carbon\Carbon::now();
//
//        for ($i = 0; $i< $limit ; $i++){
//            DB::table('booklink')->insert([
//                'cat_id'=>$faker->numberBetween('1','10'),
//                'source_url' => $faker->url,
//                'hash_url' => md5($faker->url),
//                'created_at'=>$now
//            ]);
//        }
    }
}
