<?php

use Illuminate\Database\Seeder;

class create_default_chapter_link extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('chapterlinks')->truncate();
        //
//        $faker = Faker\Factory::create();
//
//        $limit = 100;
//        $now = \Carbon\Carbon::now();
//
//        for ($i = 0; $i< $limit ; $i++){
//            DB::table('chapterlinks')->insert([
//                'id_books'=>$faker->numberBetween($min = 1, $max = 100),
//                'source_url' => $faker->url,
//                'hash_url' => md5($faker->url),
//                'created_at'=>$now
//            ]);
//        }
    }
}
