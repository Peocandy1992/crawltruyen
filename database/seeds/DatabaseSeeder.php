<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(create_xpath_test::class);
        $this->call(create_demo_book_data::class);
        $this->call(create_cron_setting_test::class);
        $this->call(create_default_chapter_detail::class);
        $this->call(create_default_chapter_link::class);
        $this->call(test_author::class);
        $this->call(test_data_category::class);
        $this->call(seed_admin::class);
    }
}
