<?php

use Illuminate\Database\Seeder;

class create_default_chapter_detail extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('chapterdetail')->truncate();
        //
//        $faker = Faker\Factory::create();
//
//        $limit = 100;
//        $now = \Carbon\Carbon::now();
//
//        for ($i = 0; $i< $limit ; $i++){
//            DB::table('chapterdetail')->insert([
//                'id_book'=>$faker->numberBetween($min = 1, $max = 100),
//                'id_link'=>$faker->numberBetween($min = 1, $max = 100),
//                'chapName'=>$faker->name,
//                'author_id'=>$faker->numberBetween($min = 1, $max = 100),
//                'chapContent'=>$faker->text,
//                'source_url' => $faker->url,
//                'hash_url' => md5($faker->url),
//                'created_at'=>$now
//            ]);
//        }
    }
}
